<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Banner;

class BannerController extends AdminController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $image = Banner::orderBy('id','desc')->first();
        //dd($popup);
        return view('admin.banner.create',['image'=>$image]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $data = $request->validate([
           'image' => 'required|image|max:500',
        ]);
        $image = Banner::orderBy('id','desc')->first();
        if($image != null) {
            $file_path=env('IMAGE_PATH').$image->image;
            if(file_exists($file_path))
            {
                @unlink($file_path);
            }
            //deleting
            $image->delete();
        }
        
        //Uploading and saving Cover
        if($request->image) {
            $image_path = request('image')->store('uploads/banner', 'public');
            $naked_path = env('IMAGE_PATH') . $image_path;
            $photo = Image::make($naked_path)->fit(1280,319);
            $photo->save();
            $data['image']=$image_path;
        }
        else{
            $data['image']='';
        }
        Banner::create($data);


        return redirect(route('banner.index'))->with('success','Banner added successfully!');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
