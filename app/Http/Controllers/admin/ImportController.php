<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Product;
use App\Product_serialnumber;
use Illuminate\Http\Request;

class ImportController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product_serialnumber::all();
        return view('admin.serial.index',['products'=>$products]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        return view('admin.serial.create',['products'=>$products]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'product_id' =>'required',
            'import_file' =>'required'
        ]);
        //get file
        $upload=$request->file('import_file');
// File Details
        $filename = $upload->getClientOriginalName();
        $extension = $upload->getClientOriginalExtension();
        $filePath = $upload->getRealPath();
        $fileSize = $upload->getSize();
        $mimeType = $upload->getMimeType();

        // Valid File Extensions
        $valid_extension = array("csv");


        // Check file extension
        if(in_array(strtolower($extension),$valid_extension))
            {
                $file=fopen($filePath,'r');
                $importData_arr = array();
                $i = 0;

                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE)
                {
                    $num = count($filedata );

                    // Skip first row
                    if($i == 0){
                       $i++;
                       continue;
                    }
                    for ($c=0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata [$c];
                    }
                    $i++;
                }
                fclose($file);

                // Insert to MySQL database
                //dd($importData_arr);
                foreach($importData_arr as $importData){

                    $insertData = array(
                        "serial_number"=>$importData[0],
                        "safety_code"=>$importData[1],
                        "product_code"=>$importData[2]
                    );
                    $insertData['product_id'] = $data['product_id'];
                    Product_serialnumber::create($insertData);

                }

                return redirect(route('serial.index'))->with('success','Data Imported successfully!');;

        }
        else{
            return back()->with('error','Invalid file extension');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product_serialnumber::findOrFail($id);
        return view('admin.serial.edit',['product'=>$product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $product=Product_serialnumber::findOrFail($id);
        $data = $request->validate([
           'product_id' =>'required',
           'serial_number' =>'required',
           'safety_code' =>'required',
           'product_code' =>'required'
        ]);
        //dd($request);

        $product->update($data);
        return redirect(route('serial.index'))->with('success','Serial Number updated successfully!');;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //
        $product=Product_serialnumber::findOrFail($id);
        $product->delete();
        return back()->with('success','Serial Number deleted successfully!');
    }
//download csv
    public function getDownload($file_name)
    {
        //CSV file is stored under project/public/files/serial_file_sample.csv
        $file_path = public_path('files/'.$file_name);
        return response()->download($file_path);
    }

}
