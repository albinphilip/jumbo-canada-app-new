<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Product;
use App\Product_registration;
use App\Product_repair_registration;
use App\Product_serialnumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ProductRepairRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product_repair_registration::all();
        $uniq = $products->unique('safety_code');
        $duplicate = $products->diff($uniq);
        $duplicate = $duplicate->unique('safety_code');
        return view('admin.product-repair-registration.index',['products'=>$products,'duplicates'=>$duplicate]);
    }

    public function edit($id)
    {
        $regisered_product=Product_repair_registration::findOrFail($id);

        $products=Product::all();
        return view("admin.product-repair-registration.edit",['products'=>$products,'regisered_product'=>$regisered_product]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product_repair_registration::findOrFail($id);
        $data=$request->validate([
            'product_id'=>'required',
            'serial_number' => 'required',
            'safety_code' =>'required',
            'product_code' =>'nullable',
            'purchase_date' =>'nullable',
            'purchase_place' =>'nullable',
            'first_name'=>'nullable',
            'last_name' => 'nullable',
            'provenance'=> 'nullable',
            'city'=>'nullable',
            'address' => 'nullable',
            'zipcode'=> 'nullable',
            'phone' => 'required',
            'email' =>'required'
        ]);
            $product->update($data);
            return redirect(route('product-repair-registration.index'))->with('success','Product repair registration updated successfully');


    }

    public function store(Request $request)
    {
        $product=Product_repair_registration::findOrFail($request->id);
        $product->status=$request->status;
        $product->save();
        return response()->json("success");
    }

    public function reject(Request $request)
    {
        $repair = Product_repair_registration::findOrFail($request->id);
        $repair->status = 'Rejected';
        $repair->reason = $request->reason;
        $repair->save();


        //sending status update to user
       $msg = 'This email is inform you that your product repair Request with RMA Number '. $repair->rma_num.'  has been rejected and the reason for rejection is '.$repair->reason.'.';

        $email = $repair->email;
        Mail::send('front.emails.product-repair-status-change', ['data' => $repair, 'msg' => $msg], function ($message) use ($email) {
            $message->to($email)
                ->subject('Jumbo Canada | Product Repair Request Rejected');
        });
        //dd('here');

        return response()->json("success");

    }
}
