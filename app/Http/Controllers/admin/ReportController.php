<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Order;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Response;
use DateTime;
use App;

class ReportController extends AdminController
{
    public function tax()
    {
        $orders = Order::where('order_id','!=',null)->get();
        return view('admin.report.tax',['orders'=>$orders]);
    }
    //pdf download
    public function tax_report(Request $request)
    {
        $from = $request->from;
        $to = $request->to;

        $from_date = date('M-d-Y',strtotime($from));
        $to_date = date('M-d-Y',strtotime($to));
        $data = '<html lang="en">
<body>
<table role="presentation" align="center" border="0" cellspacing="0" cellpadding="10" width="100%" bgcolor="#edf0f3"
       style="background-color:#ffffff;table-layout:fixed;">
    <tbody>
    <tr>
        <td>
                <div
                    style="background-color:#ffffff;max-width:600px;margin:auto; box-shadow: 1px 1px 8px rgba(0,0,0,0.1);">

                    <table cellspacing="0" cellpadding="0" align="center"
                           style="width:100%;border:3px solid #fb3333; border-top-width:5px; border-left-width:3px;margin-top: 50px;">
                        <tbody>
                        <tr>
                            <td align="center" style="vertical-align:middle">
                                <div style="padding: 10px; background: #f8f8f8;">
                                    <table cellspacing="0" cellpadding="0" border="0" align="center"
                                           style="width:100%;">
                                        <tbody>
                                        <tr>
                                            <td align="left"
                                                style="vertical-align:middle;padding-top:10px;"
                                                bgcolor="#f8f8f8">
                                                <a href="https://jumbocanada.com"><img
                                                        src="https://jumbocanada.torontomalayali.ca/assets/img/logo.png"
                                                        alt="jumbocanada.com" height="35" style="display:block"></a></td>
                                            <td align="right"
                                                style="vertical-align:middle;"
                                                bgcolor="#f8f8f8">
                                                <table width="100%" cellpadding="0" cellspacing="0"
                                                       border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td align="right">
                                                            <table align="right" cellpadding="0"
                                                                   cellspacing="0" border="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="right"
                                                                        colspan="4"
                                                                        style=" padding: 5px 0; font-size: 11px;">
                                                                        <a
                                                                            href="mailto:support@jumbocanada.com">support@jumbocanada.com</a><br/><a href="tel:+1 8557733844"> +1 (855) 773 3844</a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>


                                <table cellspacing="0" cellpadding="0" border="0" align="center"
                                       style="width:100%" bgcolor="#fff">
                                    <tbody>
                                    <tr>
                                        <td align="center" style="vertical-align:middle;height:10px"
                                            bgcolor="#ffffff">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"
                                            style="vertical-align:middle;padding:0 15px; font-size:13px;line-height:1.255!important; color: #696969"
                                            bgcolor="#ffffff">
                                            <table align="middle" style="padding-bottom: 15px;">
                                                <tr align="center pt-5 pb-10" style="color: black;">
                                                  <td align="center" style="vertical-align:middle;font-size:17px;!important;"> Tax Report :
                                                    '.($from != null ? $from_date.' to ' : "" ) .($to != null ? $to_date : ""). '
                                                   </td>
                                                    <td align="right" style="font-size:17px;!important;">  Province : '.$request->province.'
                                                    </td>
                                                </tr>
                                            </table>
                                        <table
                                            style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                                            <thead>
                                            <tr>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Province
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Date of purchase
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Order Id
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Customer Name
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Total Amount
                                                </td><td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Tax Slab
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Tax Amount
                                                </td>

                                            </tr>
                                            </thead>
                                            <tbody>';

        if($from != null && $to != null)
            $orders = Order::where([[('created_at'),'>=',$from],[('created_at'),'<=',$to.' 23:59:59'],['order_id','!=',null]])->groupBy('order_id')->get();
        else if($from == null && $to != null)
            $orders = Order::where([['created_at','<=',$to.' 23:59:59'],['order_id','!=',null]])->groupBy('order_id')->get();
        else if($from != null && $to == null)
            $orders = Order::where([['created_at','>=',$from],['order_id','!=',null]])->groupBy('order_id')->get();
        else
            $orders = Order::where('order_id','!=',null)->groupBy('order_id')->get();

        $total_tax = 0;
        $total = 0;
        $i = 1;
        foreach ($orders->sortByDesc('id') as $order)
        {
            if($order->deliveryAddress->province == $request->province || $request->province == 'All'){
                $status= null;
                $subtotal = str_replace(',','',$order->orderFeesplitUp->sub_total);
                if($order->orderFeesplitUp->tax_value == null){
                    if($order->shipping_partner == 'Canada Post' && $order->shipping_code == '' ){
                        $taxed = ($subtotal - $order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100;
                    }
                    else
                        $taxed = ($subtotal+$order->orderFeesplitUp->shipping_charge - $order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100;
                }
                else
                    $taxed = $order->orderFeesplitUp->tax_value;
                if($order->status != 'Cancel & refunded')
                {
                    $total_tax = $total_tax + $taxed;
                    $total  = $total + $order->order_total;
                }
                else
                {
                    $current_orders = Order::where([['order_id',$order->order_id],['status','!=','Cancel & refunded']])->first();
                    if($current_orders != null)
                    {
                        $total  = $total + $order->order_total;
                        $total_tax = $total_tax + $taxed;
                    }
                }

                $data = $data.'<tr>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.$order->deliveryAddress->province.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.date_format($order->created_at,"M-d-Y").'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.$order->order_id.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.$order->customer->firstname.' '.$order->customer->lastname.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.'C$'.$order->order_total.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">'.$order->orderFeesplitUp->tax.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">C$'.$taxed.'</td>
        </tr>';
            }
            if($i % 22 == 0){
                $data = $data.'</tbody></table>
                <div class="col-12" style="page-break-inside:avoid;page-break-after:always; border-bottom:1px red;"></div>
                <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                <tbody>';
            }
            $i++;
        }
        $data = $data.'<tfoot>
                <tr>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                        colspan="4"><b>Total:</b></td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                 C$'.$total.
                '</td>
                 <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"></td>
                  <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                     C$'.$total_tax.
                '</td>
                </tr>
                </tfoot>';
            $data = $data. '</tbody>';

            $data = $data.'</table>';
            $data = $data.' </td>
                           </tr>
                         </tbody>
                       </table>
                </div>
                </td>
            </tr>
            </tbody>
        </table>
        </div>

        </body>
        </html>';
        $pdf = App::make('dompdf.wrapper');
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf->setPaper( 'A4' );
        $pdf->loadHTML($data);
        $filename = 'Tax-report-'.time().'.pdf';
        $file = $pdf->output();
        $pdf->save('report/'.$filename,'public');
        $path = asset('report/'.$filename);

        return response()->json(['success'=>'success','path'=>$path]);

    }
    public function order()
    {
        $orders = Order::where('order_id','!=',null)->get();
        return view('admin.report.order',['orders'=>$orders]);
    }
    //pdf download
    public function order_report(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $from_date = date('M-d-Y',strtotime($from));
        $to_date = date('M-d-Y',strtotime($to));
        $data = '<html lang="en">
<body>
<table role="presentation" align="center" border="0" cellspacing="0" cellpadding="10" width="100%" bgcolor="#edf0f3"
       style="background-color:#ffffff;table-layout:fixed;">
    <tbody>
    <tr>
        <td>
                <div
                    style="background-color:#ffffff;max-width:600px;margin:auto; box-shadow: 1px 1px 8px rgba(0,0,0,0.1);">

                    <table cellspacing="0" cellpadding="0" align="center"
                           style="width:100%;border:3px solid #fb3333; border-top-width:5px; border-left-width:3px;margin-top: 50px;">
                        <tbody>
                        <tr>
                            <td align="center" style="vertical-align:middle">
                                <div style="padding: 10px; background: #f8f8f8;">
                                    <table cellspacing="0" cellpadding="0" border="0" align="center"
                                           style="width:100%;">
                                        <tbody>
                                        <tr>
                                            <td align="left"
                                                style="vertical-align:middle;padding-top:10px;"
                                                bgcolor="#f8f8f8">
                                                <a href="https://jumbocanada.com"><img
                                                        src="https://jumbocanada.torontomalayali.ca/assets/img/logo.png"
                                                        alt="jumbocanada.com" height="35" style="display:block"></a></td>
                                            <td align="right"
                                                style="vertical-align:middle;"
                                                bgcolor="#f8f8f8">
                                                <table width="100%" cellpadding="0" cellspacing="0"
                                                       border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td align="right">
                                                            <table align="right" cellpadding="0"
                                                                   cellspacing="0" border="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="right"
                                                                        colspan="4"
                                                                        style=" padding: 5px 0; font-size: 11px;">
                                                                        <a
                                                                            href="mailto:support@jumbocanada.com">support@jumbocanada.com</a><br/><a href="tel:+1 8557733844"> +1 (855) 773 3844</a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <table cellspacing="0" cellpadding="0" border="0" align="center"
                                       style="width:100%" bgcolor="#fff">
                                    <tbody>
                                    <tr>
                                        <td align="center" style="vertical-align:middle;height:10px"
                                            bgcolor="#ffffff">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"
                                            style="vertical-align:middle;padding:0 15px; font-size:13px;line-height:1.255!important; color: #696969"
                                            bgcolor="#ffffff">
                                            <table align="middle" style="padding-bottom: 15px; ">
                                            <tr align="center pt-5" style="color:black;">
                                              <td align="left" style="vertical-align:middle; font-size: 15px;">Order Report :
                                                '.($from != null ? $from_date.' to ' : "" ) .($to != null ? $to_date : ""). '
                                               </td>
                                             </tr>
                                            <tr>
                                            <td align="left" style="font-size: 15px;color: black"> Province : '.$request->province.' </td>
                                            </tr>
                                            <tr>
                                            <td align="left" style="font-size: 15px; color: black"> Payment Status : '.$request->status.'
                                                </td>
                                            </tr>
                                        </table>
                                        <table
                                            style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px ;border-bottom:1px solid;">
                                            <thead>
                                            <tr>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Date of purchase
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Order Id
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Province
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Payment Status
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Shipping Cost
                                                </td><td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Discounted Amount
                                                </td>
                                                 </td><td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Tax Amount
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Total Amount
                                                </td>

                                            </tr>
                                            </thead>
                                            <tbody>';

        if($from != null && $to != null)
            $orders = Order::where([[('created_at'),'>=',$from],[('created_at'),'<=',$to.' 23:59:59'],['order_id','!=',null]])->groupBy('order_id')->get();
        else if($from == null && $to != null)
            $orders = Order::where([['created_at','<=',$to.' 23:59:59'],['order_id','!=',null]])->groupBy('order_id')->get();
        else if($from != null && $to == null)
            $orders = Order::where([['created_at','>=',$from],['order_id','!=',null]])->groupBy('order_id')->get();
        else
            $orders = Order::where('order_id','!=',null)->groupBy('order_id')->get();

        $total_tax = 0;
        $total = 0;
        $discount  = 0;
        $i = 1;
        foreach ($orders->sortByDesc('id') as $order)
        {
            if(($order->deliveryAddress->province == $request->province || $request->province == 'All') && ($order->status == $request->status || $request->status == 'All')){
                $status= null;
                $subtotal = str_replace(',','',$order->orderFeesplitUp->sub_total);
                if($order->orderFeesplitUp->tax_value == null){
                    if($order->shipping_partner == 'Canada Post' && $order->shipping_code == '' ){
                        $taxed = ($subtotal - $order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100;
                    }
                    else
                        $taxed = ($subtotal+$order->orderFeesplitUp->shipping_charge - $order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100;
                }
                else
                    $taxed = $order->orderFeesplitUp->tax_value;
                if($order->status != 'Cancel & refunded')
                {
                    $total_tax = $total_tax + $taxed;
                    $total  = $total + $order->order_total;
                    $discount  = $discount + $order->orderFeesplitUp->reduction;
                }
                else
                {
                    $current_orders = Order::where([['order_id',$order->order_id],['status','!=','Cancel & refunded']])->first();
                    if($current_orders != null)
                    {
                        $total  = $total + $order->order_total;
                        $total_tax = $total_tax + $taxed;
                        $discount  = $discount + $order->orderFeesplitUp->reduction;
                    }
                }

                $data = $data.'<tr>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.date_format($order->created_at,"M-d-Y").'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.$order->order_id.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.$order->deliveryAddress->province.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.$order->status.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">'.'C$'.$order->orderFeesplitUp->shipping_charge.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">'.'C$'.$order->orderFeesplitUp->reduction.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">'.'C$'.$taxed.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">C$'.$order->order_total.'</td>
        </tr>';
            }
            if($i % 15 == 0){
                $data = $data.'</tbody></table>
                <div class="col-12" style="page-break-inside:auto;page-break-after:always; border-bottom:1px red;"></div>
                <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px;">
                <tbody>';
            }
            $i++;
        }
        $data = $data.'<tfoot>
                <tr>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                        colspan="5"><b>Total:</b></td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                 C$'.$discount.
            '</td>
                 <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">C$'.$total_tax.'</td>
                  <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                     C$'.$total.
            '</td>
                </tr>
                </tfoot>';
        $data = $data. '</tbody>';

        $data = $data.'</table>';
        $data = $data.' </td>
                           </tr>
                         </tbody>
                       </table>
                </div>
                </td>
            </tr>
            </tbody>
        </table>
        </body>
        </html>';
        $pdf = App::make('dompdf.wrapper');
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf->setPaper( 'A4' );
        $pdf->loadHTML($data);
        $filename = 'Order-report-'.time().'.pdf';
        $file = $pdf->output();
        $pdf->save('report/'.$filename,'public');
        $path = asset('report/'.$filename);

        return response()->json(['success'=>'success','path'=>$path]);

    }
    public function performance()
    {
        $products = App\Product::all();
        return view('admin.report.performance',['products'=>$products]);
    }
    //pdf download
    public function performance_report(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $from_date = date('M-d-Y',strtotime($from));
        $to_date = date('M-d-Y',strtotime($to));
        $data = '<html lang="en">
<body>
<table role="presentation" align="center" border="0" cellspacing="0" cellpadding="10" width="100%" bgcolor="#edf0f3"
       style="background-color:#ffffff;table-layout:fixed;">
    <tbody>
    <tr>
        <td>
                <div
                    style="background-color:#ffffff;max-width:600px;margin:auto; box-shadow: 1px 1px 8px rgba(0,0,0,0.1);">

                    <table cellspacing="0" cellpadding="0" align="center"
                           style="width:100%;border:3px solid #fb3333; border-top-width:5px; border-left-width:3px;margin-top: 50px;">
                        <tbody>
                        <tr>
                            <td align="center" style="vertical-align:middle">
                                <div style="padding: 10px; background: #f8f8f8;">
                                    <table cellspacing="0" cellpadding="0" border="0" align="center"
                                           style="width:100%;">
                                        <tbody>
                                        <tr>
                                            <td align="left"
                                                style="vertical-align:middle;padding-top:10px;"
                                                bgcolor="#f8f8f8">
                                                <a href="https://jumbocanada.com"><img
                                                        src="https://jumbocanada.torontomalayali.ca/assets/img/logo.png"
                                                        alt="jumbocanada.com" height="35" style="display:block"></a></td>
                                            <td align="right"
                                                style="vertical-align:middle;"
                                                bgcolor="#f8f8f8">
                                                <table width="100%" cellpadding="0" cellspacing="0"
                                                       border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td align="right">
                                                            <table align="right" cellpadding="0"
                                                                   cellspacing="0" border="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="right"
                                                                        colspan="4"
                                                                        style=" padding: 5px 0; font-size: 11px;">
                                                                        <a
                                                                            href="mailto:support@jumbocanada.com">support@jumbocanada.com</a><br/><a href="tel:+1 8557733844"> +1 (855) 773 3844</a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>


                                <table cellspacing="0" cellpadding="0" border="0" align="center"
                                       style="width:100%" bgcolor="#fff">
                                    <tbody>
                                    <tr>
                                        <td align="center" style="vertical-align:middle;height:10px"
                                            bgcolor="#ffffff">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"
                                            style="vertical-align:middle;padding:0 15px; font-size:13px;line-height:1.255!important; color: #696969"
                                            bgcolor="#ffffff">
                                            <table align="middle" style="padding-bottom: 10px;">
                                                <tr align="center pt-5" style="color:black;">
                                                      <td align="left" style="vertical-align:middle;font-size: 15px;">Performance Report :
                                                        '.($from != null ? $from_date.' - ' : "" ) .($to != null ? $to_date : ""). '
                                                       </td>
                                                    </tr>

                                        </table>
                                        <table
                                            style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                                            <thead>
                                            <tr>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Product List
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Total Sale
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Total Sales Value
                                                </td>

                                             </tr>
                                            </thead>
                                            <tbody>';
        $products = App\Product::all();
        foreach ($products->sortByDesc('id') as $product)
        {
            if($from != null && $to != null)
                $orders = Order::where([[('created_at'),'>=',$from],[('created_at'),'<=',$to.' 23:59:59'],['order_id','!=',null],
                    ['product_id',$product->id],['status', '!=' ,'Cancel & refunded'],['status','!=','Return Confirmed']])->get();
            else if($from == null && $to != null)
                $orders = Order::where([['created_at','<=',$to.' 23:59:59'],['product_id',$product->id],['order_id','!=',null],
                    ['status', '!=' ,'Cancel & refunded'],['status','!=','Return Confirmed']])->get();
            else if($from != null && $to == null)
                $orders = Order::where([['created_at','>=',$from],['product_id',$product->id],['order_id','!=',null],
                    ['status', '!=' ,'Cancel & refunded'],['status','!=','Return Confirmed']])->get();
            else
                $orders = Order::where([['product_id',$product->id],['order_id','!=',null],
                    ['status', '!=' ,'Cancel & refunded'],['status','!=','Return Confirmed']])->get();
            $total = 0;
            $count = 0;
            $i = 1;
            foreach ($orders->sortByDesc('id') as $order)
            {
                $count = $count + $order->quantity;
                $total = $total + ($order->product_price * $order->quantity);
            }
            if($count > 0)
            {
                $data = $data.'<tr>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.$product->name.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">'.$count.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">C$'.$total.'</td>
            </tr>';
            }
            /*if($i % 10 == 0){
                $data = $data.'</tbody></table>
                <div class="col-12" style="page-break-inside:avoid;page-break-after:always; border-bottom:1px red;"></div>
                <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                <tbody>';
            }
            $i++;*/
        }
        $data = $data. '</tbody>';

        $data = $data.'</table>';
        $data = $data.' </td>
                           </tr>
                         </tbody>
                       </table>
                </div>
                </td>
            </tr>
            </tbody>
        </table>
        </body>
        </html>';
        $pdf = App::make('dompdf.wrapper');
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf->setPaper( 'A4' );
        $pdf->loadHTML($data);
        $filename = 'Performance-report-'.time().'.pdf';
        $file = $pdf->output();
        $pdf->save('report/'.$filename,'public');
        $path = asset('report/'.$filename);

        return response()->json(['success'=>'success','path'=>$path]);

    }
    //return report
    public function return()
    {
        $returns = App\Product_return::all();
        $stores = App\Store::all();
        return view('admin.report.return',['returns'=>$returns,'stores'=>$stores]);
    }

    public function return_report(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $from_date = date('M-d-Y',strtotime($from));
        $to_date = date('M-d-Y',strtotime($to));
        $data = '<html lang="en">
<body>
<table role="presentation" align="center" border="0" cellspacing="0" cellpadding="10" width="100%" bgcolor="#edf0f3"
       style="background-color:#ffffff;table-layout:fixed;">
    <tbody>
    <tr>
        <td>
                <div
                    style="background-color:#ffffff;max-width:600px;margin:auto; box-shadow: 1px 1px 8px rgba(0,0,0,0.1);">

                    <table cellspacing="0" cellpadding="0" align="center"
                           style="width:100%;border:3px solid #fb3333; border-top-width:5px; border-left-width:3px;margin-top: 50px;">
                        <tbody>
                        <tr>
                            <td align="center" style="vertical-align:middle">
                                <div style="padding: 10px; background: #f8f8f8;">
                                    <table cellspacing="0" cellpadding="0" border="0" align="center"
                                           style="width:100%;">
                                        <tbody>
                                        <tr>
                                            <td align="left"
                                                style="vertical-align:middle;padding-top:10px;"
                                                bgcolor="#f8f8f8">
                                                <a href="https://jumbocanada.com"><img
                                                        src="https://jumbocanada.torontomalayali.ca/assets/img/logo.png"
                                                        alt="jumbocanada.com" height="35" style="display:block"></a></td>
                                            <td align="right"
                                                style="vertical-align:middle;"
                                                bgcolor="#f8f8f8">
                                                <table width="100%" cellpadding="0" cellspacing="0"
                                                       border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td align="right">
                                                            <table align="right" cellpadding="0"
                                                                   cellspacing="0" border="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="right"
                                                                        colspan="4"
                                                                        style=" padding: 5px 0; font-size: 11px;">
                                                                        <a
                                                                            href="mailto:support@jumbocanada.com">support@jumbocanada.com</a><br/><a href="tel:+1 8557733844"> +1 (855) 773 3844</a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <table cellspacing="0" cellpadding="0" border="0" align="center"
                                       style="width:100%" bgcolor="#fff">
                                    <tbody>
                                    <tr>
                                        <td align="center" style="vertical-align:middle;height:10px"
                                            bgcolor="#ffffff">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"
                                            style="vertical-align:middle;padding:0 15px; font-size:13px;line-height:1.255!important; color: #696969"
                                            bgcolor="#ffffff">
                                            <table align="middle" style="padding-bottom: 15px; ">
                                            <tr align="center pt-5" style="color:black;">
                                              <td align="left" style="vertical-align:middle; font-size: 15px;">Return Report :
                                                '.($from != null ? $from_date.' to ' : "" ) .($to != null ? $to_date : ""). '
                                               </td>
                                             </tr>
                                            <tr>
                                            <td align="left" style="font-size: 15px;color: black"> Store : '.$request->store.' </td>
                                            </tr>
                                            <tr>
                                            <td align="left" style="font-size: 15px; color: black"> Status : '.$request->status.'
                                                </td>
                                            </tr>
                                        </table>
                                        <table
                                            style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                                            <thead>
                                            <tr>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             RMA
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Product Detail
                                                </td>
                                            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Store Detail
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Customer Detail
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Fault
                                                </td>
                                                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                             Status
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>';

        if($from != null && $to != null)
            $returns = App\Product_return::where([[('created_at'),'>=',$from],[('created_at'),'<=',$to.' 23:59:59']])->get();
        else if($from == null && $to != null)
            $returns = App\Product_return::where('created_at','<=',$to.' 23:59:59')->get();
        else if($from != null && $to == null)
            $returns = App\Product_return::where('created_at','>=',$from)->get();
        else
            $returns = App\Product_return::all();

        foreach ($returns->sortByDesc('id') as $return)
        {
            if(($return->product_registration->store->id == $request->store || $request->store == 'All') && ($return->status == $request->status || $request->status == 'All'))
            {
                $data = $data.'<tr>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.$return->return_id.'</td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                            Product: '.$return->product_registration->product->name.'<br/>
                            Reg Id: '.$return->reg_num.'<br/>
                            Reg Date: '.date("M d, Y", strtotime($return->product_registration->created_at)).'<br/>
                            Purchase Date: '.date("M d, Y", strtotime($return->product_registration->purchase_date)).'<br/>
                            Serial No: '.$return->product_registration->serial_number.'<br/>
                            Product Code: '.$return->product_registration->product_code.'<br/>
                            Safety Code: '.$return->product_registration->safety_code.'<br/>
                    </td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.
                        $return->product_registration->store->address.'<br/>'.
                        $return->product_registration->store->city.'<br/>'.
                        $return->product_registration->store->phone.'

                    </td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                            Name: '.$return->product_registration->first_name.' '.$return->product_registration->last_name.' <br/>
                            Phone: '.$return->product_registration->phone.'<br/>
                            Email: <a target="_blank"
                                      href="mailto:$return->product_registration->email">'. $return->product_registration->email.'</a><br/>
                            Address: '.$return->product_registration->address.','. $return->product_registration->city.'
                            , '.$return->product_registration->provenance.','. $return->product_registration->zipcode.'
            </td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.
            $return->dead.'
            </td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.
                    $return->status.'
            </td>
                </tr>';
            }

        }
        $data = $data. '</tbody>';

        $data = $data.'</table>';
        $data = $data.' </td>
                           </tr>
                         </tbody>
                       </table>
                </div>
                </td>
            </tr>
            </tbody>
        </table>
        </body>
        </html>';
        $pdf = App::make('dompdf.wrapper');
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf->setPaper( 'A4' );
        $pdf->loadHTML($data);
        $filename = 'Return-report-'.time().'.pdf';
        $file = $pdf->output();
        $pdf->save('report/'.$filename,'public');
        $path = asset('report/'.$filename);

        return response()->json(['success'=>'success','path'=>$path]);

    }
}
