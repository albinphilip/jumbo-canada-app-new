<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Controllers\AdminController;
use App\Product;
use App\Product_image;
use App\Product_metadata;
use App\Product_shipment;
use App\Product_specification;
use App\Product_variant;
use App\Product_video;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Session;
class ProductController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('getImage')->get();
        //dd($products);
        return view('admin.product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.product.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->specification);
        $data = $request->validate([
            'category_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            //'keywords' => 'nullable',
            'offer' => 'nullable',
            'offer_expire' => 'nullable',
            'model' => 'nullable',
            'upc' => 'nullable',
            'weight' => 'nullable',
            'dimension' => 'nullable',
            'brand_id'=>'nullable',
        ]);
        $data['keywords'] = '';

        //add additional fields here
        if ($request->price == null || !array_filter(array_map('array_filter', $request->price)))// no price added
            $data['status'] = 'Hidden';
        else
            $data['status'] = 'Published';
        //slug
        $data['slug'] = Str::slug($data['name'], '-');
        $product = Product::create($data);

        //Storing
        if ($product->id) {
            //specification
            $data = array();
            $data['product_id'] = $product->id;
            $len = sizeof($request->specification);
            if ($len >= 1) {
                for ($i = 1; $i <= $len; $i++) {
                    foreach ($request->specification[$i] as $specification) {
                        foreach ($request->valu[$i] as $value) {
                            foreach ($request->unit[$i] as $unit) {

                                if ($specification != '' || $value != '' || $unit != '') {
                                    $data['specification'] = $specification;
                                    $data['value'] = $value;
                                    $data['unit'] = $unit;
                                    //dd($data);
                                    Product_specification::create($data);
                                }
                            }
                        }
                    }
                }
            }
            //dd('hhh');
            //storing image
            $data = array();
            $len = 0;
            $data['product_id'] = $product->id;
            if ($request->image_path != null)
                $len = sizeof($request->image_path);
            //dd($request->image_path);
            //dd($len);
            if ($len >= 1) {
                for ($i = 1; $i <= $len; $i++) {
                    foreach ($request->image_path[$i] as $image) {
                        if ($image && $image->getSize() <= 500000) {
                            //dd($image);
                            $image_path = $image->store('uploads/productImage/image', 'public');
                            $naked_path = env('IMAGE_PATH') . $image_path;
                            $photo = Image::make($naked_path);
                            $photo->save();
                            $data['image_path'] = $image_path;
                        } else {
                            $data['image_path'] = '';
                            Session::put('image_error','Size of Image is greater than 500kb');
                        }
                        //thumbnail
                        if ($image) {
                            $image_path = $image->store('uploads/productImage/thumbnail', 'public');
                            $naked_path = env('IMAGE_PATH') . $image_path;
                            $photo = Image::make($naked_path);
                            $photo->save();
                            $data['thumbnail_path'] = $image_path;
                        } else {
                            $data['thumbnail_path'] = '';
                        }
                        foreach ($request->sort_order[$i] as $sort) {
                            $data['sort_order'] = $sort;
                            if ($data['image_path'] != '' || $data['sort_order'] != '') {
                                Product_image::create($data);

                            }
                        }
                    }
                }
            }

            //storing video
            $data = array();
            $len = 0;
            $data['product_id'] = $product->id;
            //dd($request->variant);
            if ($request->url !== '') {
                $len = sizeof($request->url);
                //dd($len);
                if ($len >= 1) {
                    for ($i = 1; $i <= $len; $i++) {
                        foreach ($request->url[$i] as $url) {
                            foreach ($request->v_sort_order[$i] as $odr) {
                                if ($url != '' || $odr != '') {
                                    $video_id = explode("?v=", $url);
                                    if (empty($video_id[1]))
                                        $video_id = explode("/v/", $url);
                                    $video_id = explode("&", $video_id[1]);
                                    $video_id = $video_id[0];
                                    $data['url'] = $video_id;
                                    $data['sort_order'] = $odr;
                                    Product_video::create($data);
                                }
                            }
                        }
                    }
                }
            }

            //storing varient
            $data = array();
            $len = 0;
            $data['product_id'] = $product->id;
            //dd($request->variant);
            $len = sizeof($request->variant);
            if ($len >= 1) {
                for ($i = 1; $i <= $len; $i++) {
                    foreach ($request->variant[$i] as $variant) {
                        foreach ($request->val[$i] as $value) {
                            foreach ($request->price[$i] as $price) {
                                foreach ($request->quantity[$i] as $quantity) {
                                    if ($variant != '' || $value != '' || $price != '' || $quantity != '') {
                                        $data['variant'] = $variant;
                                        $data['value'] = $value;
                                        $data['price'] = $price;
                                        $data['quantity'] = $quantity;
                                        Product_variant::create($data);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //storing metadata

            $data = array('title'=>$request->title,'description'=>$request->meta_description,'keywords'=>$request->keywords);
            $data['product_id'] = $product->id;
            Product_metadata::create($data);

            //storing shipment parameters

            $data = $request->validate([
                'height' => 'nullable',
                'shipment_weight' => 'nullable',
                'length' => 'nullable',
                'breadth'=>'nullable',
            ]);
            //inch to cm
            $data['height'] = $data['height']*2.54;
            $data['length'] = $data['length']*2.54;
            $data['breadth'] = $data['breadth']*2.54;
            $data['product_id'] = $product->id;
            Product_shipment::create($data);
        }
        return redirect(route('product.index'))->with('success', 'Product added successfully!');;


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        //dd($product->productVideos);
        $categories = Category::all();
       // dd($product->productSpecifications);
        return view('admin.product.edit', ['product' => $product, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // dd($request->specification);
        $product = Product::findOrFail($id);
        $data = $request->validate([
            'category_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'keywords' => 'nullable',
            'offer' => 'nullable|numeric',
            'offer_expire' => 'nullable',
            'model' => 'nullable',
            'upc' => 'nullable',
            'status' => 'required',
            'weight' => 'nullable',
            'dimension' => 'nullable',
            'brand_id'=>'nullable',
        ]);
        //dd($data['status']);
        //slug
        $data['slug'] = Str::slug($data['name'], '-');
        if ($data['status'] == 'Published') {
            if ($request->price == null || !array_filter(array_map('array_filter', $request->price))) {
                // price is empty
                return back()->withInput()->with('error', 'Please add Variant and Price');
            }
        }
        $product->update($data);
        //delete all specifications
        Product_specification::where('product_id', '=', $id)->delete();
        //add new specifications
        $data = array();
        $len = 0;
        $data['product_id'] = $product->id;
        if ($request->specification != null)
            $len = sizeof($request->specification);
        if ($len >= 1) {
            for ($i = 1; $i <= $len; $i++) {
                foreach ($request->specification[$i] as $specification) {
                    foreach ($request->valu[$i] as $value) {
                        foreach ($request->unit[$i] as $unit) {

                            if ($specification != '' || $value != '' || $unit != '') {
                                $data['specification'] = $specification;
                                $data['value'] = $value;
                                $data['unit'] = $unit;
                                //dd($data);
                                Product_specification::create($data);
                            }
                        }
                    }
                }
            }
        }
        //delete image
        //Product_image::where('product_id','=',$id)->delete();
        //add new images
        $data = array();
        $len = 0;
        $data['product_id'] = $product->id;
        if ($request->image_path != null)
            $len = sizeof($request->image_path);
        //dd($request->image_path);
        if ($len >= 1) {
            for ($i = 1; $i <= $len; $i++) {
                foreach ($request->image_path[$i] as $image) {
                    if (is_string($image)) {
                        foreach ($request->sort_order[$i] as $sort) {
                            $data['sort_order'] = $sort;
                            Product_image::where('image_path', $image)->update(['sort_order' => $sort]);
                        }
                    } else {
                        if ($image && $image->getSize() <= 500000) {
                            //dd($image->getSize());
                            $image_path = $image->store('uploads/productImage/image', 'public');
                            $naked_path = env('IMAGE_PATH') . $image_path;
                            $photo = Image::make($naked_path);
                            $photo->save();
                            $data['image_path'] = $image_path;
                        } else {
                            //dd($image);
                            $data['image_path'] = '';
                            Session::put('image_error','Size of Image is greater than 500kb');
                        }
                        //thumbnail
                        if ($image) {
                            $image_path = $image->store('uploads/productImage/thumbnail', 'public');
                            $naked_path = env('IMAGE_PATH') . $image_path;
                            $photo = Image::make($naked_path);
                            $photo->save();
                            $data['thumbnail_path'] = $image_path;
                        } else {
                            $data['thumbnail_path'] = '';
                        }
                        foreach ($request->sort_order[$i] as $sort) {
                            $data['sort_order'] = $sort;
                            if ($data['image_path'] != '' || $data['sort_order'] != '') {
                                Product_image::create($data);

                            }
                        }
                    }

                }
            }
        }

        //delete video
        Product_video::where('product_id', $id)->delete();
        //Add new videos
        //storing video
        $data = array();
        $len = 0;
        $data['product_id'] = $product->id;
        //dd($request->variant);
        if ($request->url != '') {
            $len = sizeof($request->url);
            //dd($len);
            if ($len >= 1) {
                for ($i = 1; $i <= $len; $i++) {
                    foreach ($request->url[$i] as $url) {
                        foreach ($request->v_sort_order[$i] as $odr) {
                            if ($url != '' || $odr != '') {
                                $video_id = explode("?v=", $url);
                                if (empty($video_id[1]))
                                    $video_id = explode("/v/", $url);
                                $video_id = explode("&", $video_id[1]);
                                $video_id = $video_id[0];
                                $data['url'] = $video_id;
                                $data['sort_order'] = $odr;
                                Product_video::create($data);
                            }
                        }
                    }
                }
            }
        }
        //delete variant
        Product_variant::where('product_id', '=', $id)->delete();
        //add new variants
        //storing varient
        $data = array();
        $len = 0;
        $data['product_id'] = $product->id;
        //dd($request->val);
        if ($request->variant != null)
            $len = sizeof($request->variant);
        //dd($len);
        if ($len >= 1) {
            for ($i = 1; $i <= $len; $i++) {
                foreach ($request->variant[$i] as $variant) {
                    foreach ($request->val[$i] as $value) {
                        foreach ($request->price[$i] as $price) {
                            foreach ($request->quantity[$i] as $quantity) {
                                if ($variant != '' || $value != '' || $price != '' || $quantity != '') {
                                    $data['variant'] = $variant;
                                    $data['value'] = $value;
                                    $data['price'] = $price;
                                    $data['quantity'] = $quantity;
                                    Product_variant::create($data);
                                }
                            }
                        }
                    }
                }
            }
        }
        //updating shipment parameters
        //dd($request);
        $data = array('title'=>$request->title,'description'=>$request->meta_description,'keywords'=>$request->keywords);
        $data['product_id'] = $product->id;
        $metadata = Product_metadata::where('product_id',$product->id)->delete();
        Product_metadata::create($data);


        $data = $request->validate([
            'height' => 'nullable',
            'shipment_weight' => 'nullable',
            'length' => 'nullable',
            'breadth'=>'nullable',
        ]);
        //inch to cm
        $data['height'] = $data['height']*2.54;
        $data['length'] = $data['length']*2.54;
        $data['breadth'] = $data['breadth']*2.54;
        $data['product_id'] = $product->id;
        $shipment = Product_shipment::where('product_id',$product->id)->delete();
        Product_shipment::create($data);

        return back()->with('success', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return back()->with('success', 'Product deleted successfully!');
    }

    public function out()
    {
        $out = Product_variant::where('quantity', '<', 5)->with('product')->get();
        //dd($out);
        return view('admin.product.out_of_stock', ['outs' => $out]);
    }

    public function bestseller($id)
    {
        $product = Product::findOrFail($id);
        $product->update(['bestseller'=>'yes']);
        return redirect(route('product.index'))->with('success','Product added to bestseller');
    }
    public function remove_bestseller($id)
    {
        $product = Product::findOrFail($id);
        $product->update(['bestseller'=>'']);
        return redirect(route('product.index'))->with('success','Product removed from bestseller');
    }
}
