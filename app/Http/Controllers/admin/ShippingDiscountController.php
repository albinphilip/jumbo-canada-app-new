<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Controllers\AdminController;
use App\Product;
use App\Shipping_discount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ShippingDiscountController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $codes=Shipping_discount::all();
        return view('admin.shippingdiscount.index',['codes'=>$codes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $products = Product::all();
        return view('admin.shippingdiscount.create',['categories'=>$categories,'products'=>$products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->has('category'))
            $request->request->add(['category'=>'']);
        //dd($request);
        $code = mt_rand(1111,999999);
        $request->request->add(['code' => $code]);
        $data=$request->validate([
            'code'=>'required|unique:coupon_codes,code',
            'expires_on'=>'required',
            'applicable_to'=>'required',
            'value'=>'required|numeric',
            'category'=>'nullable',
            'applicable_product'=>'nullable',

        ]);
        $data['reduction_type'] = 'Amount';
        $data['status']='Published';
        if($data['category']==null && $data['applicable_product']==null )
            return back()->with('error','Select category or product')->withInput($data);
        else{
            Shipping_discount::create($data);
            return redirect(route('shippingdiscount.index'))->with('success','Shipping Discount added successfully!');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $code=Shipping_discount::findOrFail($id);
        $categories = Category::all();
        $products = Product::all();
//        dd($code->category);
        return view('admin.shippingdiscount.edit',['code'=>$code,'categories'=>$categories,'products'=>$products]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $code=Shipping_discount::findOrFail($id);
        if(!$request->has('category'))
            $request->request->add(['category'=>'']);
        $data=$request->validate([
            'code'=>'required|unique:coupon_codes,code,'.$id,
            'expires_on'=>'required',
            'applicable_to'=>'required',
            'value'=>'required|numeric',
            'category'=>'nullable',
            'applicable_product'=>'nullable',
            'status'=>'required',
        ]);
        $data['reduction_type'] = 'Amount';
        if($data['category']==null && $data['applicable_product']==null )
            return back()->with('error','Select category or product')->withInput($data);
        else {
            $code->update($data);
            return redirect(route('shippingdiscount.index'))->with('success', 'Shipping Discount updated successfully!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $code=Shipping_discount::findOrFail($id);
        $code->delete();
        return back()->with('success','Shipping Discount deleted successfully!');
    }


}
