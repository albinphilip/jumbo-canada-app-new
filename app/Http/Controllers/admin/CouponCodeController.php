<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\CouponCode;
use App\Http\Controllers\AdminController;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CouponCodeController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $codes=CouponCode::all();
        return view('admin.couponcode.index',['codes'=>$codes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $products = Product::all();
        return view('admin.couponcode.create',['categories'=>$categories,'products'=>$products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->has('category'))
            $request->request->add(['category'=>'']);
        //dd($request);
        $data=$request->validate([
            'code'=>'required|unique:coupon_codes,code',
            'expires_on'=>'required',
            'applicable_to'=>'required',
            'reduction_type'=>'required',
            'value'=>'required|numeric',
            'total_redemptions'=>'required|numeric',
            'redemptions_per_customer'=>'required|numeric',
            'category'=>'nullable',
            'applicable_product'=>'nullable',

        ]);
        $data['status']='Published';
        if($data['category']==null && $data['applicable_product']==null )
            return back()->with('error','Select category or product')->withInput($data);
        else{
            CouponCode::create($data);
            return redirect(route('couponcode.index'))->with('success','Coupon Code added successfully!');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $code=CouponCode::findOrFail($id);
        $categories = Category::all();
        $products = Product::all();
        return view('admin.couponcode.edit',['code'=>$code,'categories'=>$categories,'products'=>$products]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $code=CouponCode::findOrFail($id);
        if(!$request->has('category'))
            $request->request->add(['category'=>'']);
        $data=$request->validate([
            'code'=>'required|unique:coupon_codes,code,'.$id,
            'expires_on'=>'required',
            'applicable_to'=>'required',
            'reduction_type'=>'required',
            'value'=>'required|numeric',
            'total_redemptions'=>'required|numeric',
            'redemptions_per_customer'=>'required|numeric',
            'category'=>'nullable',
            'applicable_product'=>'nullable',
            'status'=>'required',
        ]);
        if($data['category']==null && $data['applicable_product']==null )
            return back()->with('error','Select category or product')->withInput($data);
        else {
            $code->update($data);
            return redirect(route('couponcode.index'))->with('success', 'Coupon Code updated successfully!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $code=CouponCode::findOrFail($id);
        $code->delete();
        return back()->with('success','Coupon Code deleted successfully!');
    }


}
