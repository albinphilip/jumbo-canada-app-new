<?php

namespace App\Http\Controllers\admin;

use App\Additional_fee;
use App\Http\Controllers\AdminController;
use App\Order;
use App\OrderFee_splitup;
use App\Product_variant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Response;
use DateTime;
use App;
use Barryvdh\DomPDF\Facade as PDF;

class ExportOrderController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //export to csv
    public function export(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $sel_status = implode(',',$request->status);

        if($from != null && $to != null)
            $orders = Order::where([[('created_at'),'>=',$from],[('created_at'),'<=',$to.' 23:59:59'],['order_id','!=',null]])->groupBy('order_id')->get();
        else if($from == null && $to != null)
            $orders = Order::where([['created_at','<=',$to.' 23:59:59'],['order_id','!=',null]])->groupBy('order_id')->get();
        else if($from != null && $to == null)
            $orders = Order::where([['created_at','>=',$from],['order_id','!=',null]])->groupBy('order_id')->get();
        else
            $orders = Order::where('order_id','!=',null)->groupBy('order_id')->get();

        $order_arr = array();
        $filename = 'OrderInvoice';
        $delimiter=",";
        $f=fopen($filename.'.csv',"w");//create a file pointer

    // Header row
        $fields = array("Date","Invoice","Customer","Email Id","Phone Number","Province (Tax %)","Status","Shipping","Tax",'Amount');
        fputcsv($f,$fields,$delimiter);
        $total_tax = 0;
        foreach ($orders as $order)
        {
            if(strpos($sel_status,$order->status)!== false || strpos($sel_status,'All') !== false)
            {
                $status= null;
                $subtotal = str_replace(',','',$order->orderFeesplitUp->sub_total);
                if($order->orderFeesplitUp->tax_value == null){
                    if($order->shipping_partner == 'Canada Post' && $order->shipping_code == '' ){
                        $taxed = ($subtotal - $order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100;
                    }
                    else
                        $taxed = ($subtotal+$order->orderFeesplitUp->shipping_charge - $order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100;
                }
                else
                     $taxed = $order->orderFeesplitUp->tax_value;
                if($order->status != 'Cancel & refunded')
                    $total_tax = $total_tax + $taxed;
                else
                {
                    $current_orders = Order::where('order_id',$order->order_id)->get();
                    $count = count($current_orders);
                    if($count > 1)
                    {
                        foreach ($current_orders as $current)
                        {
                            if($current->status != 'Cancel & refunded')
                            {
                                $total_tax = $total_tax + $taxed;
                                $status = $current->status;
                                break;
                            }
                        }
                    }
                }

                $date = date_format($order->created_at,'d-M-Y');
                $order_id = $order->order_id;
                $customer = $order->customer->firstname.' '.$order->customer->lastname;
                $email = $order->customer->email;
                $phone = $order->customer->telephone;
                $province = $order->billingAddress->province.' ('.$order->orderFeesplitUp->tax.'%)';
                if($status == null)
                    $status = $order->status;
                else $status =  $status;
                $shipping = 'C$ '.$order->orderFeesplitUp->shipping_charge;
                $tax = 'C$ '.number_format($taxed, 2, '.', ',');
                $total = 'C$ '.$order->order_total;

                $order_arr = array($date,$order_id,$customer,$email,$phone,$province,$status,$shipping,$tax,$total);
                // Write to file
                fputcsv($f,$order_arr);
            }

        }
        $order_arr = array('','','','','','','','Total Tax :','C$ '.number_format($total_tax, 2, '.', ','));
        fputcsv($f,$order_arr);

        fclose($f);

        // download
        header("Content-Description: File Transfer");
        //header("Content-Disposition: attachment; filename=$filename.'csv'");
        header("Content-Type: application/download; ");
        $headers = array(
            'Content-Type' => 'text/csv',
        );
        $path = $filename.'.csv';

        //return response()->download($path, $filename.'.csv')->deleteFileAfterSend(true);
         $path = asset($path);
        return response()->json(['success'=>'success','path'=>$path]);

    }
     public function download_pdf($order_id)
     {

         $orders = Order::where('order_id',$order_id)->get();
        $data = '<html lang="en">
<body>
<table role="presentation" align="center" border="0" cellspacing="0" cellpadding="10" width="100%" bgcolor="#edf0f3"
       style="background-color:#ffffff;table-layout:fixed;">
    <tbody>
    <tr>
        <td>
                <div
                    style="background-color:#ffffff;max-width:600px;margin:auto; box-shadow: 1px 1px 8px rgba(0,0,0,0.1);">

                    <table cellspacing="0" cellpadding="0" align="center"
                           style="width:100%;border:3px solid #fb3333; border-top-width:5px; border-left-width:3px;margin-top: 50px;">
                        <tbody>
                        <tr>
                            <td align="center" style="vertical-align:middle">
                                <div style="padding: 10px; background: #f8f8f8;">
                                    <table cellspacing="0" cellpadding="0" border="0" align="center"
                                           style="width:100%;">
                                        <tbody>
                                        <tr>
                                            <td align="left"
                                                style="vertical-align:middle;padding-top:10px;"
                                                bgcolor="#f8f8f8">
                                                <a href="https://jumbocanada.com"><img
                                                        src="https://jumbocanada.torontomalayali.ca/assets/img/logo.png"
                                                        alt="jumbocanada.com" height="35" style="display:block"></a></td>
                                            <td align="right"
                                                style="vertical-align:middle;"
                                                bgcolor="#f8f8f8">
                                                <table width="100%" cellpadding="0" cellspacing="0"
                                                       border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td align="right">
                                                            <table align="right" cellpadding="0"
                                                                   cellspacing="0" border="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="right"
                                                                        colspan="4"
                                                                        style=" padding: 5px 0; font-size: 11px;">
                                                                        <a
                                                                            href="mailto:support@jumbocanada.com">support@jumbocanada.com</a><br/>
                                                                            <a href="tel:+1 8557733844"> +1 (855) 773 3844</a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <table cellspacing="0" cellpadding="0" border="0" align="center"
                                       style="width:100%" bgcolor="#fff">
                                    <tbody>
                                    <tr>
                                        <td align="center" style="vertical-align:middle;height:10px"
                                            bgcolor="#ffffff">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"
                                            style="vertical-align:middle;padding:0 15px; font-size:13px!important;line-height:1.255!important; color: #696969"
                                            bgcolor="#ffffff">';
         foreach($orders as $order) {
             $data = $data.'<table
                    style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                    <thead>
                    <tr>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222"
                            colspan="2">Order Details
                 </td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                            <b>Order ID:</b> ' . $order->order_id . '<br>
                            <b>Date Added:</b>' . date("d-M-Y", strtotime($order->created_at)) . '<br>
                            <b>Payment Method:</b> Credit / Debit Card<br>
                            <b>Shipping Method:</b> Flat Shipping Rate
                 </td>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                            <b>E-mail:</b>
                            <a href="mailto:ggbranjithgmail.com" target="_blank">' . $order->customer->email . '</a><br>
                            <b>Telephone:</b>' . $order->customer->telephone . '<br>
                            <b>Order Status:</b> Complete<br>
                            <b>IP Address:</b>' . $order->ip_address . '
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table
                    style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                    <thead>
                    <tr>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                     Payment
                            Address
                            </td>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                     Shipping
                            Address
                            </td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">' .
                 $order->deliveryAddress->firstname . ' ' . $order->deliveryAddress->lastname . '<br>' .
                 $order->deliveryAddress->address_1 . ' ' . $order->deliveryAddress->address_2 . '<br>' .
                 $order->deliveryAddress->city . '<br>' .
                 $order->deliveryAddress->province . '<br>'.
                 $order->deliveryAddress->postcode . '<br>
                        </td>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">' .
                 $order->billingAddress->firstname . '  ' . $order->billingAddress->lastname . '<br>' .
                 $order->billingAddress->address_1 . ' ' . $order->billingAddress->address_2 . '<br>' .
                 $order->billingAddress->city . '<br>' .
                 $order->billingAddress->province . '<br>' .
                 $order->billingAddress->postcode . '<br>' .
                 '</td>
                    </tr>
                    </tbody>
                </table>';
             break;
         }
     $data= $data .'<table
            style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
            <thead>
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
             Product
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
             Model
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">
             Quantity
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">
             Price
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">
             Total
                </td>
            </tr>
            </thead>
            <tbody>';
         foreach($orders as $order)
         {
             $data = $data.'<tr>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.$order->product->name.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">'.$order->product->model.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">'.$order->quantity.'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">C$'.number_format($order->product_price,2).'</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                C$'.number_format($order->product_price*$order->quantity,2)
         .'</td>
        </tr>';
        }
        $data = $data. '</tbody>';
         foreach($orders as $order)
         {
             $data = $data.'<tfoot>
                <tr>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                        colspan="4"><b>Sub-Total:</b></td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                 C$'.$order->orderFeesplitUp->sub_total.
                             '</td>
                </tr>
                <tr>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                        colspan="4"><b> Shipping Charge:</b></td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                 C$'.number_format($order->orderFeesplitUp->shipping_charge,2).
                             '</td>
                </tr>';
             if($order->orderFeesplitUp->reduction!=0)
             {
                 $data = $data.'<tr>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                        colspan="4"><b>Discount :</b></td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                 C$'.number_format($order->orderFeesplitUp->reduction,2).
                                 '</td>
                </tr>';
             }

             if($order->orderFeesplitUp->tax_value == null){
                 if($order->shipping_partner == 'Canada Post' && $order->shipping_code == '' ){
                     $taxed = ($order->orderFeesplitUp->sub_total - $order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100;
                 }
                 else
                     $taxed = ($order->orderFeesplitUp->sub_total+$order->orderFeesplitUp->shipping_charge - $order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100;
             }
             else
                 $taxed = $order->orderFeesplitUp->tax_value;
             $data = $data.'<tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                    colspan="4"><b>Tax Rate ( '.$order->orderFeesplitUp->tax.'%):</b></td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                C$'.number_format(($taxed),2).
                         '</td>
                </tr>
                <tr>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                        colspan="4"><b>Total:</b></td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                 C$'.number_format(($order->order_total),2).
                             '</td>
                </tr>
                </tfoot>';
                break;
         }
         $data = $data.'</table>';
         $data = $data.' </td>
                           </tr>
                         </tbody>
                       </table>
                      </td>
                     </tr>
                    </tbody>
                   </table>
                </div>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>';
         $pdf = App::make('dompdf.wrapper');
         PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
         //$pdf->setPaper( 'A4' );
         $pdf->loadHTML($data);
         //$pdf->render( );

         return $pdf->download('OrderDetails.pdf');
     }
}
