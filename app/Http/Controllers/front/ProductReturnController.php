<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Product_registration;
use App\Product_return;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use App;

class ProductReturnController extends Controller
{
    public function productReturn(Request $request)
    {
        //dd($request);
        $data = $request->validate([
            'reg_num' => 'required',
            'dead' => 'required',
            'faulty' => 'nullable',
            'is_open' => 'required',
            'image1' => 'nullable',
        ]);
        //check if already raised return request
        $return = Product_return::where('reg_num', $request->reg_num)->first();
        if($return!=null)return Redirect::back()->with('error', 'You have already raised a return request and we are processing the same. Please contact our customer care for status update.');
        //check reg_num & email exists
        $registration = Product_registration::where([['reg_no', '=', $data['reg_num']],['email','=',$request->email]])->first();
        if ($registration == null) return Redirect::back()->with('error', 'Registration Number or email does not exists');
        if($registration->status!='Approved') return Redirect::back()->with('error', 'Product registration not approved. Please contact our customer care');
        //upload image
        $image_path = '';
        if ($request->image1) {
            $image_path = request('image1')->store('uploads/returns', 'public');
            $data['image1'] = $image_path;
        } else {
            $data['image1'] = '';
        }
        //create incremental return id
        $last_return_id = Product_return::orderBy('id', 'desc')->first();
        //dd($last_order_id->order_id);
        if ($last_return_id == '')
            $data['return_id'] = 1000;
        else
            $data['return_id'] = $last_return_id->return_id + 1;
        $return = Product_return::create($data);
        //send email to customer
        //dd($return);
        $email = $registration->email;
        //dd($email);
        Mail::send('front.emails.return-requested', ['return' => $return, 'registration' => $registration], function ($message) use ($email) {
            $message->to($email)
                ->subject('Jumbo Canada | Product return request receieved');
        });
        //send email to client
        $email = env('MAIL_TO');
        Mail::send('front.emails.client-return-request', ['return' => $return, 'registration' => $registration], function ($message) use ($email, $image_path) {
            $message->to($email)
                ->subject('New product return request')
                ->attach($image_path);
        });
        return Redirect::back()->with('success', 'Product return request submitted');
    }

    public function returnStatusUpdate(Request $request)
    {
        //validate security code
        if ($request->sec_code != md5($request->reg_num * 123456)) {
            return Redirect::back()->with('error', 'Security code not valid');
        }
        $return = Product_return::where('return_id', $request->reg_num)->first();
        //check whether the product is already replaced or rejected
        if ($return->status == 'Rejected' || $return->status == 'Replaced')
            return Redirect::back()->with('error', 'Product status already updated');
        $return->status = $request->status;
        $return->reject_reason = $request->comment;
        $return->save();
        //Send email to customer
        if ($return->status == 'Replaced') {
            $email = $return->product_registration->email;
            Mail::send('front.emails.return-request-approved-store', ['return' => $return], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | Product return | Replaced');
            });
            //to client
            $email = env('MAIL_TO');
            Mail::send('front.emails.client-return-request-approved-store', ['return' => $return], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Product return request accepted by store');
            });
            //send replacement summary as pdf to store
            $data = '<html lang="en">
            <body>
            <table role="presentation" align="center" border="0" cellspacing="0" cellpadding="10" width="100%" bgcolor="#edf0f3"
                   style="background-color:#ffffff;table-layout:fixed;">
                <tbody>
                <tr>
                    <td>
                            <div
                                style="background-color:#ffffff;max-width:600px;margin:auto; box-shadow: 1px 1px 8px rgba(0,0,0,0.1);">

                                <table cellspacing="0" cellpadding="0" align="center"
                                       style="width:100%;border:3px solid #fb3333; border-top-width:5px; border-left-width:3px;margin-top: 50px;">
                                    <tbody>
                                    <tr>
                                        <td align="center" style="vertical-align:middle">
                                            <div style="padding: 10px; background: #f8f8f8;">
                                                <table cellspacing="0" cellpadding="0" border="0" align="center"
                                                       style="width:100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td align="left"
                                                            style="vertical-align:middle;padding-top:10px;"
                                                            bgcolor="#f8f8f8">
                                                            <a href="https://ultrakitchenappliances.com"><img
                                                                    src="https://ultrakitchenappliances.com/assets/img/logo.png"
                                                                    alt="ultrakitchenappliances.com" height="35" style="display:block"></a></td>
                                                        <td align="right"
                                                            style="vertical-align:middle;"
                                                            bgcolor="#f8f8f8">
                                                            <table width="100%" cellpadding="0" cellspacing="0"
                                                                   border="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="right">
                                                                        <table align="right" cellpadding="0"
                                                                               cellspacing="0" border="0">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td align="right"
                                                                                    colspan="4"
                                                                                    style=" padding: 5px 0; font-size: 11px;">
                                                                                    <a
                                                                                        href="mailto:support@ultrakitchenappliances.com">support@ultrakitchenappliances.com</a><br/><a href="tel:+1 8557733844"> +1 (855) 773 3844</a>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </div>


                                            <table cellspacing="0" cellpadding="0" border="0" align="center"
                                                   style="width:100%" bgcolor="#fff">
                                                <tbody>
                                                <tr>
                                                    <td align="center" style="vertical-align:middle;height:10px"
                                                        bgcolor="#ffffff">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left"
                                                        style="vertical-align:middle;padding:0 15px; font-size:13px;line-height:1.255!important; color: #696969"
                                                        bgcolor="#ffffff">
                                                        <table align="middle" style="padding-bottom: 10px;">
                                                            <tr align="center pt-5" style="color:black;">
                                                                  <td align="left" style="vertical-align:middle;font-size: 15px;">
                                                                  Replaced on '.date("M d, Y", strtotime($return->updated_at)).'
                                                                   </td>
                                                                </tr>
                                                    </table>
                                                    <table
                                            style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                                            <thead>
                                                         <tr>
                                                            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                                            Product Details
                                                            </td>
                                                            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#fff;font-weight:bold;text-align:left;padding:7px;color:#222222">
                                                            Customer Details
                                                            </td>
                                                            </tr>
                                                             </thead>
                         <tbody>
                         <tr>
                            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">                            Product: '.$return->product_registration->product->name.'<br/>
                            Reg Id: '.$return->reg_num.'<br/>
                            Reg Date: '.date("M d, Y", strtotime($return->product_registration->created_at)).'<br/>
                            Purchase Date: '.date("M d, Y", strtotime($return->product_registration->purchase_date)).'<br/>
                            Purchased From: '.$return->product_registration->store->address.'<br/>
                            Serial No: '.$return->product_registration->serial_number.'<br/>
                            Product Code: '.$return->product_registration->product_code.'<br/>
                            Safety Code: '.$return->product_registration->safety_code.'<br/>
                            </td>

                            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">                            Name: '.$return->product_registration->first_name.' '.$return->product_registration->last_name.' <br/>
                            Phone: '.$return->product_registration->phone.'<br/>
                            Email: <a target="_blank"
                                      href="mailto:$return->product_registration->email">'. $return->product_registration->email.'</a><br/>
                            Address: '.$return->product_registration->address.','. $return->product_registration->city.'
                            , '.$return->product_registration->province.','. $return->product_registration->zipcode.'
                         </td>
                         </tr>
                       </tbody>';

                        $data = $data.'</table>';
                        $data = $data.' </td>
                                       </tr>
                                     </tbody>
                                   </table>
                            </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </body>
                    </html>';
                        $pdf = App::make('dompdf.wrapper');
                        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
                        $pdf->setPaper( 'A4' );
                        $pdf->loadHTML($data);
                        $filename = 'Replacement-summary-'.time().'.pdf';
            //email to store
            $email = $return->product_registration->store->email;
            Mail::send('front.emails.replacement-summary', ['return' => $return], function ($message) use ($email , $pdf) {
                $message->to($email)
                    ->subject('Jumbo Canada | Product Replacement Summary')
                    ->attachData($pdf->output(), 'Replacement-summary.pdf');
            });


            //end pdf
        } else {
            $email = $return->product_registration->email;
            Mail::send('front.emails.return-request-rejected-store', ['return' => $return], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | Product return | Rejected');
            });
            //to client
            $email = env('MAIL_TO');
            Mail::send('front.emails.client-return-request-rejected-store', ['return' => $return], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Product return request rejected by store');
            });
        }
        return Redirect::back()->with('success', 'Status updated');
    }

}
