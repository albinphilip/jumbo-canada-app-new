<?php

namespace App\Http\Controllers\front;

use App\Discount;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class DiscountController extends Controller
{
    public function create()
    {
        return view('front.discount');
    }
    public function store(Request $request)
    {
        //dd($request);
        $data =$request->validate([
            'name' => 'required',
            'email'=>'required|email|unique:discounts,email',
            'phone' => 'required',
            'city' => 'nullable',
            'address_1' => 'nullable',
            'address_2' => 'nullable',
            'province' => 'required',
            'postcode' => 'nullable',
            'current_status' => 'required',
            'proof' => 'required|mimes:pdf,jpeg,png,jpg',
        ]);
        //$data['status'] = 'New';
        $data['ip_address'] = $request->ip();
        //store proof
        if ($request->proof) {
            $file_path = request('proof')->store('uploads/discount', 'public');
            $data['proof'] = $file_path;
        } else {
            $data['proof'] = '';
        }
       $discount =  Discount::create($data);
        //mail to client and user
        $email = $discount->email;
        Mail::send('front.emails.discount-request', ['discount' => $discount], function ($message) use ($email) {
            $message->to($email)
                ->subject('Jumbo Canada | Exclusive Discount');

        });
        //send email to client
        $email = env('MAIL_TO');
        Mail::send('front.emails.client-discount-request', ['discount' => $discount], function ($message) use ($email,$discount,$file_path) {
            $message->to($email)
                ->subject($discount->name.'- Exclusive Discount request')
                ->attach($file_path);
        });
        return back()->with('success','Your request to avail exclusive discount submitted successfully!');
    }
}
