<?php

namespace App\Http\Controllers\front;

use App\Gcaptcha;
use App\Http\Controllers\Controller;
use App\Product;
use App\Product_registration;
use App\Product_repair_registration;
use App\Product_serialnumber;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;

class ProductRepairRegistrationController extends Controller
{
    public function create(Request $request)
    {
        $products = Product::all();
        $stores = Store::all();
        return view("front.product.product-repair-registration", ['products' => $products, 'stores' => $stores]);
    }

    public function store(Request $request)
    {

        //dd($request);
//        $resultJson = Gcaptcha::verifyCaptcha($request->get('recaptcha_response'));
//        if ($resultJson->success != true) {
//            return back()->with('error', 'Recaptcha error')->withInput();
//        }
//        if ($resultJson->score >= 0.3) {
            $data = $request->validate([
                'product_id' => 'required',
                'serial_number' => 'required',
                'safety_code' => 'required',
                'product_code' => 'required',
                'purchase_date' => 'required',
                'store_id' => 'required',
                'issue' => 'required',
                'first_name' => 'required',
                'last_name' => 'nullable',
                'provenance' => 'nullable',
                'city' => 'nullable',
                'address' => 'nullable',
                'zipcode' => 'nullable',
                'phone' => 'required',
                'email' => 'required'
            ]);
            $data['ip_address']=$request->ip();
            //get latest rma Number
            $rma = Product_repair_registration::OrderBy('id', 'desc')->first();
            if ($rma == '')
                $data['rma_num'] = 1772825210;
            else
                $data['rma_num'] = $rma->rma_num + 1;

            $find_product = Product_serialnumber::where([
                'product_id' => $request->product_id,
                'serial_number' => $request->serial_number,
                'safety_code' => $request->safety_code,
                'product_code' => $request->product_code])->first();

            /*if (empty($find_product)) {
                return back()->with('error', 'Given product not found.!')->withInput();
            }*/
                //Uploading and saving attached_file
                if ($request->attached_file) {
                    $image_path = request('attached_file')->store('uploads/product-repare-register', 'public');
                    $data['attached_file'] = $image_path;
                } else {
                    $data['attached_file'] = '';
                }
                Product_repair_registration::create($data);
                //sending email confirmation to customer
                $product = Product::find($request->product_id);
                $data['first_name'] = $data['first_name'];
                $data['product_name'] = $product->name;
                $email = $request->email;
                Mail::send('front.emails.product-repair-reg-ack', ['data' => $data], function ($message) use ($email) {
                    $message->to($email)
                        ->subject('Jumbo Canada | Repair Registration registration');
                });
                //mail to client
                $email = env('MAIL_TO');
                Mail::send('front.emails.client-product-repair-registration', ['data' => $data], function ($message) use ($email) {
                    $message->to($email)
                        ->subject('Jumbo Canada | New product repair registration');
                });
                return redirect(route('repair-product'))->with('success', 'Product repair registration completed successfully');


        }
   // }
}
