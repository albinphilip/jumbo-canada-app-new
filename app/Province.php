<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $guarded = [];

    public function product_registration()
    {
        return $this->belongsTo(Product_registration::class,'provenance','name');
    }
}
