<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $guarded=[];
    public function customers()
    {
        return $this->hasMany(Zone::class);
    }
}
