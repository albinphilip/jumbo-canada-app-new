<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    public function customer()
    {
        return $this->hasOne(Customer::class,'id','customer_id');
    }
    public function couponCode()
    {
        return $this->hasOne(CouponCode::class,'id','coupon_id');
    }
    public function deliveryAddress()
    {
        return $this->hasOne(Address::class,'id','delivery_address');
    }
    public function product()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }
    public  function orderFeesplitUp()
    {
        return $this->hasOne(OrderFee_splitup::class,'order_id','order_id');
    }
    public function billingAddress()
    {
        return $this->hasOne(Address::class,'id','billing_address');
    }
    public function returns()
    {
        return $this->hasOne(Product_return::class);
    }
}
