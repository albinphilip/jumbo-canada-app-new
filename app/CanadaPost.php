<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\ArrayToXml\ArrayToXml;
use Illuminate\Support\Facades\Log;


use Carbon\Carbon;
class CanadaPost extends Model
{

    public static function GetRates($datas)
    {
        Log::notice(json_encode($datas));

        $username = getenv('CANADA_POST_USERNAME');
        $password = getenv('CANADA_POST_PASSWORD');
        $mailedBy = getenv('CANADA_POST_MAILEDBY');
        $key = base64_encode(getenv('CANADA_POST_KEY'));

        $data = [
            'service_url' => getenv('CANADA_POST_SERVICE_RATE_URL'),
            'originPostalCode' => getenv('CANADA_POST_ORIGINPOSTALCODE'),
            'servicecode' => getenv('CANADA_POST_SERVICECODE'),
            'contractId' => getenv('CANADA_POST_CONTRACTID'),
        ];

        $data = array_merge( $data , $datas );

        $xmlRequest = <<<XML
        <?xml version="1.0" encoding="UTF-8"?>
        <mailing-scenario xmlns="http://www.canadapost.ca/ws/ship/rate-v4">
          <customer-number>{$mailedBy}</customer-number>
          <contract-id>{$data['contractId']}</contract-id>
          <options>
              <option>
                  <option-code>COV</option-code>
                  <option-amount>{$data['item-amount']}</option-amount>
              </option>
          </options>
          <parcel-characteristics>
            <weight>{$data['weight']}</weight>
            <dimensions>
                <length>{$data['length']}</length>
                <width>{$data['width']}</width>
                <height>{$data['height']}</height>
            </dimensions>
          </parcel-characteristics>
          <services>
            <service-code>{$data['servicecode']}</service-code>
          </services>
          <origin-postal-code>{$data['originPostalCode']}</origin-postal-code>
          <destination>
            <domestic>
              <postal-code>{$data['postalCode']}</postal-code>
            </domestic>
          </destination>
        </mailing-scenario>
        XML;

        $curl = curl_init($data['service_url']); // Create REST Request
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/vnd.cpc.ship.rate-v4+xml',
            'content-type: application/vnd.cpc.ship.rate-v4+xml',
            'Authorization: Basic ' . $key,
            'Accept-language: en',
        ));
        $curl_response = curl_exec($curl); // Execute REST Request
        if(curl_errno($curl)){
            return curl_error($curl);
            // echo 'Curl error: ' . curl_error($curl) . "\n";
        }

        curl_close($curl);

        libxml_use_internal_errors(true);
        $xml = simplexml_load_string('<root>' . preg_replace('/<\?xml.*\?>/','',$curl_response) . '</root>');
        if (!$xml) {
            return $curl_response;
            // echo 'Failed loading XML' . "\n";
            // echo $curl_response . "\n";
            // foreach(libxml_get_errors() as $error) {
            //     echo "\t" . $error->message;
            // }
        } else {
            if ($xml->{'price-quotes'} ) {
                $priceQuotes = $xml->{'price-quotes'}->children('http://www.canadapost.ca/ws/ship/rate-v4');
                if ( $priceQuotes->{'price-quote'} ) {

                    foreach ( $priceQuotes as $priceQuote ) {
                        return $priceQuote->{'price-details'}->{'due'};
                        // echo 'Service Name: ' . $priceQuote->{'service-name'} . "\n";
                        // echo 'Price: ' . $priceQuote->{'price-details'}->{'due'} . "\n\n";
                    }
                }
            }
            if ($xml->{'messages'} ) {
                $messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');

                foreach ( $messages as $message ) {
                    Log::notice($message->description);
                    // echo 'Error Code: ' . $message->code . "\n";
                    // echo 'Error Msg: ' . $message->description . "\n\n";
                }

                return 	$messages;
            }

        }
    }


    public static function CreateShipment($datas)
    {
        Log::notice(json_encode($datas));
        /**
         * This is for creating new shipping.
         */

        $username = getenv('CANADA_POST_USERNAME');
        $password = getenv('CANADA_POST_PASSWORD');
        $key = base64_encode(getenv('CANADA_POST_KEY'));



        $data = [
            'service_url' => getenv('CANADA_POST_SERVICE_RATE_URL'),
            'originPostalCode' => getenv('CANADA_POST_ORIGINPOSTALCODE'),
            'servicecode' => getenv('CANADA_POST_SERVICECODE')
        ];

        $data = [
            'service_url' => getenv('CANADA_POST_SHIPMENT_URL'),
            'originPostalCode' => getenv('CANADA_POST_ORIGINPOSTALCODE'),
            'transmit-shipment' => 'true',

            'servicecode' => getenv('CANADA_POST_SERVICECODE'),
            'groupId' => 'grp2021',
            'requestedShippingPoint' => getenv('CANADA_POST_ORIGINPOSTALCODE'),
            'contractId' => getenv('CANADA_POST_CONTRACTID'),

            'cpc-pickup-indicator' => 'true',

            'service-code' => getenv('CANADA_POST_SERVICECODE'),

            'sender-name' => getenv('CANADA_POST_SENDER_NAME'),
            'sender-company' => getenv('CANADA_POST_SENDER_COMPANY'),
            'sender-contact-phone' => getenv('CANADA_POST_SENDER_PHONE'),
            'sender-address-line-1' => getenv('CANADA_POST_SENDER_ADDRESS'),
            'sender-city' => getenv('CANADA_POST_SENDER_CITY'),
            'sender-prov-state' => getenv('CANADA_POST_SENDER_STATE'),
            'sender-country-code' => getenv('CANADA_POST_SENDER_COUNTRY'),

            'sender-postal-zip-code' => getenv('CANADA_POST_ORIGINPOSTALCODE'),

            'option-code' => 'COV',

            'notification-email' => getenv('MAIL_TO'),
            'on-shipment' => 'true',
            'on-exception' => 'true',
            'on-delivery' => 'true',

            'show-packing-instructions' => 'true',
            'show-postage-rate' => 'false',
            'show-insured-value' => 'true',

            // 'intended-method-of-payment' => 'SupplierAccount',
            'intended-method-of-payment' => 'Account'
        ];

        $data = array_merge( $data , $datas );

        $xmlRequest = <<<XML
        <?xml version="1.0" encoding="UTF-8"?>
        <shipment xmlns="http://www.canadapost.ca/ws/shipment-v8">

            <group-id>{$data['groupId']}</group-id>
            <!-- <transmit-shipment>{$data['transmit-shipment']}</transmit-shipment> -->
            <requested-shipping-point>{$data['requestedShippingPoint']}</requested-shipping-point>
            <cpc-pickup-indicator>{$data['cpc-pickup-indicator']}</cpc-pickup-indicator>
            <expected-mailing-date>{$data['mailingDate']}</expected-mailing-date>
            <delivery-spec>
                <service-code>{$data['service-code']}</service-code>
                    <sender>
                        <name>{$data['sender-name']}</name>
                        <company>{$data['sender-company']}</company>
                        <contact-phone>{$data['sender-contact-phone']}</contact-phone>
                        <address-details>
                            <address-line-1>{$data['sender-address-line-1']}</address-line-1>
                            <city>{$data['sender-city']}</city>
                            <prov-state>{$data['sender-prov-state']}</prov-state>
                            <country-code>{$data['sender-country-code']}</country-code>
                            <postal-zip-code>{$data['sender-postal-zip-code']}</postal-zip-code>
                        </address-details>
                    </sender>
                    <destination>
                        <name>{$data['destination-name']}</name>
                        <company>{$data['destination-company']}</company>
                        <address-details>
                            <address-line-1>{$data['destination-address-line-1']}</address-line-1>
                            <city>{$data['destination-city']}</city>
                            <prov-state>{$data['destination-prov-state']}</prov-state>
                            <country-code>{$data['destination-country-code']}</country-code>
                            <postal-zip-code>{$data['destination-postal-zip-code']}</postal-zip-code>
                        </address-details>
                    </destination>
                <options>
                    <option>
                        <option-code>{$data['option-code']}</option-code>
                        <option-amount>{$data['item-amount']}</option-amount>
                    </option>
                </options>
                <parcel-characteristics>
                    <weight>{$data['weight']}</weight>
                    <dimensions>
                        <length>{$data['length']}</length>
                        <width>{$data['width']}</width>
                        <height>{$data['height']}</height>
                    </dimensions>
                </parcel-characteristics>
                <notification>
                    <email>{$data['notification-email']}</email>
                    <on-shipment>{$data['on-shipment']}</on-shipment>
                    <on-exception>{$data['on-exception']}</on-exception>
                    <on-delivery>{$data['on-delivery']}</on-delivery>
                </notification>
                <preferences>
                    <show-packing-instructions>{$data['show-packing-instructions']}</show-packing-instructions>
                    <show-postage-rate>{$data['show-postage-rate']}</show-postage-rate>
                    <show-insured-value>{$data['show-insured-value']}</show-insured-value>
                </preferences>
                <references>
                    <customer-ref-1>{$data['reference-id']}</customer-ref-1>
                </references>
                <settlement-info>
                    <contract-id>{$data['contractId']}</contract-id>
                    <intended-method-of-payment>{$data['intended-method-of-payment']}</intended-method-of-payment>
                </settlement-info>
            </delivery-spec>
        </shipment>
        XML;



        $curl = curl_init($data['service_url']); // Create REST Request
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem'); // Signer Certificate in PEM format
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/vnd.cpc.shipment-v8+xml',
            'Accept: application/vnd.cpc.shipment-v8+xml',
            'Authorization: Basic ' . $key,
            'Accept-language: en',
        ));
        $curl_response = curl_exec($curl); // Execute REST Request
        if(curl_errno($curl)){
            return curl_error($curl);
            // echo 'Curl error: ' . curl_error($curl) . "\n";
        }

        // echo 'HTTP Response Status: ' . curl_getinfo($curl,CURLINFO_HTTP_CODE) . "\n";

        curl_close($curl);

        // Example of using SimpleXML to parse xml response
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string('<root>' . preg_replace('/<\?xml.*\?>/','',$curl_response) . '</root>');
        if (!$xml) {
            return $curl_response;
            // echo 'Failed loading XML' . "\n";
            // echo $curl_response . "\n";
            // foreach(libxml_get_errors() as $error) {
            //     echo "\t" . $error->message;
            // }
        } else {
            if ($xml->{'shipment-info'} ) {
                $shipment = $xml->{'shipment-info'}->children('http://www.canadapost.ca/ws/shipment-v8');
                if ( $shipment->{'shipment-id'} ) {
                    // return $shipment;
                    $return['shipment_id'] = $shipment->{'shipment-id'};
                    $return['tracking_pin'] = $shipment->{'tracking-pin'};
                    // echo  'Shipment Id: ' . $shipment->{'shipment-id'} . "\n";
                    foreach ( $shipment->{'links'}->{'link'} as $link ) {
                        // echo $link->attributes()->{'rel'} . ': ' . $link->attributes()->{'href'} . "\n";


                        if($link->attributes()->{'rel'} == 'price')
                        {
                            $datas = [
                                'service_url' => $link->attributes()->{'href'},
                            ];

                            $canadapost = CanadaPost::GetShipmentPrice($datas);

                            $return['price'] = $canadapost->{'due-amount'};
                        }

                        if($link->attributes()->{'rel'} == 'label')
                        {
                            $datas = [
                                'service_url' => $link->attributes()->{'href'},
                            ];

                            $return['file_path'] = CanadaPost::GetShipmentArtifact($datas);
                        }
                    }

                    $transmitData = [
                        'reference-id' => $data['reference-id']
                    ];

                    $return['manifest_file_path'] = CanadaPost::TransmitShipments($transmitData);

                    return $return;
                }
            }
            if ($xml->{'messages'} ) {
                $messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');
                foreach ( $messages as $message ) {
                    Log::notice($message->description);
                    // echo 'Error Code: ' . $message->code . "\n";
                    // echo 'Error Msg: ' . $message->description . "\n\n";
                }

                return 	$messages;
            }
        }

    }


    public static function GetShipmentPrice($data)
    {
        Log::notice(json_encode($data));
        /**
         * This is for getting the rate of created shipping.
         */

        $username = getenv('CANADA_POST_USERNAME');
        $password = getenv('CANADA_POST_PASSWORD');
        $key = base64_encode(getenv('CANADA_POST_KEY'));


        $curl = curl_init($data['service_url']); // Create REST Request
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem'); // Signer Certificate in PEM format
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept:application/vnd.cpc.shipment-v8+xml',
            'Accept-Language:en-CA',
            'Authorization: Basic ' . $key,
        ));
        $curl_response = curl_exec($curl); // Execute REST Request
        if(curl_errno($curl)){
            return curl_error($curl);
            // echo 'Curl error: ' . curl_error($curl) . "\n";
        }

        // echo 'HTTP Response Status: ' . curl_getinfo($curl,CURLINFO_HTTP_CODE) . "\n";

        curl_close($curl);

        // Example of using SimpleXML to parse xml response
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string('<root>' . preg_replace('/<\?xml.*\?>/','',$curl_response) . '</root>');
        if (!$xml) {
            return $curl_response;
            // echo 'Failed loading XML' . "\n";
            // echo $curl_response . "\n";
            // foreach(libxml_get_errors() as $error) {
            //     echo "\t" . $error->message;
            // }
        } else {
            if ($xml->{'shipment-price'} ) {
                $shipmentPrice = $xml->{'shipment-price'}->children('http://www.canadapost.ca/ws/shipment-v8');
                return $shipmentPrice;
                // if ( $shipmentPrice->{'service-code'} ) {
                //     echo 'Service Code: ' . $shipmentPrice->{'service-code'} . "\n";
                //     echo 'Due amount: ' . $shipmentPrice->{'due-amount'} . "\n";
                // }
            }
            if ($xml->{'messages'} ) {
                $messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');
                foreach ( $messages as $message ) {
                    Log::notice($message->description);
                    // echo 'Error Code: ' . $message->code . "\n";
                    // echo 'Error Msg: ' . $message->description . "\n\n";
                }

                return 	$messages;
            }
        }
    }


    public static function GetShipmentArtifact($data)
    {
        Log::notice(json_encode($data));
        /*
         * This is for getting the label of created shipping.
         */

        $username = getenv('CANADA_POST_USERNAME');
        $password = getenv('CANADA_POST_PASSWORD');
        $key = base64_encode(getenv('CANADA_POST_KEY'));

        $curl = curl_init($data['service_url']); // Create REST Request
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem'); // Mozilla cacerts
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept:application/pdf,application/zpl',
            'Content-Type: application/vnd.cpc.shipment-v8+xml',
            'Accept-Language:en-CA',
            'Authorization: Basic ' . $key,
        ));
        $curl_response = curl_exec($curl); // Execute REST Request
        if(curl_errno($curl)){
            return curl_error($curl);
            // echo 'Curl error: ' . curl_error($curl) . "\n";
        }

        // echo 'HTTP Response Status: ' . curl_getinfo($curl,CURLINFO_HTTP_CODE) . "\n";

        $contentType =  curl_getinfo($curl,CURLINFO_CONTENT_TYPE);
        $filePath = "";
        if ( strpos($contentType, 'application/pdf' ) !== FALSE ) {
            // Writing binary response to file
            // $fileName = 'shipmentArtifact.pdf';

            $now = Carbon::now();
            $time = $now->format('Y-m-d-H-i-s');
            // Writing binary response to file

            // $fileName = 'h.pdf';

            $fileName = 'label' . $time . '.pdf';





            $filePath = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . DIRECTORY_SEPARATOR . $fileName;
            // echo 'Writing response to ' . $filePath . "\n";
            file_put_contents($filePath, $curl_response);

        } elseif ( strpos($contentType, 'application/zpl' ) !== FALSE ) {
            // Writing binary response to file
            $fileName = 'shipmentArtifact.zpl';
            $filePath = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . DIRECTORY_SEPARATOR . $fileName;
            // echo 'Writing response to ' . $filePath . "\n";
            file_put_contents($filePath, $curl_response);
        } elseif (strpos($contentType, 'xml' ) > -1 ) {
            // Example of using SimpleXML to parse xml error response
            libxml_use_internal_errors(true);
            $xml = simplexml_load_string('<root>' . preg_replace('/<\?xml.*\?>/','',$curl_response) . '</root>');
            if (!$xml) {
                return $curl_response;
                // echo 'Failed loading XML' . "\n";
                // echo $curl_response . "\n";
                // foreach(libxml_get_errors() as $error) {
                //     echo "\t" . $error->message;
                // }
            } else {
                if ($xml->{'messages'} ) {
                    $messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');
                    foreach ( $messages as $message ) {
                        Log::notice($message->description);
                        // echo 'Error Code: ' . $message->code . "\n";
                        // echo 'Error Msg: ' . $message->description . "\n\n";
                    }

                    return 	$messages;
                }
            }
        } else {
            return 'Unknown Content Type';
            // echo 'Unknown Content Type: ' . $contentType . "\n";
        }

        curl_close($curl);
        return $filePath;
    }

    public static function GetTrackingSummary($pin)
    {
        Log::notice(json_encode($pin));
        /**
         * This is for tracking.
         */

        $username = getenv('CANADA_POST_USERNAME');
        $password = getenv('CANADA_POST_PASSWORD');
        $key = base64_encode(getenv('CANADA_POST_KEY'));


        $data = [
            'service_url' => getenv('CANADA_POST_TRACKING_URL').$pin.'/summary'
        ];

        $curl = curl_init($data['service_url']); // Create REST Request
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem'); // Mozilla cacerts
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept:application/vnd.cpc.track+xml',
            'Accept-Language:en-CA',
            'Authorization: Basic ' . $key,
        ));
        $curl_response = curl_exec($curl); // Execute REST Request
        if(curl_errno($curl)){
            return curl_error($curl);
            // echo 'Curl error: ' . curl_error($curl) . "\n";
        }

        // echo 'HTTP Response Status: ' . curl_getinfo($curl,CURLINFO_HTTP_CODE) . "\n";

        curl_close($curl);

        // Example of using SimpleXML to parse xml response
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($curl_response);
        if (!$xml) {
            return $curl_response;
            // echo 'Failed loading XML' . "\n";
            // echo $curl_response . "\n";
            // foreach(libxml_get_errors() as $error) {
            //     echo "\t" . $error->message;
            // }
        } else {

            $trackingSummary = $xml->children('http://www.canadapost.ca/ws/track');

            if ( $trackingSummary->{'pin-summary'} ) {
                return $trackingSummary;
                // foreach ( $trackingSummary as $pinSummary ) {
                //     echo 'PIN Number: ' . $pinSummary->{'pin'} . "\n";
                //     echo 'Mailed On Date: ' . $pinSummary->{'mailed-on-date'} . "\n";
                //     echo 'Event Description: ' . $pinSummary->{'event-description'} . "\n\n";
                // }
            } else {
                $messages = $xml->children('http://www.canadapost.ca/ws/messages');
                if(empty($messages)){
                    return $trackingSummary;
                }
                foreach ( $messages as $message ) {
                    Log::notice($message->description);
                    // echo 'Error Code: ' . $message->code . "\n";
                    // echo 'Error Msg: ' . $message->description . "\n\n";
                }

                return 	$messages;
            }
        }

    }


    public static function GetTrackingDetails($pin)
    {
        Log::notice(json_encode($pin));
        /**
         * This is for getting the Receipt of created shipping.
         */

        $username = getenv('CANADA_POST_USERNAME');
        $password = getenv('CANADA_POST_PASSWORD');
        $key = base64_encode(getenv('CANADA_POST_KEY'));


        $data = [
            'service_url' => getenv('CANADA_POST_TRACKING_URL').$pin.'/details'
        ];

        $curl = curl_init($data['service_url']); // Create REST Request
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem'); // Mozilla cacerts
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept:application/vnd.cpc.track+xml',
            'Accept-Language:en-CA',
            'Authorization: Basic ' . $key,
        ));
        $curl_response = curl_exec($curl); // Execute REST Request
        if(curl_errno($curl)){
            return curl_error($curl);
            // echo 'Curl error: ' . curl_error($curl) . "\n";
        }

        // echo 'HTTP Response Status: ' . curl_getinfo($curl,CURLINFO_HTTP_CODE) . "\n";

        curl_close($curl);

        // Example of using SimpleXML to parse xml response
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($curl_response);
        if (!$xml) {
            return $curl_response;
            // echo 'Failed loading XML' . "\n";
            // echo $curl_response . "\n";
            // foreach(libxml_get_errors() as $error) {
            //     echo "\t" . $error->message;
            // }
        } else {

            $trackingDetail = $xml->children('http://www.canadapost.ca/ws/track');

            if ( $trackingDetail->{'significant-events'} ) {
                $items =  $trackingDetail->{'significant-events'}->{'occurrence'};

                foreach ( $items as $item ) {
                    $itemData = [
                        'date' => $item->{'event-date'},
                        'time' => $item->{'event-time'},
                        'description' => $item->{'event-description'},
                        'site' => $item->{'event-site'},
                        'province' => $item->{'event-province'},
                    ];

                    $returnData[] = $itemData;
                }

                return $returnData;
            }


            if ( $trackingDetail->{'pin'} ) {
                return $trackingDetail;
                // echo 'PIN Number: ' . $trackingDetail->{'pin'} . "\n";
                // echo 'Signature Exists: ' . $trackingDetail->{'signature-image-exists'} . "\n";
                // echo 'Suppress Signature: ' . $trackingDetail->{'suppress-signature'} . "\n";
            } else {
                $messages = $xml->children('http://www.canadapost.ca/ws/messages');
                if(empty($messages)){
                    return $trackingDetail;
                }
                foreach ( $messages as $message ) {
                    Log::notice($message->description);
                    // echo 'Error Code: ' . $message->code . "\n";
                    // echo 'Error Msg: ' . $message->description . "\n\n";
                }

                return 	$messages;
            }
        }
    }

// the end
// return label
    public static function returnLabel($returner)

    {
        Log::notice(json_encode($returner));

        $username = getenv('CANADA_POST_USERNAME');
        $password = getenv('CANADA_POST_PASSWORD');
        $mailedBy = getenv('CANADA_POST_MAILEDBY');
        $mobo = getenv('CANADA_POST_MOBO');
        $service_url = getenv('CANADA_POST_SERVICE_URL');
        $contractId = getenv('CANADA_POST_CONTRACTID');
        $key = base64_encode(getenv('CANADA_POST_KEY'));
        //$key = base64_encode('6e93d53968881714:0bfa9fcb9853d1f51ee57a');

        // send data

        $name = $returner['name'];
        // $company = $returner['company'];
        $service_code =  $returner['service_code'];
        $province =    $returner['province'];
        $city =    $returner['city'];
        $address =   $returner['address'];
        $postal_code = $returner['postal_code'];
        $weight = $returner['weight'];
        $length = $returner['length'];
        $breadth = $returner['breadth'];
        $height = $returner['height'];
        //  code for convert data to xml
        $array = [
            'service-code' => $service_code,
            'returner' => [
                'name' => $name,
                //'company' => $company,
                'domestic-address' => [
                    'address-line-1' => $address,
                    'city' => $city,
                    'province' => $province,
                    'postal-code' => $postal_code,
                ],//domestic
            ],//returner
            'receiver' => [
                'name' => 'Customer Care',
                'company' => 'Ultralinks Distribution Inc',
                'domestic-address' => [
                    'address-line-1' => '88 Aspen Hills Road',
                    'city' => 'Brampton',
                    'province' => 'ON',
                    'postal-code' =>'L6Y6E4',
                ],//domestic
            ],//receiver

            'parcel-characteristics' => [
                'weight' => $weight,
                'dimensions' => [
                    'length'=>$length,
                    'width'=>$breadth,
                    'height'=>$height],
            ],//parcel
            'print-preferences' => [
                'encoding' => 'PDF',
            ],//print
            'settlement-info' => [
                'contract-id' => $contractId,
            ],//settlement
        ];//main
        $xmlRequest = ArrayToXml::convert($array, [
            'rootElementName' => 'authorized-return',
            '_attributes' => [
                'xmlns' => 'http://www.canadapost.ca/ws/authreturn-v2',
            ]
        ]);

        $curl = curl_init($service_url); // Create REST Request
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '../storage/app/public/cacert.pem'); // Signer Certificate in PEM format
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/vnd.cpc.authreturn-v2+xml',
            'content-type: application/vnd.cpc.authreturn-v2+xml',
            'Authorization: Basic ' . $key,
            'Accept-language: en',
        ));

        $curl_response = curl_exec($curl); // Execute REST Request

        if (curl_errno($curl)) {
            echo 'Curl error: ' . curl_error($curl) . "\n";
        }
        //echo 'HTTP Response Status: ' . curl_getinfo($curl, CURLINFO_HTTP_CODE) . "\n";
        curl_close($curl);
        // Example of using SimpleXML to parse xml response
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string('<root>' . preg_replace('/<\?xml.*\?>/', '', $curl_response) . '</root>');

        if (!$xml) {
            echo 'Failed loading XML' . "\n";
            echo $curl_response . "\n";
            foreach (libxml_get_errors() as $error) {
                echo "\t" . $error->message;
            }
        } else {
            if ($xml->{'authorized-return-info'}) {
                $authreturn = $xml->{'authorized-return-info'}->children('http://www.canadapost.ca/ws/authreturn-v2');
                if ($authreturn->{'tracking-pin'}) {
                    $tracking_pin =  'Tracking Pin: ' . $authreturn->{'tracking-pin'} . "\n";
                    foreach ($authreturn->{'links'}->{'link'} as $link) {
                        $rel =$link->attributes()->{'href'};
                        // $rel = $link->attributes()->{'rel'} . ': ' . $link->attributes()->{'href'} . "\n";

                        // get manifest start
                        $service_url = $rel;
                        $curl = curl_init($service_url); // Create REST Request
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);

                        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                            'Accept:application/pdf,application/zpl',
                            'Authorization: Basic ' . $key,
                            'Accept-Language: en'

                        ));

                        $curl_response = curl_exec($curl); // Execute REST Request
                        if (curl_errno($curl)) {
                            echo 'Curl error: ' . curl_error($curl) . "\n";
                        }

                        //echo 'HTTP Response Status: ' . curl_getinfo($curl, CURLINFO_HTTP_CODE) . "\n";

                        $contentType =  curl_getinfo($curl, CURLINFO_CONTENT_TYPE);

                        $now = Carbon::now();
                        $time = $now->format('Y-m-d-H-i-s'); //14:15:16

                        if (strpos($contentType, 'application/pdf') !== FALSE) {

                            // Writing binary response to file

                            $fileName = 'Return-label'.$time.'.pdf';
                            // $filePath = realpath(dirname($path)) .$fileName;PostController
                            $filePath =realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . DIRECTORY_SEPARATOR . 'report/'.$fileName;
                            file_put_contents($filePath, $curl_response);

                            $return_data = [
                                'tracking_pin' => $authreturn->{'tracking-pin'}[0],
                                'file_path' => $filePath
                            ];

                            if ($authreturn->{'public-key-info'}) {
                                $return_data['expiry_date'] = $authreturn->{'public-key-info'}->{'expiry-date'};
                            }
                            return $return_data;

                        } elseif (strpos($contentType, 'application/zpl') !== FALSE) {
                            // Writing binary response to file
                            $fileName = $time.'.zpl';
                            $filePath = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . DIRECTORY_SEPARATOR . $fileName;
                            echo 'Writing response to ' . $filePath . "\n";
                            file_put_contents($filePath, $curl_response);
                        } elseif (strpos($contentType, 'xml') > -1) {
                            // Example of using SimpleXML to parse xml error response
                            libxml_use_internal_errors(true);
                            $xml = simplexml_load_string('<root>' . preg_replace('/<\?xml.*\?>/', '', $curl_response) . '</root>');
                            if (!$xml) {
                                echo 'Failed loading XML' . "\n";
                                echo $curl_response . "\n";
                                foreach (libxml_get_errors() as $error) {
                                    echo "\t" . $error->message;
                                }
                            } else {
                                if ($xml->{'messages'}) {
                                    $messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');
                                    foreach ($messages as $message) {
                                        Log::notice($message->description);
                                        echo 'Error Code: ' . $message->code . "\n";
                                        echo 'Error Msg: ' . $message->description . "\n\n";
                                    }
                                }
                            }
                        } else {
                            echo 'Unknown Content Type: ' . $contentType . "\n";
                        }

                        curl_close($curl);
                        // get manifest end

                    }
                }
            }
            if ($xml->{'messages'}) {
                $messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');
                foreach ($messages as $message) {
                    Log::notice($message->description);
                    echo 'Error Code: ' . $message->code . "\n";
                    echo 'Error Msg: ' . $message->description . "\n\n";
                }
            }
        }
    }

    public static function TransmitShipments($data)
    {

        Log::notice(json_encode($data));

        $username = getenv('CANADA_POST_USERNAME');
        $password = getenv('CANADA_POST_PASSWORD');
        $mailedBy = getenv('CANADA_POST_MAILEDBY');
        $mobo = getenv('CANADA_POST_MOBO');
        $url = getenv('CANADA_POST_SERVICE_RATE_URL');
        $contractId = getenv('CANADA_POST_CONTRACTID');
        $key = base64_encode(getenv('CANADA_POST_KEY'));

        $datas = [

            'groupId' => 'grp2021',

            'service_url' => getenv('CANADA_POST_TRANSMIT_SHIPMENT_URL'),
            'requestedShippingPoint' => getenv('CANADA_POST_ORIGINPOSTALCODE'),
            'cpc-pickup-indicator' => 'true',
            'detailed-manifests' => 'true',
            'method-of-payment' => 'Account',
            'manifest-company' => getenv('CANADA_POST_SENDER_COMPANY'),
            'phone-number'=> getenv('CANADA_POST_SENDER_PHONE'),
            'address-line-1' => getenv('CANADA_POST_SENDER_ADDRESS'),
            'city' =>  getenv('CANADA_POST_SENDER_CITY'),
            'prov-state' => getenv('CANADA_POST_SENDER_STATE'),
            'country-code' => getenv('CANADA_POST_SENDER_COUNTRY'),
            'postal-zip-code' => getenv('CANADA_POST_ORIGINPOSTALCODE')
        ];

        $data = array_merge( $data , $datas );

        $xmlRequest = <<<XML
        <?xml version="1.0" encoding="UTF-8"?>
        <transmit-set xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.canadapost.ca/ws/manifest-v8" >
        <group-ids>
            <group-id>{$data['groupId']}</group-id>
        </group-ids>
        <requested-shipping-point>{$data['requestedShippingPoint']}</requested-shipping-point>
        <cpc-pickup-indicator>{$data['cpc-pickup-indicator']}</cpc-pickup-indicator>
        <detailed-manifests>{$data['detailed-manifests']}</detailed-manifests>
        <method-of-payment>{$data['method-of-payment']}</method-of-payment>
        <manifest-address>
            <manifest-company>{$data['manifest-company']}</manifest-company>
            <phone-number>{$data['phone-number']}</phone-number>
            <address-details>
            <address-line-1>{$data['address-line-1']}</address-line-1>
            <city>{$data['city']}</city>
            <prov-state>{$data['prov-state']}</prov-state>
            <country-code>{$data['country-code']}</country-code>
            <postal-zip-code>{$data['postal-zip-code']}</postal-zip-code>
            </address-details>
        </manifest-address>
        <customer-reference>{$data['reference-id']}</customer-reference>

        </transmit-set>
        XML;

        $curl = curl_init($data['service_url']); // Create REST Request
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem'); // Signer Certificate in PEM format
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/vnd.cpc.manifest-v8+xml',
            'Authorization: Basic ' . $key,
            'Accept: application/vnd.cpc.manifest-v8+xml'
        ));
        $curl_response = curl_exec($curl); // Execute REST Request
        if(curl_errno($curl)){
            return curl_error($curl);
            // echo 'Curl error: ' . curl_error($curl) . "\n";
        }

        // echo 'HTTP Response Status: ' . curl_getinfo($curl,CURLINFO_HTTP_CODE) . "\n";

        curl_close($curl);

        // Example of using SimpleXML to parse xml response
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string('<root>' . preg_replace('/<\?xml.*\?>/','',$curl_response) . '</root>');
        if (!$xml) {
            return $curl_response;
            // echo 'Failed loading XML' . "\n";
            // echo $curl_response . "\n";
            // foreach(libxml_get_errors() as $error) {
            //     echo "\t" . $error->message;
            // }
        } else {
            if ($xml->{'manifests'} ) {
                $manifest = $xml->{'manifests'}->children('http://www.canadapost.ca/ws/manifest-v8');
                if ( $manifest->{'link'} ) {
                    foreach ( $manifest->{'link'} as $link ) {
                        $data = [
                            'service_url' => $link->attributes()->{'href'},
                        ];
                        // echo 'test1';

                        $file_path = CanadaPost::GetManifest($data);

                        // echo $file_path;
                        // return $link->attributes()->{'href'};
                    }
                }
            }
            if ($xml->{'messages'} ) {
                $messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');
                foreach ( $messages as $message ) {
                    Log::notice($message->description);
                    //     echo 'Error Code: ' . $message->code . "\n";
                    //     echo 'Error Msg: ' . $message->description . "\n\n";
                }

                return $messages;

            }
        }

        return $file_path;
    }



    public static function GetManifest($data)
    {
        Log::notice(json_encode($data));

        /**
         * This is for getting the Receipt of created shipping.
         */

        $username = getenv('CANADA_POST_USERNAME');
        $password = getenv('CANADA_POST_PASSWORD');
        $mailedBy = getenv('CANADA_POST_MAILEDBY');
        $mobo = getenv('CANADA_POST_MOBO');
        $url = getenv('CANADA_POST_SERVICE_RATE_URL');
        $contractId = getenv('CANADA_POST_CONTRACTID');
        $key = base64_encode(getenv('CANADA_POST_KEY'));


        $curl = curl_init($data['service_url']); // Create REST Request
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem'); // Signer Certificate in PEM format
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept:application/vnd.cpc.manifest-v8+xml',
            'Authorization: Basic ' . $key,
        ));
        $curl_response = curl_exec($curl); // Execute REST Request
        if(curl_errno($curl)){
            return curl_error($curl);
            // echo 'Curl error: ' . curl_error($curl) . "\n";
        }

        // echo 'HTTP Response Status: ' . curl_getinfo($curl,CURLINFO_HTTP_CODE) . "\n";

        curl_close($curl);

        // Example of using SimpleXML to parse xml response
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string('<root>' . preg_replace('/<\?xml.*\?>/','',$curl_response) . '</root>');
        if (!$xml) {
            return $curl_response;
            // echo 'Failed loading XML' . "\n";
            // echo $curl_response . "\n";
            // foreach(libxml_get_errors() as $error) {
            //     echo "\t" . $error->message;
            // }
        } else {
            if ($xml->{'manifest'} ) {
                $manifest = $xml->{'manifest'}->children('http://www.canadapost.ca/ws/manifest-v8');
                if ( $manifest->{'po-number'} ) {
                    // return $manifest;
                    // echo 'PO Number: ' . $manifest->{'po-number'} . "\n";


                    foreach ( $manifest->{'links'}->{'link'} as $link ) {

                        if($link->attributes()->{'rel'} == 'artifact')
                        {
                            $data = [
                                'service_url' => $link->attributes()->{'href'},
                            ];

                            $file_path = CanadaPost::GetManifestArtifact($data);
                        }
                    }


                    return $file_path;
                }
            }
            if ($xml->{'messages'} ) {
                $messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');
                foreach ( $messages as $message ) {
                    Log::notice($message->description);
                    //     echo 'Error Code: ' . $message->code . "\n";
                    //     echo 'Error Msg: ' . $message->description . "\n\n";
                }

                return $messages;
            }
        }
    }




    public static function GetManifestArtifact($data)
    {
        /*
         * This is for getting the manifest of created shipping.
         */

        $username = getenv('CANADA_POST_USERNAME');
        $password = getenv('CANADA_POST_PASSWORD');
        $mailedBy = getenv('CANADA_POST_MAILEDBY');
        $mobo = getenv('CANADA_POST_MOBO');
        $url = getenv('CANADA_POST_SERVICE_RATE_URL');
        $contractId = getenv('CANADA_POST_CONTRACTID');
        $key = base64_encode(getenv('CANADA_POST_KEY'));

        $curl = curl_init($data['service_url']); // Create REST Request
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem'); // Mozilla cacerts
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept:application/pdf',
            'Accept-Language:en-CA',
            'Authorization: Basic ' . $key,
        ));
        $curl_response = curl_exec($curl); // Execute REST Request
        if(curl_errno($curl)){
            return curl_error($curl);
            // echo 'Curl error: ' . curl_error($curl) . "\n";
        }

        // echo 'HTTP Response Status: ' . curl_getinfo($curl,CURLINFO_HTTP_CODE) . "\n";

        $contentType =  curl_getinfo($curl,CURLINFO_CONTENT_TYPE);

        $now = Carbon::now();
        $time = $now->format('Y-m-d-H-i-s');

        if ( strpos($contentType, 'application/pdf' ) !== FALSE ) {
            // Writing binary response to file

            $fileName = 'manifest' . $time . '.pdf';
            $filePath = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . DIRECTORY_SEPARATOR . $fileName;
            // echo 'Writing response to ' . $filePath . "\n";
            file_put_contents($filePath, $curl_response);
        } elseif (strpos($contentType, 'xml' ) > -1 ) {
            // Example of using SimpleXML to parse xml error response
            libxml_use_internal_errors(true);
            $xml = simplexml_load_string('<root>' . preg_replace('/<\?xml.*\?>/','',$curl_response) . '</root>');
            if (!$xml) {
                return $curl_response;
                // echo 'Failed loading XML' . "\n";
                // echo $curl_response . "\n";
                // foreach(libxml_get_errors() as $error) {
                //     echo "\t" . $error->message;
                // }
            } else {
                if ($xml->{'messages'} ) {
                    $messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');
                    foreach ( $messages as $message ) {
                        Log::notice($message->description);
                        //     echo 'Error Code: ' . $message->code . "\n";
                        //     echo 'Error Msg: ' . $message->description . "\n\n";
                    }

                    return $messages;
                }
            }
        } else {
            return 'Unknown Content Type';
            // echo 'Unknown Content Type: ' . $contentType . "\n";
        }

        curl_close($curl);

        return $filePath;

    }

    public static function GetGroups()
    {

        $username = getenv('CANADA_POST_USERNAME');
        $password = getenv('CANADA_POST_PASSWORD');
        $mailedBy = getenv('CANADA_POST_MAILEDBY');
        $mobo = getenv('CANADA_POST_MOBO');
        $url = getenv('CANADA_POST_SERVICE_RATE_URL');

        $key = base64_encode(getenv('CANADA_POST_KEY'));
        // REST URL
        $service_url = 'https://ct.soa-gw.canadapost.ca/rs/' . $mailedBy . '/' . $mobo . '/group';

        $curl = curl_init($service_url); // Create REST Request
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem'); // Signer Certificate in PEM format
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept-Language:en-CA',
            'Authorization: Basic ' . $key,
            'Accept:application/vnd.cpc.shipment-v8+xml',
        ));
        $curl_response = curl_exec($curl); // Execute REST Request
        if(curl_errno($curl)){
            echo 'Curl error: ' . curl_error($curl) . "\n";
        }

        echo 'HTTP Response Status: ' . curl_getinfo($curl,CURLINFO_HTTP_CODE) . "\n";

        curl_close($curl);

        // Example of using SimpleXML to parse xml response
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string('<root>' . preg_replace('/<\?xml.*\?>/','',$curl_response) . '</root>');
        if (!$xml) {
            echo 'Failed loading XML' . "\n";
            echo $curl_response . "\n";
            foreach(libxml_get_errors() as $error) {
                echo "\t" . $error->message;
            }
        } else {
            if ($xml->{'groups'} ) {
                $groups = $xml->{'groups'}->children('http://www.canadapost.ca/ws/shipment-v8');
                if ( $groups->{'group'} ) {
                    foreach ( $groups->{'group'} as $group ) {
                        // echo $group->{'link'}->attributes()->{'rel'} . ': ' . $group->{'link'}->attributes()->{'href'} . "\n";
                        echo $group->{'group-id'}. "\t";
                    }
                } else {
                    echo 'No groups returned.' . "\n";
                }
            }
            if ($xml->{'messages'} ) {
                $messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');
                foreach ( $messages as $message ) {
                    echo 'Error Code: ' . $message->code . "\n";
                    echo 'Error Msg: ' . $message->description . "\n\n";
                }
            }
        }
    }
// the end

}

