@extends('front.layouts.app')
@section('title','Login | Jumbo Canada')
@section('description','Login. to access your account, please identify yourself by providing the information requested in the fields below, then click Login.')
@section('keywords','canada kitchen store,canada kitchen supply,canada online kitchen store,cookware online shopping,kitchen accessories online shopping,kitchen accessories store,kitchen shop online shopping,kitchen supplies online canada')
@section('canonical','https://jumbokitchenappliances.com/customer-login')
@section('content')
    <div class="profile page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <ul class="profile-links">
                        <li><a href="{{route('profile')}}">My Profile</a></li>
                        <li><a href="{{route('address')}}">My Address Book</a></li>
                        <li><a href="{{route('edit-profile')}}">Edit Account</a></li>
                        <li><a href="{{route('password')}}">Password</a></li>
                        <li><a href="">My Wish List</a></li>
                        <li><a href="">Order History</a></li>
                    </ul>
                </div>
                <div class="col-sm-9">
                    <h1>Log In</h1>
                    <div class="profile-inner">
                        <p>New users please <a href="{{route('register-user')}}" style="color: darkblue;">create an
                                account </a></p>
                        <form class="form-horizontal" method="POST" action="{{ route('post-login') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-5">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-5">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-check">
                                    <label class="form-check-label">
                                    <input type="checkbox" onclick="togglePassword()" class="form-check-input"> Show Password
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" name="recaptcha_response" id="recaptchaResponse">

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <input type="submit" value="Login">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <a class="reset_pass" href="{{route('password.forgot')}}" >Forgot password?</a>
                                </div>
                            </div>
                            @if(Session::has('message'))
                                <p style="color:red;">{{Session::get('message')}}</p>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", {action: "Login"}).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
     <script defer>
    function togglePassword() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
        }
    </script>

    <script src="https://www.google.com/recaptcha/api.js?render={{env('GOOGLE_RECAPTCHA_KEY')}}"></script>
    <script>var recptchaKey = "{{env('GOOGLE_RECAPTCHA_KEY')}}"</script>
    <style>
        .grecaptcha-badge {
            visibility: hidden;
        }
    </style>
    <!-- RECAPTCHA -->
@endsection
