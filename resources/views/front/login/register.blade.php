@extends('front.layouts.app')
@section('title','Sign Up | Jumbo Canada')
@section('description','Join the Jumbo family! Sign up to see the latest catalogue on home appliances, updates, unique offers and exciting deals.')
@section('keywords','kitchen shop online shopping,kitchen supplies online canada,kitchen products,kitchen online shop,kitchen supplies shop,kitchen supply store canada,online store for kitchen accessories')
@section('canonical','https://jumbokitchenappliances.com/customer-signup')
@section('content')
    <div class="profile page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <ul class="profile-links">
                        <li><a href="{{route('profile')}}">My Profile</a></li>
                        <li><a href="{{route('address')}}">My Address Book</a></li>
                        <li><a href="{{route('edit-profile')}}">Edit Account</a></li>
                        <li><a href="{{route('password')}}">Password</a></li>
                        <li><a href="">My Favourites Lists</a></li>
                        <li><a href="">Order History</a></li>
                    </ul>
                </div>
                <div class="col-sm-9">
                    @if(Session::has('message'))
                        <p style="color:red;">{{Session::get('message')}}</p>
                    @endif
                    <h1>Register Account</h1>
                    <div class="profile-inner">
                        <p>Existing users please login  <a href="{{route('customerlogin')}}">here.</a></p>
                        <form action="{{route('postregistration')}}" method="POST">
                            @csrf
                            <h3>Your Personal Details</h3>
                            <div class="row">
                                <div class="col-sm-6"><input type="text" placeholder="First Name"
                                                             class="form-control {{ $errors->has('firstname') ? 'has-error' : ''}}" name="firstname" required value="{{old('firstname')}}">
                                    {!! $errors->first('firstname', '<p class="help-block" style="color:red;">:message</p>') !!}

                                </div>
                                <div class="col-sm-6"><input type="text" placeholder="last Name"
                                                             class="form-control {{ $errors->has('lastname') ? 'has-error' : ''}}" name="lastname" required value="{{old('lastname')}}">
                                    {!! $errors->first('lastname', '<p class="help-block" style="color:red;">:message</p>') !!}
                                </div>
                                <div class="col-sm-6"><input type="email" placeholder="Email"
                                                             class="form-control {{ $errors->has('email') ? 'has-error' : ''}}" name="email" required value="{{old('email')}}">
                                    {!! $errors->first('email', '<p class="help-block" style="color:red;">:message</p>') !!}
                                </div>
                                <div class="col-sm-6"><input type="tel" placeholder="Phone Number"
                                                             class="form-control {{ $errors->has('telephone') ? 'has-error' : ''}}" name="telephone"  data-inputmask='"mask": "(999) 999-9999"' data-mask required value="{{old('telephone')}}">
                                    {!! $errors->first('telephone', '<p class="help-block" style="color:red;">:message</p>') !!}
                                </div>
                            </div>
                            <h3>Your Password</h3>
                            <div class="row">
                                <div class="col-sm-6"><input type="password" placeholder="Password" id="password"
                                                             class="form-control {{ $errors->has('password') ? 'has-error' : ''}}" name="password" required>
                                    {!! $errors->first('password', '<p class="help-block" style="color:red;">:message</p>') !!}
                                    <div class="form-check">
                                        <label class="form-check-label">
                                        <input type="checkbox" onclick="togglePassword()" class="form-check-input"> Show Password
                                        </label>
                                    </div>
                                    <p class="password">Password must be of minimum length 8-16 and must has at least 1 digit
                                        and 1 character</p>
                                </div>
                                <div class="col-sm-6"><input type="password" placeholder="Confirm Password"
                                                             class="form-control {{ $errors->has('confirm_password') ? 'has-error' : ''}}" name="confirm_password" required>
                                    {!! $errors->first('confirm_password', '<p class="help-block" style="color:red;">:message</p>') !!}
                                </div>
                            </div>
                            <div class="subscribe">
                                <span>subscribe newsletter</span>
                                <div class="pretty p-default  p-curve">
                                    <input type="radio" name="newsletter" checked required value="Yes">
                                    <div class="state">
                                        <label>Yes</label>
                                    </div>
                                </div>
                                <div class="pretty p-default  p-curve">
                                    <input type="radio" name="newsletter" value="No"/>
                                    <div class="state">
                                        <label>No</label>
                                    </div>
                                </div>
                            </div>
                            <div class="agree">
                                <div class="pretty p-default p-curve">
                                    <input type="checkbox" name="check" checked required>
                                    <div class="state">
                                        <label></label>
                                    </div>
                                </div>
                                <span>I have read and agree to the <a href="{{route('policy','privacy-policy')}}" target="_blank">Privacy Policy</a></span>
                            </div>
                            <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                            <input type="submit" value="continue">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
<script src="https://www.google.com/recaptcha/api.js?render={{env('GOOGLE_RECAPTCHA_KEY')}}"></script>
    <script>var recptchaKey = "{{env('GOOGLE_RECAPTCHA_KEY')}}"</script>
    <style>
        .grecaptcha-badge {
            visibility: hidden;
        }
    </style>
    <!-- RECAPTCHA -->
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", { action: "Register" }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
    <script>
    function togglePassword() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
        }
    </script>
    <!-- InputMask -->
    <script src="{{asset('/plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
    <script>
        $('[data-mask]').inputmask()
    </script>
    
@endsection
