@extends('front.layouts.app')
@section('title','Kitchen Supply Store | Kitchen Store Near Me | House Appliances')
@section('description','At Jumbo, we believe the kitchen is one of the most emotional investments in a house, and we give the best products to enhance that value.')
@section('keywords','kitchen accessories store,kitchen shop online shopping,kitchen supplies online canada,kitchen products,kitchen online shop,Kitchen Supply Store,kitchen supplies shop,kitchen supply store canada,online store for kitchen accessories')
@section('canonical','https://jumbokitchenappliances.com/about-jumbo-canada')
@section('content')
    <div class="about-page page">
        <div class="banner-common">
            <img src="{{asset('assets/img/about-banner.png')}}" alt="banner" class="img-fluid w-100">
        </div>
        <div class="container">
            <div class="about-inner">
                <div class="row">
                    <div class="col-sm-8">
                        <h5>About us</h5>
                    
                        <p style="text-align: justify;">A kitchen is every woman largest financial and emotional investment. 
                            At Jumbo Canada, our goal is to assist you in crafting your dream 
                            kitchen with the right appliance, especially sourced <i>‘Make in India’</i> 
                            range of products.
                        </p>
                        <p style="text-align: justify;">At Jumbo, we are passionate about appliances being essential part of your 
                        life and even more passionate about earning the trust of our customers.</p>
                        
                    </div>
                    <div class="col-sm-4 pt-5 pb-3" >
                        <img src="{{asset('assets/img/about-right.png')}}" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-sm-12">
                    <p style="text-align: justify;">Jumbo Canada Inc in Association with Ultralinks Distribution represents 
                            “Vidiem Appliances”, “Butterfly Cookware’s”, “Kent Water Purification
                            Systems”, “Bajaj Kitchen Appliances” and expanding further on brands 
                            and categories those are among fortune 500 companies in India now ready to 
                            enter the hearts and kitchens of many families across North America.</p>
                        <h5>MAKE IN INDIA </h5>    
                        <p style="text-align: justify;">Our main focus is to bring the premium brands that are “Made in India” to your reach and 
                        uplifting Asian cooking with our wide range of products touching a sweet nostalgia from India. </p>
                        <div class ="row">
                        <div class="col-sm-4"><img data-src="{{asset('assets/img/abt1.jpg')}}"  class="img-fluid lazy" style="width:75%" alt="Make in india"></div> 
                        <div class="col-sm-4"><img data-src="{{asset('assets/img/abt2.png')}}"  class="img-fluid lazy" style="width:50%" alt=""></div> 
                        <div class="col-sm-4"><img data-src="{{asset('assets/img/abt3.png')}}"  class="img-fluid lazy" style="width:50%" alt=""></div> 
                        </div>
                        <h5 class="pt-3">SUPPORT＋ </h5>
                        <p style="text-align: justify;"> We are available at service right from product registration to recycling a product, 
                        offering state of the art service to make your life easier and comfortable. We make your 
                        dreams come true whenever you want… Our live chat option is available all the time for you 
                        to support with turnaround time within same day…</p>  
                        <h5>COMPETITIVE PRICING</h5>   
                        <p style="text-align: justify;">we have target product for each of your needs and budget. Our website also lays down a 
                        product to product comparison with pricing that becomes easier for you to decide and 
                        complete your dream kitchen.</p>
                        <p style="text-align: justify;">Every appliance we represent meets the standards of the Canadian Market, efficient, 
                        contemporary, and easy to use, evolving to support and often improve the lifestyle of many 
                        people.  </p>
                        <p style="text-align: justify;">Explore dozens of incredible kitchen appliance brands today and buy the right brand for you and your family. </p>
                        <p style="text-align: justify;">we also offer appliance rebates and online coupons to Canadian New Immigrants & Students so you get the best prices with peace of mind.</p>
                        <ul class="points">
                                <li>Consumer Focused</li>
                                <li>Honesty and Integrity</li>
                                <li>Commitment to Excellence</li>
                                <li>Collaboration</li>
                                <li>Individual Respect and Accountability</li>
                                <li>Sustainability</li>
                        </ul>
                    </div>                    
                    <div class="about-block">
                        <h5>Stores and Products</h5>
                        <p style="text-align: justify;">The Jumbo Appliances operates at various e-commerce platforms and offline retail partners that reaches you everywhere across Canada using efficient logistics.</p>
                        <a href="{{route('store')}}" class="btn btn-primary" style="background-color:#2A3A92">STORE LOCATOR </a>

                    </div>
                </div>
            </div>
        </div>
     <!--   <div class="container">
            <div class="buy-back">
                <a href="">
                    <img src="{{asset('assets/img/buy-back-exchange.png')}}" alt="" class="img-fluid w-100">
                </a>
            </div>

        </div>-->

    </div>
@endsection
