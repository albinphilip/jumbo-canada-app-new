@extends('front.layouts.app')
@section('title','Jumbo Canada | Make Payment')
@section('description','')
@section('keywords','')
@section('content')
    @php
        $stripe_key = env('STRIPE_PUBLISHABLE_KEY');
    @endphp
    @php($customer=Auth::guard('customer')->user())
    <div class="cart page">
        <div class="container-fluid">
            <ul class="list-inline text-center breadcrumb">
                <li class="list-inline-item"><a href="{{route('cart.index')}}">CART</a></li>
                <li class="list-inline-item"><a href="{{route('shipping')}}">CHECKOUT</a></li>
                <li class="list-inline-item"><a href="#" style="color: darkblue">PAYMENT</a></li>
            </ul>
            <div class="cart-inner">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="cart-items">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h3>CART ITEMS</h3>

                                </div>
                            </div>
                        </div>
                        @php($carts = \Cart::Content())
                            @foreach($carts as $cart)
                                <div class="cart-item">
                                    <div class="product-image">
                                        <a href="{{route('productdetail',[strtolower($cart->options['brand']),$cart->options['slug']])}}"><img src="{{asset($cart->options['image'])}}" alt=""
                                                                                            class="img-fluid"></a>
                                    </div>
                                    <div class="product-details">
                                        <a href="{{route('productdetail',[strtolower($cart->options['brand']),$cart->options['slug']])}}" class="pname">{{$cart->name}}</a>
                                        @if($cart->options['rating'] > 0)
                                            <div class="rating">
                                                <span>{{number_format($cart->options['rating'], 2, '.', ',')}}/5</span>
                                                <ul class="list-inline star">
                                                    <li>
                                                        @php($rating = $cart->options['rating'])
                                                        @foreach(range(1,5) as $i)
                                                            @if($rating>0)
                                                                @if($rating>0.5)
                                                                    <i class="fa fa-star" style="color: goldenrod;"></i>
                                                                @else
                                                                    <i class="fa fa-star-half" style="color: goldenrod;"></i>
                                                                @endif
                                                            @else
                                                                <i class="fa  fa-star-o"></i>
                                                            @endif
                                                            @php($rating--)
                                                        @endforeach
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="quantity">
                                            <span>Quantity </span>
                                            <span>{{$cart->qty}} </span>
                                        </div>
                                    </div>
                                 </div>
                        @endforeach
                            <div class="secure-payment">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <img src="{{asset('assets/img/secure.png')}}" alt="" class="img-fluid secure">
                                        <h4>100% <br><span>secure payment</span></h4>
                                    </div>
                                    <div class="col-sm-8">
                                        <img src="{{asset('assets/img/pay.png')}}" alt="" class="img-fluid pay">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- payment -->
                    <div class="col-sm-4">
                        <h3>Payment Detail</h3>
                        <div class="cart-items-pay">
                            <table id="total_table">
                                <tbody>
                                <tr>
                                    <td>TOTAL</td>
                                    <td>C${{\Cart::total()}}</td>
                                </tr>
                                @if(Session::has('reduction'))
                                    <tr>
                                        <td>Reduction</td>
                                        <td id="tax">C${{Session::get('reduction')}}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <td>TAX ({{$tax}} %)</td>
                                    <td id="tax">C${{number_format($taxed, 2, '.', ',')}}</td>
                                </tr>
                                <tr>
                                    <td> SHIPPING CHARGE</td>
                                    <td id="shipping">C${{$shipping}}</td>
                                </tr>

                                <tr class="total">
                                    <td>GRAND TOTAL</td>
                                    <td id="amount">C${{Session::get('final_amount')}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        @if(!Session::has('reduction'))
                        <form action="{{route('guest.appplyCoupon')}}" class="apply-coupon" method="post">
                            @csrf
                            <input type="text" placeholder="Apply Coupon" name="coupon" class="form-control"
                                   required>
                            <button><img src="{{asset('assets/img/coupon.png')}}" alt="" class="img-fluid"></button>
                        </form>
                        @endif
                        @if(!$carts->isEmpty())
                            <form action="{{route('confirm-order')}}"  method="post" id="payment-form">
                                @csrf
                                <div class="form-group">
                                    <div class="card-body">
                                        <div id="card-element" style="border: 1px solid;padding: 20px">
                                            <!-- A Stripe Element will be inserted here. -->
                                        </div>
                                        <!-- Used to display form errors. -->
                                        <div id="card-errors" role="alert" style="color:red"></div>
                                        <input type="hidden" name="plan" value="" />
                                    </div>
                                </div>
                                <input type="checkbox" required="" id="agree"> <label for="agree">I Agree <a target="_blank" href="{{route('policy','terms-and-conditions-of-warranty-and-safe-appliances-usage')}}"> Terms and Conditions</a></label>
                                <div class="card-footer" style="background-color: #ffffff;border: none">
                                    <button
                                        id="card-button"
                                        class="proceed"
                                        type="submit"
                                        data-secret="{{ $intent }}"
                                    > Make Payment </button>
                                    <span id="msg"></span>
                                </div>
                            </form>
                            @if(Session::has('msg'))
                                <p style="color:red;"> {{ Session::get('msg') }}</p>
                            @endif
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
@section('additionalScripts')

    <script src="https://js.stripe.com/v3/"></script>
    <script>
        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)

        var style = {
            base: {
                color: '#32325d',
                lineHeight: '18px',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        const stripe = Stripe('{{ $stripe_key }}', { locale: 'en' }); // Create a Stripe client.
        const elements = stripe.elements(); // Create an instance of Elements.
        const cardElement = elements.create('card', { style: style, hidePostalCode: true}); // Create an instance of the card Element.
        const cardButton = document.getElementById('card-button');
        const clientSecret = cardButton.dataset.secret;

        cardElement.mount('#card-element'); // Add an instance of the card Element into the `card-element` <div>.

        // Handle real-time validation errors from the card Element.
        cardElement.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form');

        form.addEventListener('submit', function (event) {
            event.preventDefault();
            //show please wait
            $('#msg').html(' <i class="fa fa-cog fa-spin"></i> Please wait');
            //$("#card-button").hide();
            stripe.confirmCardPayment(clientSecret, {
              payment_method: {
                    card: cardElement,
                billing_details: {
                  name: '{{$customer_name}}'
                }
              },
              setup_future_usage: 'off_session'
            }).then(function (result) {
                    if (result.error) {
                        // Inform the user if there was an error.
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;
                        $('#msg').html('');

                        $('#payment-form').append('<input type="hidden" name="payment_status" value="incomplete" />');
                        // form.append("<input type='hidden' name='payment_status' value='failed'/>");


                        $('#skip_payment').removeClass('hide');

                    } else {
                        $('#payment-form').append('<input type="hidden" name="payment_status" value="success" />');
                        form.submit();
                    }
                });
        });
    </script>
@endsection
