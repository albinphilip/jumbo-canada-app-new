@extends('front.layouts.app')
@section('title','')
@section('description','')
@section('keywords','')
@section('canonical','')
@section('content')
    @php($customer=Auth::guard('customer')->user())
    <div class="register page">
        <div class="register-inner">
            <h1>Return a product</h1>
            <form action="{{route('productReturn')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <p>Dear customer, before returning the product you must <a href="{{route('warranty-registration')}}"> register the product for warranty</a> and
                            obtain your registration number</p>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="reg_num">Registration Number <small>*</small></label>
                        <input type="text" class="form-control"  placeholder="Registration Number"
                               name="reg_num" id="reg_num"
                               value="{{ old('reg_num') }}" required>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="email">Email Address <small>*</small></label>
                        <input type="email" class="form-control" required placeholder="Email Address" name="email" id="email"
                               value="@if(Auth::guard('customer')->check()){{$customer->email}} @else{{ old('email') }}@endif">
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="dead">Reason for returning <small>*</small></label>
                        <input type="text" class="form-control" required placeholder="eg: Dead on arrival" name="dead" id="dead"
                               value="{{ old('dead') }}">
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label" for="address">Is product opened ?</label>
                        <br/>
                        <div class="pretty p-default p-curve">
                            <input type="radio" name="is_open" value="Yes" id="address" checked required>
                            <div class="state p-primary-o">
                                <label>Yes</label>
                            </div>
                        </div>
                        <div class="pretty p-default p-curve">
                            <input type="radio" name="is_open" value="No"/>
                            <div class="state p-primary-o">
                                <label>No</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="upload">
                            <input class="form-control" id="upload-file-img" type="file"
                                   accept="image/png, image/jpeg" name="image1" required>

                            <a id="fileupload-img"><span>Upload Image*</span><img
                                    src="{{asset('assets/img/attach.svg')}}"
                                    alt="upload"></a>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="faulty">Detailed Description <small>*</small></label>
                        <textarea class="form-control" required placeholder="" name="faulty" id="faulty"></textarea>
                    </div>
                    <div class="col-12">
                        <input type="submit" value="submit">
                    </div>
                </div>
            </form>
            <div class="col-12">
                <div class="response" id="pleaseWait" style="display: none;">
                    Please wait <i class="fa fa-cog fa-spin"></i>
                </div>
                <div id="subscribed"></div>
            </div>
        </div>
    </div>
@endsection

