@extends('front.layouts.app')
@section('title')
    @if($product->productMetadata!='' && $product->productMetadata->title != '') {{$product->productMetadata->title}}
                @else Jumbo Canada   @endif
@endsection
@section('description')@if($product->productMetadata!=''){{$product->productMetadata->description}}@endif
@endsection
@section('keywords')@if($product->productMetadata!=''){{$product->productMetadata->keywords}} @endif
@endsection
@section('canonical')https://jumbocanada.com/product/{{$product->brand->name}}/product-detail/{{$product->slug}}@endsection
@section('content')
    <div class="product-view page">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="property-slider-sec">
                        <div class="pkg-gallery">
                            <div class="main-image">
                                <div class="image-view">
                                    @if($product->getImage!='')
                                    <a class="popup-link" data-fancybox
                                       href="{{asset($product->getImage->image_path)}}">
                                        <img src="{{asset($product->getImage->image_path)}}" class="img-fluid" alt="full-image" id="image">
                                    </a>
                                    @endif
                                    @if($product->getVideo!='')
                                            <iframe id="if" class="d-none" width="100%" height="400"
                                                    src="https://www.youtube.com/embed/{{$product->getVideo->url}}"
                                                    frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen></iframe>
                                    @endif

                                </div>
                            </div>
                            <div class="thumb-block">
                                <div class="owl-carousel owl-theme thumb-images">
                                    @php($k = 1)
                                    @foreach($product->productImages->sortBy('sort_order') as $image)
                                        <div class="item">
                                            <img class="thumb" src="{{asset($image->thumbnail_path)}}" alt="{{$product->slug.$k}}"
                                                 data-src="{{asset($image->image_path)}}">
                                        </div>
                                        @php($k++)
                                    @endforeach
                                    @foreach($product->productVideos->sortBy('sort_order') as $video)
                                        <div class="item">
                                            <img class="thumb-video" src="{{asset('assets/img/play_video.png')}}"
                                                 data-src="https://www.youtube.com/embed/{{$video->url}}">
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
                <p hidden id="slug">{{$product->slug}}</p>
                <p hidden id="brand">{{$product->brand->name}}</p>
                @if($product->getVariant->quantity >0)
                    @php($availability = 'In Stock')
                @else
                    @php($availability = 'Pre Booking')
                @endif
                <div class="col-sm-6">
                    <div class="product-details">
                        <h3 class="product-name" id="name">{{$product->name}}</h3>
                        <h5>Specifications</h5>
                        <ul class="product-info">
                            <li><span>Model Name</span><span>{{$product->model}}</span></li>
                            <li><span>Net Weight (in Kg)</span><span>{{$product->weight}}</span></li>
                            <li><span>Product Dimensions (L)X(B)X(H) (in cm)</span><span>{{$product->dimension}}</span></li>
                            <li><span>Availability</span><span>{{$availability}}</span></li>

                        </ul>
                        <h5>Price</h5>
                        <p class="price">
                         @php($price = 0)
                        @if($product->offer != null && $product->offer_expire >= today())
                                <span class="offer">-{{$product->offer}}%</span><span
                                    class="old-price">C${{$product->getVariant->price}}</span>
                            @php($price = $product->getVariant->price - ($product->getVariant->price*$product->offer/100))
                            @else
                               @php($price = $product->getVariant->price)
                            @endif
                            <span id="price">C${{$price}}</span>
                        </p>
                        <div class="actions">
                           <!-- <a href="" class="shop">shop now</a>-->
                            <a href="#" onclick="addToCart({{$product->id}})" class="add">add to cart</a>
                            <a href="{{route('wishlist.store',$product->id)}}" class="fav"><i
                                        class="fa fa-heart"
                                        aria-hidden="true"></i></a>
                            <p id="msg" style="color:red;"></p>

                        </div>

                    <!-- variant-->
                        <p hidden id="variant">{{$product->getVariant->value}}</p>
                        @if($product->productVariants->count()>1)
                        <div class="colors">
                            <span>{{$product->getVariant->variant}}S AVAILABLE</span>
                            @foreach($product->productVariants->sortByDesc('id') as $var)
                                <div class="pretty p-default p-round {{strtolower($var->value)}}">
                                    <input type="radio" value="{{$var->value}}" name="radio1" id="radio1"
                                           onclick="radioclick({{$var->id}})" {{$product->getVariant->value == $var->value ? 'checked' : ''}}>
                                    <div class="state">
                                        <label></label>
                                    </div>
                                </div>
                                {{$var->value}}
                            @endforeach
                        </div>
                        @endif
                        <ul class="list-inline price-rate">
                            @if($product->reviews->count()>0)
                                <li class="list-inline-item">
                                    <div class="rating">
                                        @php($rating = 0)
                                        @foreach($product->reviews as $reviews)
                                            @php($rating = $rating+$reviews->rating / Count($product->reviews))
                                        @endforeach
                                        <div hidden id="rating">{{$rating}}</div>
                                        <span>{{number_format($rating, 2, '.', ',')}}/5<img
                                                src="{{asset('assets/img/star-active.png')}}"
                                                alt="" class="img-fluid"></span>
                                    </div>
                                </li>
                            @else
                                <li hidden id="rating">{{0}}</li>
                            @endif
                                <li class="list-inline-item">
                                    <a href="#reviews" class="view-rating">VIEW ALL REVIEW <img
                                            src="{{asset('assets/img/star.png')}}"
                                            alt=""></a>
                                </li>
                        </ul>
                        <a href="{{route('cart.index')}}" class="continue-shop">continue shopping <img src="{{asset('assets/img/continue-shopping.svg')}}"
                                                                                alt=""></a>
                        <div class="delivery-block">
                            <img src="{{asset('assets/img/delivery.png')}}" alt="" class="img-fluid">
                                @if($availability == 'In Stock')
                                    @php($delivery = 'Deliver between 1 - 5 business days')
                                @else
                                    @php($delivery = 'Deliver between 25 - 30 Working days')
                                @endif
                                <h5 id="delivery">{{$delivery}}</h5>
                        </div>
                        <div class="tabs">
                            <div class="tab-sec">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#description">DESCRIPTION</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#specifications">SPECIFICATIONS</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#reviews"> REVIEWS
                                            ({{$product->reviews()->count()}}) </a>
                                    </li>
                                </ul>

                                <div class="tab-content" data-aos="fade-up">
                                    <div id="description" class="tab-pane active">
                                        {!! $product->description !!}
                                    </div>
                                    <div id="specifications" class="tab-pane">
                                        <div class="col-sm-6">
                                        <ul class="other-info">
                                            @if(Illuminate\Support\Str::contains($product->category->name,'Mixer') == false )
                                                @foreach($product->productSpecifications->sortBy('id') as $specification)
                                                    <li><span>{{$specification->specification}}</span>
                                                        <span>{{$specification->value}} {{$specification->unit}}</span>
                                                    </li>
                                                @endforeach
                                            @else
                                                @php($data  = array('No.Of Jars','Super Extractor Juicer','Chutney Jar Size',
                                                            'Medium Jar Size','Large Jar Size',
                                                            "Grind N' Store Containers",'Warranty','Voltage, Wattage',
                                                            'Chef Jar','Coconut Scraper','Local customization','Sound score',
                                                             'Performance Score','Design Score','Return Ratio','Durability Score Jars',
                                                             'Sellers Warranty Motor','Sellers Warranty Jars','Sellers Warranty Parts',
                                                             'Certification','Motor Type','Mixer Shape','Colour','OLP','Manufactured By','Brand name',
                                                             'Price','Remarks'))
                                                @php($len = count($data))
                                                @for($i = 0; $i<$len;$i++)
                                                    @foreach($product->productSpecifications as $specification)
                                                        @if($specification->specification == $data[$i] && $specification->value != null )
                                                            <li><span>{{$data[$i]}}</span>
                                                                @if($specification->specification == 'Price')
                                                                    @php($pric = 0)
                                                                    @if($product->offer != null && $product->offer_expire >= today() && $specification->value != null)
                                                                        @php($price = $specification->value - (($specification->value*$product->offer)/100))
                                                                    @else
                                                                        @php($price = $specification->value)
                                                                    @endif
                                                                    <span>{{'C$'.$price}} </span>
                                                                @else
                                                                    <span>{{$specification->value}} {{$specification->unit}}</span>
                                                                @endif
                                                                @break;
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                @endfor
                                            @endif

                                        </ul>
                                        </div>
                                    </div>
                                    <div id="reviews" class="tab-pane ">
                                        <a href="{{route('review.index',$product->id)}}" class="btn btn-primary btn-sm">Write a
                                            review</a>
                                        @if($product->reviews()->count() == 0)

                                            <h6 style="padding-top: 5px;">No Reviews</h6>
                                        @else
                                            @foreach($product->reviews->sortByDesc('id') as $review)
                                                @php($rating = $review->rating)
                                                <div class="cart-item pt-3">
                                                    @foreach(range(1,5) as $i)
                                                        @if($review->rating >0)
                                                            @if($review->rating >0.5)
                                                                <i class="fa fa-star" style="color: goldenrod;"></i>
                                                            @else
                                                                <i class="fa fa-star-half" style="color: goldenrod;"></i>
                                                            @endif
                                                        @else
                                                            <i class="fa  fa-star-o"></i>
                                                        @endif
                                                        @php($review->rating--)
                                                    @endforeach
                                                    <span>({{$rating}})</span>
                                                    <div>
                                                        <h6>{{$review->review}}</h6>
                                                        By : {{$review->customer->firstname.' '.$review->customer->lastname}}
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="products-slide-block">
                        <h3 class="common-head">Similar products</h3>
                        <div class="owl-carousel owl-theme products-slider">
                            @foreach($similiars as $similiar)
                                <div class="item">
                                    <div class="product-block">
                                        <div class="pdt-image">
                                            <a href="{{route('productdetail',[strtolower($similiar->brand->name),$similiar->slug])}}">
                                                <img @if($product->getImage != null)
                                                     data-src="{{asset($similiar->getImage->thumbnail_path)}}" @endif alt="" class="img-fluid w-100 lazy">
                                            </a>
                                        </div>
                                        <div class="pdt-details">
                                            <p class="brand">{{$similiar->brand->name}}</p>
                                            <p class="name">{{$similiar->name}}</p>
                                            <p class="price">
                                                @if($similiar->offer != null && $similiar->offer_expire >= today())
                                                    <span class="offer">-{{$similiar->offer}}%</span><span
                                                        class="old-price">C${{$similiar->getVariant->price}}</span>
                                                    C${{$similiar->getVariant->price - ($similiar->getVariant->price*$similiar->offer/100)}}
                                                @else
                                                    C${{$similiar->getVariant->price}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="actions">
                                            <a href="{{route('productdetail',[strtolower($similiar->brand->name),$similiar->slug])}}" class="shop">shop now</a>
                                            <a href="{{route('productdetail',[strtolower($similiar->brand->name),$similiar->slug])}}" class="add">add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
@section('additionalScripts')
    <script defer>
        function radioclick(id) {
            console.log(id);
            url = "/productprice/" + id;
            $.ajax({
                url: url,
                method: 'get',
                success: function (response) {
                    console.log(response)
                    $('#price').text('C$' + response.price);
                    $('#cart_price').val(response.price);
                    $('#available').text(response.availability);
                    if (response.availability == 'In Stock') {
                        $('#delivery').text('Deliver between 2 - 7 days');
                    } else {
                        $('#delivery').text('Delivers between 25-30 working days');
                    }
                },

            })
        }

        function addToCart(id) {
            $('#msg').html('<i class="fa fa-cog fa-spin"></i> Adding');
            console.log(id);
            rating = null;
            url = "/add-to-cart";
            var name = document.getElementById('name').textContent;
            var quantity = 1;
            var price = document.getElementById('price').textContent;
            price = price.slice(2);
            var variant = $('input[name=radio1]:checked').val();
            if (variant == null)
                variant = document.getElementById('variant').textContent;
            var image = $('#image').attr('src');
            var rating = document.getElementById('rating').textContent;
            rating = '';
            var slug = document.getElementById('slug').textContent;
            var brand = document.getElementById('brand').textContent;
            //alert(rating);
            $.ajax({
                url: url,
                method: 'post',
                data: {
                    product_id: id,
                    name: name,
                    quantity: quantity,
                    price: price,
                    variant: variant,
                    image: image,
                    rating: rating,
                    slug: slug,
                    brand:brand,
                    "_token": "{{ csrf_token() }}",
                },
                success: function (response) {
                    console.log(response)
                    if (response.success == 'success') {
                        $('#msg').hide();
                        fireTost('Item Added To Cart');
                        console.log(response.cart)
                        $('#c_count span').text(response.cart);
                        //console.log($('#c_count').text());
                        //setInterval('location.reload()', 3000);

                    }
                },

            });
        }

        function fireTost(message) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }

    </script>
@endsection
