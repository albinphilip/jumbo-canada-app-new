@extends('front.layouts.app')
@section('title','Jumbo Canada | Compare Products')
@section('description','')
@section('keywords','')
@section('content')
    <div class="compare page">
        <div class="container">
            <div class="compare-block">
                <h1 class="text-center">Compare products</h1>
                @if($products == null)
                    No Products to compare
                @else
                    @if(count($products) == 1)
                        @php($width = 50)
                    @elseif(count($products) == 2)
                        @php($width = 33.33)
                    @elseif(count($products) == 3)
                        @php($width = 25)
                    @else
                        @php($width = 15)
                    @endif
                        @php($data  = array('No.Of Jars','Super Extractor Juicer','Chutney Jar Size',
                                                'Medium Jar Size','Large Jar Size',
                                                "Grind N' Store Containers",'Warranty','Voltage, Wattage',
                                                'Chef Jar','Coconut Scraper','Local customization'))
                        @php($len = count($data))
                        @foreach($products as $product)
                            @php($prdt = $product)
                        @endforeach
                        @if(Illuminate\Support\Str::contains($prdt->category->name,'Mixer') == false )
                            @php($hide = '')
                        @else   @php($hide = 'hidden')
                        @endif
                        <table class="table-bordered">
                        @php($flag = 0)

                            <tr  style="border: 1px solid !important; border-color:   #ebebe0 !important; border-bottom-color: white !important;">
                                <td width="{{$width}}%"   width="{{$width}}%" style="border: none !important;"></td>
                                @foreach($products as $product)
                                    <td width="{{$width}}%" style="border: none !important;">
                                    <a href="{{route('productdetail',[strtolower($product->brand->name),$product->slug])}}">
                                        @if($product->getImage!='')
                                            <img src="{{asset($product->getImage->image_path)}}" alt="" id="image{{$product->id}}" class="img-fluid product-image">
                                        @endif
                                    </a>
                                        <h6 id="name[{{$product->id}}]"><b>{{$product->name}}</b></h6>
                                    </td>
                                @endforeach

                            </tr>
                            <tr style="border: 1px solid !important; border-color:   #ebebe0 !important;">
                                <td width="{{$width}}%" style="border: none !important;"></td>
                                @foreach($products as $product)
                                        <td wth="{{$width}}%" style="border: none !important;">
                                        <ul class="list-inline actions">
                                        @if($product->status != 'Hidden')<li class="list-inline-item"><a href="#" onclick="addToCart({{$product->id}})">
                                                <img src="{{asset('assets/img/cart-icon.png')}}" alt="" class="img-fluid"> </a>
                                        </li>@endif
                                        <li class="list-inline-item"><a href="{{route('comparison.destroy',$product->id)}}"><i class="fa fa-trash"></i></a></li>
                                    </ul>
                                    <p id="msg" style="color:red;"></p>
                                    <p hidden id="slug[{{$product->id}}]" >{{$product->slug}}</p>
                                    <p hidden id="brand[{{$product->id}}]" >{{$product->brand->name}}</p>
                                </td>
                            @endforeach
                        </tr>
                        <tr {{$hide}}>
                           <td width="{{$width}}%">
                               Product Model
                           </td>
                           @foreach($products as $product)
                               <td width="{{$width}}%">{{$product->model}}</td>
                           @endforeach
                        </tr>
                        <tr {{$hide}}>
                            <td width="{{$width}}%">
                                Price
                            </td>
                            @foreach($products as $product)
                                @if($product->offer!=null && $product->offer_expire >= today())
                                    @php($price = $product->getVariant->price-
                                                  ($product->getVariant->price*$product->offer)/100)
                                @else
                                    @php($price = $product->getVariant->price)
                                @endif
                                <td width="{{$width}}%" id="price[{{$product->id}}]">C${{$price}}</td>
                                <p hidden id="variant[{{$product->id}}]">{{$product->getVariant->value}}</p>
                            @endforeach
                        </tr>
                        <tr id="rat_row" hidden>
                            <td width="{{$width}}%">
                                Rating
                            </td>
                            @foreach($products as $product)

                            <td width="{{$width}}%">
                                @if($product->reviews->count()>0)
                                    <div class="rating">
                                        @php($rating = 0)
                                        @foreach($product->reviews as $reviews)
                                            @php($rating = $rating+$reviews->rating / Count($product->reviews))
                                        @endforeach
                                        <p hidden id="rating[{{$product->id}}]" >{{$rating}}</p>
                                        <span>{{number_format($rating, 2, '.', ',')}}/5</span>
                                        <ul class="list-inline star">
                                            @foreach(range(1,5) as $i)
                                                @if($rating>0)
                                                    @if($rating>0.5)
                                                        <i class="fa fa-star" style="color: goldenrod;"></i>
                                                    @else
                                                        <i class="fa fa-star-half" style="color: goldenrod;"></i>
                                                    @endif
                                                @else
                                                    <i class="fa  fa-star-o"></i>
                                                @endif
                                                @php($rating--)
                                            @endforeach
                                        </ul>
                                        @php($flag = 1)
                                    </div>
                                @else
                                    <p hidden id="rating[{{$product->id}}]" >{{0}}</p>
                                @endif
                            </td>
                            @endforeach
                                <input type="hidden" id="flag" value="{{$flag}}">
                        </tr>

                        <tr {{$hide}}>
                            <td width="{{$width}}%">
                                Availability
                            </td>
                            @foreach($products as $product)
                                @if($product->getVariant->quantity > 0)
                                <td width="{{$width}}%">In stock</td>
                                @else
                                    <td width="{{$width}}%">Out Of stock</td>
                                @endif
                            @endforeach
                        </tr>
                        <tr {{$hide}}>
                            <td width="{{$width}}%">
                                Dimensions (cms)
                            </td>
                            @foreach($products as $product)
                                <td width="{{$width}}%" style="vertical-align:top;">
                                    {{$product->dimension}}</td>
                            @endforeach
                        </tr>
                        <tr {{$hide}}>
                            <td width="{{$width}}%">
                                Weight (Lbs)
                            </td>
                            @foreach($products as $product)
                            <td width="{{$width}}%" style="vertical-align:top;">
                                       {{$product->weight}}</td>
                           @endforeach
                        </tr>
                        @if(Illuminate\Support\Str::contains($prdt->category->name,'Mixer') == true )

                            @for($i = 0; $i<$len;$i++)
                                <tr>
                                    <td width = "{{$width}}%">{{$data[$i]}}</td>
                                    @foreach($products as $product)
                                        @php($k = 1)
                                        @foreach($product->productSpecifications as $specification)
                                            @if($specification->specification == $data[$i])
                                                @if($data[$i] == 'Price')
                                                    @php($price = 0)
                                                    @if($product->offer != null && $product->offer_expire >= today())
                                                        @php($price = $specification->value - (($specification->value*$product->offer)/100))
                                                    @else
                                                        @php($price = $specification->value)
                                                    @endif
                                                    <td width="{{$width}}%">{{'C$'.$price}}</td>
                                                @else
                                                    <td width="{{$width}}%">{{$specification->value.' '.$specification->unit}}</td>
                                                @endif
                                                    @php($k = 0)
                                                @break;
                                            @endif
                                        @endforeach
                                        @if($k == 1)
                                            <td width="{{$width}}%"></td>
                                        @endif
                                    @endforeach
                                </tr>
                            @endfor
                        @endif
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script defer>
        window.onload = function() {
            flag = $('#flag').val();
            console.log(flag);
            if(flag == 0)
                $('#rat_row').hide();
        }
        function addToCart(id) {
            $('#msg'+id).html('<i class="fa fa-cog fa-spin"></i> Adding');
            console.log(id);
            variant = null;
            rating =null;
            url = "/add-to-cart";
            var name = document.getElementById('name['+id+']').textContent;
            var quantity = 1;
            var price = document.getElementById('price['+id+']').textContent;
            price = price.slice(2);
            var variant = document.getElementById('variant['+id+']').textContent;
            var image = $('#image'+id).attr('src');
            var rating = document.getElementById('rating['+id+']').textContent;
            var slug = document.getElementById('slug['+id+']').textContent;
            var brand = document.getElementById('brand['+id+']').textContent;

            //alert(image);
            $.ajax({
                url: url,
                method: 'post',
                data: {
                    product_id: id, name: name, quantity: quantity, price: price, variant: variant, image: image,rating:rating,slug:slug,brand:brand,
                    "_token": "{{ csrf_token() }}",
                },
                success: function (response) {
                    console.log(response)
                    if (response.success == 'success') {
                        $('#msg'+id).hide();
                        firTost('Item Added To Cart');
                        console.log(response.cart)
                        $('#c_count span').text(response.cart);
                    }
                },

            });
        }
        function firTost(message) {
            const Toast1 = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast1.fire({
                icon: 'success',
                title: message,
            })
        }

    </script>
