@extends('front.emails.user-mail')
@section('preText')
    {{$partner->name}} - Partner registration Phone: {{$partner->phone}} Email: {{$partner->email}} Address: {{$partner->address}}
@endsection
@section('body')
    Hi,
    <p>You have new partner registration request</p>
    Name: {{$partner->name}}<br/>
    Phone: {{$partner->phone}}<br/>
    Email: {{$partner->email}}<br/>
    Address: {{$partner->address}}<br/>
    Province: {{$partner->provice}}<br/>
@endsection
