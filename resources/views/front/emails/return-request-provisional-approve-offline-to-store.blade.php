@extends('front.emails.user-mail')
@section('preText')
    Return request for product {{$return->product_registration->product->name}} by {{$return->product_registration->first_name}} {{$return->product_registration->last_name}} has been approved provisionally.
@section('body')
    Hi,
    <p>
        We have provisionally approved Return request for product {{$return->product_registration->product->name}},
        which is purchased from your store
        by {{$return->product_registration->first_name}} {{$return->product_registration->last_name}}.</p>
    <p>Customer will bring the product to your store and you are requested to give replacement for the faulty product
        after a through inspection and confirmation of the fault.
    </p>
    <P><b>Product details</b></P>
    <ul>
        <li>Product Name: {{$return->product_registration->product->name}}</li>
        <li>Model: {{$return->product_registration->product_code}}</li>
        <li>Serial Number: {{$return->product_registration->serial_number}}</li>
        <li>Safety code: {{$return->product_registration->safety_code}}</li>
        <li>Purchase Date: {{date('M d, Y', strtotime($return->product_registration->purchase_date))}}</li>
        <li><b>RMA Number: {{$return->return_id}}</b></li>
        <li><b>Security Code: {{md5($return->return_id*123456)}}</b></li>
    </ul>
    <b style="color:red">Update the replacement approval/rejection status by the following <a
            href="{{route('returnStatusUpdate')}}">this link</a></b>
    <p>You may contact our customer care <a href="tel:+1 8557733844"> +1 (855) 773 3844</a> for assistance.
    </p>

@endsection
