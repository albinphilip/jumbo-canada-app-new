@extends('front.emails.user-mail')
@section('preText')
    PR-{{ $data['rma_num'] ?? ''}} is your RMA number for repair request. Our representative will contact you soon.
@endsection
@section('body')
    <P>
        Dear {{$data['first_name'] ?? ''}}, <br/>
        This email is to acknowledge that we have received your product repair request.<br>
    </P>
    Your RMA number is : <b>PR-{{ $data['rma_num'] ?? ''}}</b>
    <p>
        <b>Product details are given below</b><br/>
        Product: {{$data['product_name'] ?? ''}}<br/>
        Serial Number: {{$data['serial_number']}}<br/>
        Product Code: {{$data['product_code']}}<br/>
        Safety Code: {{$data['safety_code']}}<br/>
        Date of Purchase: {{$data['purchase_date']}}<br/>
    </P>
    <p>Our representative will contact you soon and in case of emergency please give us a call on <a
            href="tel:+1 8557733844"> +1 (855) 773 3844</a></p>
@endsection
