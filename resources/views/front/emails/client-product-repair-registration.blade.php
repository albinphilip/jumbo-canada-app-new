@extends('front.emails.user-mail')
@section('preText')
    {{$data['product_name'] ?? ''}}- Repair registration Client: {{$data['first_name'] ?? ''}} RMA number: PR-{{ $data['rma_num'] ?? ''}}
@endsection
@section('body')
    <P>
        Hi you have new product repair registration request. , <br/>
    </P>
    Client: {{$data['first_name'] ?? ''}}<br/>
    Email: {{$data['email'] ?? ''}}<br/>
    Phone: {{$data['phone'] ?? ''}}<br/>
    RMA number: PR-{{ $data['rma_num'] ?? ''}}<br/>
    Product: {{$data['product_name'] ?? ''}}<br/>
    Serial Number: {{$data['serial_number']}}<br/>
    Product Code: {{$data['product_code']}}<br/>
    Safety Code: {{$data['safety_code']}}<br/>
    Date of Purchase: {{$data['purchase_date']}}<br/>
@endsection
