@extends('front.emails.user-mail')
@section('preText')
    {{$enquiry['name']}} - Enquiry, Message: {{$enquiry['message']}}
@endsection
@section('body')
Hi ,<br/>
    You have a new enquiry<br/>
Name: {{$enquiry['name']}}<br/>
Email: {{$enquiry['email']}}<br/>
Message: {{$enquiry['message']}}
@endsection
