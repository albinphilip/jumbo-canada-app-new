@extends('front.emails.user-mail')
@section('preText')
    Order Estimated Delivery Date from jumbocanada.
@endsection
@section('body')
    Hi {{$order->customer->firstname ?? ''}},
    <p>Estimated Delivery date of your order for {{$order->product->name}} is {{date('M-d-Y',strtotime($order->delivery_date))}}</p>
@endsection
