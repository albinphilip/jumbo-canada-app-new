@extends('front.emails.user-mail')
@section('preText')
    New discount request Name: {{$discount->name}} Phone: {{$discount->phone}} Email: {{$discount->email}}
@endsection
@section('body')
    Hi,
    <p>You have new discount request</p>
    Name: {{$discount->name}}<br/>
    Phone: {{$discount->phone}}<br/>
    Email: {{$discount->email}}<br/>
    Address: {{$discount->address_1.' '.$discount->address_2.' '.$discount->city.' '.$discount->postcode}}<br/>
    Province: {{$discount->province}}<br/>
    Type: {{$discount->current_status}}<br/>
@endsection
