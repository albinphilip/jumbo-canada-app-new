@extends('front.emails.user-mail')
@section('preText')
    We have shipped your {{$return->product_registration->product->name}} via {{$return->shipping_partner}} Shipping Number is: {{$return->shippment_number}}
@endsection
@section('body')
    Hi {{$return->product_registration->first_name}} {{$return->product_registration->last_name}},
    <p>We have shipped replacement for your {{$return->product_registration->product->name}}
        via {{$return->shipping_partner}}. Your return is on the way.</p>
    <p>Shipping Number is: {{$return->shippment_number}}</p>
    <p>Please register your replaced product for warranty by following <a href="{{route('warranty-re-registration')}}">
            the link.</a></p>
    <p>Please note your RMA number is <b>{{$return->return_id}}.</b></p>
    <p><i>Please choose Place Of Purchase as Jumbocanada Canada Online Store, Toronto in warranty registration page.</i></p>
    <p>You may contact our customer care <a href="tel:+1 8557733844"> +1 (855) 773 3844</a> for assistance.
    </p>
@endsection
