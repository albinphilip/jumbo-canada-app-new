@extends('front.layouts.app')
@section('title')
    {{$page->page}} | Jumbo Canada 
@endsection
@if(strpos($page->page, 'Shipping') !== false)
    @section('description','The shipping, return and exchange policy for every product is clearly mentioned on the product page. Please keep the receipt.')
    @section('keywords','canada kitchen store,canada kitchen supply,canada online kitchen store,cookware online shopping,kitchen shop online shopping,kitchen supplies online canada,kitchen products,kitchen online shop,,kitchen supplies shop,kitchen supply store canada,online store for kitchen accessories')
@elseif(strpos($page->page, 'Terms') !== false)
    @section('description','This is our obligations to you regarding our website. Sections of this disclaimer have been provided and approved by a legal framework.')
    @section('keywords','buy the appliances in online, branded appliances,canada kitchen store,canada kitchen supply,canada online kitchen store,cookware online shopping,kitchen accessories online shopping,kitchen accessories store,kitchen shop online shopping,kitchen supplies online canada')
@elseif(strpos($page->page, 'Privacy') !== false)
    @section('description','Jumbo takes your data privacy seriously. Our privacy policy explains who we are, how we collect, share and use Personal Information.')
    @section('keywords','kitchen products,kitchen online shop,,kitchen supplies shop,kitchen supply store canada,online store for kitchen accessories,preethi mixer grinder,preethi wet grinder,preethi products,preethi steele mixer grinder,preethi mixer grinder jar')
@elseif(strpos($page->page, 'Disclaimer') !== false)
    @section('description','This disclaimer details our obligations to you regarding our website. Provided and approved by legal forms provider Jumbocanada Distribution Inc.')
    @section('keywords','')
@endif
@section('keywords','')
@section('canonical')https://jumbocanada.com/policy/{{strtolower($page->page)}} @endsection
@section('content')
    <div class="shipping page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <ul class="quick-nav">
                        <li class="{{(Request::segment(2)=='shipping-and-return-policy')? 'active' : ''}}"><a
                                href="{{route('policy','shipping-and-return-policy')}}">Shipping & Return Policy</a>
                        </li>
                        <li class="{{(Request::segment(2)=='privacy-policy')? 'active' : ''}}"><a
                                href="{{route('policy','privacy-policy')}}">Privacy Policy</a></li>
                        <li class="{{(Request::segment(2)=='terms-and-conditions-of-warranty-and-safe-appliances-usage')? 'active' : ''}}">
                            <a href="{{route('policy','terms-and-conditions-of-warranty-and-safe-appliances-usage')}}">Terms
                                & Conditions Of Warranty & Safe Appliances Usage</a></li>
                        <li class="{{(Request::segment(2)=='disclaimer')? 'active' : ''}}"><a
                                href="{{route('policy','disclaimer')}}">Disclaimer</a></li>
                    </ul>
                </div>
                <div class="col-sm-9">
                    {!! $page->content !!}
                </div>
            </div>
        </div>
    </div>


@endsection
