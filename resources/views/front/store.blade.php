@extends('front.layouts.app')
@section('title','Jumbo Stores Canada | Kitchen Appliances and Cookware')
@section('description','Explore Jumbo’s entire range of home appliances and accessories near you! Enjoy the best experience at our retail store.')
@section('keywords','Kitchen Appliances and Cookware,kitchen accessories store,kitchen shop online shopping,kitchen supplies online canada,kitchen products,kitchen online shop,kitchen supplies shop,kitchen supply store canada,online store for kitchen accessories')
@section('canonical','https://jumbokitchenappliances.com/store-locator')
@section('content')

    <div class="stores page">
        <div class="container-fluid">
            <h1>Distribution Stores & Support</h1>
            <div id="accordion-two">
                @foreach($stores as $store)
                    @if($store->address=='Jumbo Canada Online Store') @continue;@endif
                    <div class="card">
                        <div class="card-header" id="headingTwo{{$store->id}}">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse"
                                        data-target="#property-details-{{$store->id}}" aria-expanded="false"
                                        aria-controls="property-details-{{$store->id}}">
                                    {{$store->city}}
                                    <span></span>
                                </button>
                            </h5>
                        </div>
                        <div id="property-details-{{$store->id}}" class="collapse show" aria-labelledby="headingTwo{{$store->id}}"
                             data-parent="#accordion-two" >
                            <div class="card-body">
                                <p>{{$store->address}}</p>
                                <p> Phone - {{$store->phone}}</p>
                                <p> Email - {{$store->email}}</p>
                                @if($store->website_url != '') 
                                 <p>Website - <a href="{{$store->website_url}}" target="_blank">{{$store->website_url}}</a></p>
                               @endif
                                <a target="_blank" href="https://maps.google.com/?q={{$store->mapLocation}}" class="view-map">View location map</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>



@endsection
