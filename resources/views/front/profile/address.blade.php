@extends('front.layouts.profile')
@section('subtitle','| Addresses')
@section('description','')
@section('keywords','')
@section('profileHead','My Addresses')
@section('profileContent')
    @php($customer=Auth::guard('customer')->user())
    <h6>@if($customer->addresses()->count() == 0) No Saved Addresses Found @endif</h6>
    <a href="" class="add-new" data-toggle="modal" data-target="#add-address">add new</a>
    <div class="modal fade" id="add-address" tabindex="-1" role="dialog"
         aria-labelledby="consultation-modal"
         style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Addresses</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <form action="{{route('storeaddress')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="text" placeholder="First Name" name="firstname" required class="form-control"
                                           value="">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" placeholder="Last Name" name="lastname" required class="form-control"
                                           value="">
                                </div>
                            </div>
                            <input type="text" placeholder="Company" name="company"  class="form-control"
                                   value="">
                            <input type="text" placeholder="Address Line 1" name="address_1"  class="form-control"
                                   value="" required>
                            <input type="text" placeholder="Address Line 2" name="address_2" class="form-control"
                                   value="">
                            <div class="form-group">
                                <select name="province" class="form-control" id="province" required>
                                    <option value="">Select Province</option>
                                    <option value="Nunavut" >Nunavut</option>
                                    <option value="Quebec">Quebec</option>
                                    <option value="Northwest Territories" >Northwest Territories</option>
                                    <option value="Ontario">Ontario</option>
                                    <option value="British Columbia">British Columbia</option>
                                    <option value="Alberta">Alberta</option>
                                    <option value="Saskatchewan" >Saskatchewan</option>
                                    <option value="Manitoba">Manitoba</option>
                                    <option value="Yukon" >Yukon</option>
                                    <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
                                    <option value="New Brunswick">New Brunswick</option>
                                    <option value="Nova Scotia" >Nova Scotia</option>
                                    <option value="Prince Edward Island">Prince Edward Island</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="text" placeholder="City" name="city" class="form-control"
                                           value="">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" placeholder="ZIP Code" name="postcode" class="form-control"
                                           value="">
                                </div>
                            </div>
                            <div class="pretty p-default p-curve">
                                <input type="checkbox" name="custom_field" value="yes" checked>
                                <div class="state">
                                    <label>Make this my default address</label>
                                </div>
                            </div>
                            <div class="actions">
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <button type="button" data-dismiss="modal"
                                                aria-label="Close" class="form-control">cancel
                                        </button>
                                    </li>
                                    <li class="list-inline-item">
                                        <button type="submit" class="form-control">create</button>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- address -->
    @if($customer->addresses()->count() > 0)
        <div class="row">
        @foreach($customer->addresses->sortByDesc('id') as $address)
            @if($address->type !='billing' && $address->type !='hidden')
            <div class="col-md-6">
            <div class="address-block">
                <p>{{$address->company}} {{$address->custom_field ? '(Default)' : ''}} </p>
                <p>{{$address->firstname.' '.$address->lastname}}</p>
                <p>{{$address->address_1}}</p>
                <p>{{$address->address_2}}</p>
                <p>{{$address->province}}</p>
                <p>{{$address->city .' - '}}{{$address->postcode}}</p>
                <ul class="list-inline actions">
                    <li class="list-inline-item"><a href="{{route('edit-address',$address->id)}}"><img src="{{asset('assets/img/edit.png')}}" alt=""
                                                                                                       class="img-fluid"></a></li>
                    <li class="list-inline-item"><a href="{{route('delete-address',$address->id)}}" onclick="return confirm('Are you sure?')" ><img src="{{asset('assets/img/close.png')}}" alt=""
                                                                                                                                                    class="img-fluid"></a></li>
                </ul>
            </div>
            </div>
            @endif
        @endforeach
        </div>
    @endif
@endsection
