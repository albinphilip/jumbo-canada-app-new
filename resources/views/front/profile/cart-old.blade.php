@extends('front.layouts.app')
@section('title','Cart')
@section('description','')
@section('keywords','')
@section('content')
    @php($customer=Auth::guard('customer')->user())
    <div class="cart page">
        <div class="container-fluid">
            <ul class="list-inline text-center breadcrumb">
                <li class="list-inline-item"><a href="{{route('cart.index')}}">CART</a></li>
                <li class="list-inline-item">CHECKOUT</li>
            </ul>
            <div class="cart-inner">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="cart-items">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h3>CART ITEMS</h3>

                                </div>
                            </div>
                        </div>
                        @if($carts->isEmpty())
                            Your car is empty!
                        @else

                            @foreach($carts as $cart)
                                <div class="cart-item">
                                    <div class="product-image">
                                        <a href="{{route('productdetail',$cart->id)}}"><img src="{{asset($cart->options['image'])}}" alt=""
                                                                                            class="img-fluid"></a>
                                    </div>
                                    <div class="product-details">
                                        <a href="{{route('productdetail',$cart->id)}}" class="pname">{{$cart->name}}</a>
                                        @if($cart->options['rating'] > 0)
                                            <div class="rating">
                                                <span>{{number_format($cart->options['rating'], 2, '.', ',')}}/5</span>
                                                <ul class="list-inline star">
                                                    @php($rating = $cart->options['rating'])
                                                    @foreach(range(1,5) as $i)
                                                        @if($rating>0)
                                                            @if($rating>0.5)
                                                                <i class="fa fa-star" style="color: goldenrod;"></i>
                                                            @else
                                                                <i class="fa fa-star-half" style="color: goldenrod;"></i>
                                                            @endif
                                                        @else
                                                            <i class="fa  fa-star-o"></i>
                                                        @endif
                                                        @php($rating--)
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="quantity">
                                            <span>Quantity</span>
                                            <ul class="list-inline">
                                                <li class="list-inline-item">
                                                    <button
                                                        onclick="location.href='{{route('cart-minus',$cart->rowId)}}'">
                                                        <img src="{{asset('assets/img/minus-button.png')}}" alt="">
                                                    </button>
                                                </li>
                                                <li class="list-inline-item">
                                                    <input type="text" placeholder="" value="{{$cart->qty}}" readonly>
                                                </li>
                                                <li class="list-inline-item">
                                                    <button
                                                        onclick="location.href='{{route('cart-plus',$cart->rowId)}}'">
                                                        <img src="{{asset('assets/img/plus-button.png')}}" alt="">
                                                    </button>
                                                </li>

                                            </ul>
                                        </div>

                                    </div>
                                    <div class="actions">
                                        <button class="delete"
                                                onclick="location.href='{{route('cart.remove',$cart->rowId)}}'"><img
                                                src="{{asset('assets/img/delete.png')}}" alt="" class="img-fluid">
                                        </button>
                                        <div class="amount">
                                            <p class="price">Price</p>
                                            @php($price = $cart->price*$cart->qty)
                                            <h5>C${{$price}}</h5>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="secure-payment">
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="{{asset('assets/img/secure.png')}}" alt="" class="img-fluid secure">
                                    <h4>100% <br><span>secure payment</span></h4>
                                </div>
                                <div class="col-sm-8">
                                    <img src="{{asset('assets/img/pay.png')}}" alt="" class="img-fluid pay">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <h3>Amount</h3>
                        <div class="cart-items-pay">
                            <table>
                                <tbody>
                                <tr>
                                    <td>TOTAL</td>
                                    <td>C${{\Cart::total()}}</td>
                                </tr>
                                @if(Session::has('coupon'))
                                    @php( $payable = Session::get('coupon'))
                                    <tr class="total">
                                        <td>PAYABLE AMOUNT</td>
                                        <td>C${{$payable}}</td>
                                    </tr>
                                @else
                                    @php( $payable = \Cart::total() )
                                @endif
                                @php(session()->put('total', $payable))
                                </tbody>
                            </table>
                        </div>
                        @if(Auth::guard('customer')->check())
                            <form action="{{route('appplyCoupon')}}" class="apply-coupon" method="post">
                                @csrf
                                <input type="text" placeholder="Apply Coupon" name="coupon" class="form-control"
                                       required>
                                <button><img src="{{asset('assets/img/coupon.png')}}" alt="" class="img-fluid"></button>
                            </form>
                        @else
                            <form action="{{route('guest.appplyCoupon')}}" class="apply-coupon" method="post">
                                @csrf
                                <input type="text" placeholder="Apply Coupon" name="coupon" class="form-control"
                                       required>
                                <button><img src="{{asset('assets/img/coupon.png')}}" alt="" class="img-fluid"></button>
                            </form>
                        @endif
                        <form>
                            @csrf
                            <div class="form-group">
                                <select name="province" class="form-control" id="province" required>
                                    <option value="">Select Province</option>
                                    <option value="Nunavut" >Nunavut</option>
                                    <option value="Quebec">Quebec</option>
                                    <option value="Northwest Territories" >Northwest Territories</option>
                                    <option value="Ontario">Ontario</option>
                                    <option value="British Columbia">British Columbia</option>
                                    <option value="Alberta">Alberta</option>
                                    <option value="Saskatchewan">Saskatchewan</option>
                                    <option value="Manitoba">Manitoba</option>
                                    <option value="Yukon">Yukon</option>
                                    <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
                                    <option value="New Brunswick">New Brunswick</option>
                                    <option value="Nova Scotia">Nova Scotia</option>
                                    <option value="Prince Edward Island">Prince Edward Island</option>
                                </select>
                            </div>
                        </form>
                        @if(!$carts->isEmpty())
                            <a href="#"  class="proceed">Proceed to checkout</a>
                            <div class="clearfix"></div>

                            @if(Session::has('msg'))
                                <p style="color:red;"> {{ Session::get('msg') }}</p>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('additionalScripts')
    <script>
        //store province
        $("#province").change(function () {
            if($(this).val()!=''){
                $('.proceed').attr("href","{{route('checkout')}}");
                url="/set-province/"+$(this).val();
                $.ajax({
                    url: url,
                    method: 'get',
                    success: function (response) {
                        console.log(response);
                    },

                })
            }
            else {
                $('.proceed').attr("href","");
            }
            //store province in session

        })
        $(".proceed").click(function () {
            $("#province").focus();
        })
    </script>
@endsection


<div class="col-sm-4">
    <h3>Payment Detail</h3>
    <div class="cart-items-pay">
        <table id="total_table">
            <tbody>
            <tr>
                <td>TOTAL</td>
                <td>C${{Session::get('total')}}</td>
            </tr>
            <tr>
                <td> SHIPPING CHARGE</td>
                <td id="shipping">C${{$shipping}}</td>
            </tr>
            <tr>
                <td>TAX ({{$tax}} %)</td>
                <td id="tax">C${{$taxed}}</td>
            </tr>
            <tr class="total">
                <td>TOTAL AMOUNT</td>
                <td id="amount">C${{Session::get('final_amount')}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    @if(!$carts->isEmpty())
        <form action="{{route('confirm-order')}}"  method="post" id="payment-form" style="display: none">
            @csrf
            <div class="form-group">
                <div class="card-body">
                    <div id="card-element" style="border: 1px solid;padding: 20px">
                        <!-- A Stripe Element will be inserted here. -->
                    </div>
                    <!-- Used to display form errors. -->
                    <div id="card-errors" role="alert"></div>
                    <input type="hidden" name="plan" value="" />
                </div>
            </div>
            <div class="card-footer" style="background-color: #ffffff;border: none">
                <button
                    id="card-button"
                    class="btn btn-danger"
                    type="submit"
                    data-secret="{{ $intent }}"
                > Make Payment </button>
                <span id="msg"></span>
            </div>
        </form>
        @if(Session::has('msg'))
            <p style="color:red;"> {{ Session::get('msg') }}</p>
        @endif
    @endif
</div>
<div class="col-sm-4">
    <h3>Payment Details</h3>
    <div class="cart-items-pay">
        <table>
            <tbody>
            <tr>
                <td>TOTAL</td>
                <td>C${{Session::get('total')}}</td>
            </tr>
            <tr>
                <td> SHIPPING CHARGE</td>
                <td id="shipping">C${{$shipping}}</td>
            </tr>
            <tr>
                <td>TAX ({{$tax}} %)</td>
                <td id="tax">C${{$taxed}}</td>
            </tr>
            <tr class="total">
                <td>TOTAL AMOUNT</td>
                <td id="amount">C${{Session::get('final_amount')}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    @if(!$carts->isEmpty())
        @if(session()->has('data'))
            @php($guest = session()->get('data'))
            <form action="{{route('guest.confirm-order',$guest->id)}}"  method="post" id="payment-form">
                @csrf
                <div class="form-group">
                    <div class="card-body">
                        <div id="card-element" style="border: 1px solid;padding: 20px">
                            <!-- A Stripe Element will be inserted here. -->
                        </div>
                        <!-- Used to display form errors. -->
                        <div id="card-errors" role="alert"></div>
                        <input type="hidden" name="plan" value="" />
                    </div>
                </div>
                <div class="card-footer" style="background-color: #ffffff;border: none">
                    <button
                        id="card-button"
                        class="btn btn-danger"
                        type="submit"
                        data-secret="{{ $intent }}"
                    > Make Payment </button>
                    <span id="msg"></span>
                </div>
            </form>
        @endif
    @endif
</div>
@endif
