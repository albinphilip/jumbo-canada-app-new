<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('favicon .png')}}" rel="icon">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
@yield('additionalStyles')
<!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper" id="app">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{route('home')}}" class="nav-link" target="_blank"><i class="fa fa-home"></i> Main Website</a>
            </li>
        </ul>
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    {{ Auth::user()->name }} <i class="fa fa-cog"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">User Settings</span>
                    <div class="dropdown-divider"></div>
                    <a href="{{route('password.edit',Auth::user()->id)}}" class="dropdown-item">
                        <i class="fa fa-key"></i> Reset Password
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-user"></i> Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{route('adminHome')}}" class="brand-link">
            <span class="brand-text">{{env('APP_NAME')}}</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->

                    <li class="nav-item">
                        <a href="{{route('adminHome')}}" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-box"></i>
                            <p>
                                Products
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('product.index')}}" class="nav-link">
                                    <i class="fa fa-eye nav-icon"></i>
                                    <p>View all Products</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('category.index')}}" class="nav-link">
                                    <i class="fa fa-bars nav-icon"></i>
                                    <p>Product Categories</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('brand.index')}}" class="nav-link">
                                    <i class="fas fa-cube nav-icon"></i>
                                    <p>Product Brands</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('serial.index')}}" class="nav-link">
                                    <i class="nav-icon fas fa-barcode"></i>
                                    <p>
                                        Product Serial Numbers
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('banner.index')}}" class="nav-link">
                                    <i class="fas fa-image nav-icon"></i>
                                    <p>Change Banner</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('customer.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Customer
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('couponcode.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-receipt"></i>
                            <p>
                                Coupon Codes
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('shippingdiscount.index')}}" class="nav-link">
                            <i class="nav-icon fa fa-ticket-alt"></i>
                            <p>
                                Flat Shipping Rate
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('free_shipping.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-tag"></i>
                            <p>
                                Free Shipping
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('fees.index')}}" class="nav-link">
                            <i class="nav-icon fa fa-money-bill-alt"></i>
                            <p>
                                Additional Fees & Taxes
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('order.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-shopping-cart"></i>
                            <p>
                                Order
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('abandoned_cart')}}" class="nav-link">
                            <i class="nav-icon fas fa-shopping-basket"></i>
                            <p>
                                Abandoned Cart
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('return.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-reply"></i>
                            <p>
                                Product Return
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('enquiry.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-mail-bulk"></i>
                            <p>
                                Enquiry
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('popup.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-bell"></i>
                            <p>
                                Popup
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-registered"></i>
                            <p>
                                Registration
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="{{route('product-registration.index')}}" class="nav-link">
                                    <i class="nav-icon fa fa-star"></i>
                                    <p>
                                        Warranty Registration
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('product-repair-registration.index')}}" class="nav-link">
                                    <i class="nav-icon fa fa-wrench"></i>
                                    <p>
                                        Repair Registration
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('buy-back.index')}}" class="nav-link">
                                    <i class="nav-icon fa fa-recycle"></i>
                                    <p>Buy Back</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('discount.index')}}" class="nav-link">
                                    <i class="nav-icon fas fa-plus-circle"></i>
                                    <p>
                                        Discount Requests
                                    </p>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-copy"></i>
                            <p>
                                Reports
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="{{route('tax')}}" class="nav-link">
                                    <i class="nav-icon fa fa-file-pdf"></i>
                                    <p>
                                        Tax Report
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('order')}}" class="nav-link">
                                    <i class="nav-icon fa fa-file-alt"></i>
                                    <p>
                                        Order Report
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('performance')}}" class="nav-link">
                                    <i class="nav-icon fa fa-file"></i>
                                    <p>
                                        Performance Report
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('return-report')}}" class="nav-link">
                                    <i class="nav-icon fa fa-file"></i>
                                    <p>
                                        Return Report
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-file"></i>
                            <p>
                                Editable Pages
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="{{route('faq.index')}}" class="nav-link">
                                    <i class="nav-icon fa fa-question"></i>
                                    <p>
                                        FAQ
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('pages.edit',1)}}" class="nav-link">
                                    <i class="nav-icon fa fa-file"></i>
                                    <p>
                                        Shipping Policy
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('pages.edit',2)}}" class="nav-link">
                                    <i class="nav-icon fa fa-file"></i>
                                    <p>
                                        Privacy Policy
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('pages.edit',3)}}" class="nav-link">
                                    <i class="nav-icon fa fa-file"></i>
                                    <p>
                                        Terms and Conditions
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('pages.edit',4)}}" class="nav-link">
                                    <i class="nav-icon fa fa-file"></i>
                                    <p>
                                        Disclaimer
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('store.index')}}" class="nav-link">
                                    <i class="nav-icon fa fa-shopping-bag"></i>
                                    <p>
                                        Stores
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('product-banner.index')}}" class="nav-link">
                                    <i class="nav-icon fa fa-image"></i>
                                    <p>Product sliders </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@yield('title')</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
                            @if(request()->segment(2))
                                @if(Route::has(request()->segment(2).".index"))
                                    <li class="breadcrumb-item"><a
                                            href="{{route(request()->segment(2).".index")}}">{{ucfirst(request()->segment(2))}}</a>
                                    </li>
                                @else
                                    <li class="breadcrumb-item active">{{ucfirst(request()->segment(2))}}</li>
                                @endif
                            @endif
                            @if(request()->segment(3))
                                <li class="breadcrumb-item active">{{ucfirst(request()->segment(3))}}</li>
                            @endif
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <strong>Copyright &copy; {{ now()->year }} <a href="https://mapletechspace.com.com">Maple Techspace</a>.</strong> All
        rights reserved.
    </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>

<script src="{{asset('/plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
<script> $('[data-mask]').inputmask()</script>

<!-- Select2 -->
<script src="{{asset('/plugins/select2/js/select2.full.min.js')}}"></script>
<script>
    //Script to activate menu item based on current URL
    var url = window.location;
    // for sidebar menu entirely but not cover treeview
    $('ul.nav-sidebar a').filter(function () {
        return this.href == url;
    }).addClass('active');

    // for treeview
    $('ul.nav-treeview a').filter(function () {
        return this.href == url;
    }).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
</script>
<script type="text/javascript">
    $(function () {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        @if ($message = Session::get('success'))
        Toast.fire({
            icon: 'success',
            title: '{{$message}}.'
        })
        @endif
        @if ($message = Session::get('error'))
        Toast.fire({
            icon: 'error',
            title: '{{$message}}.'
        })
        @endif
        @if ($message = Session::get('warning'))
        Toast.fire({
            icon: 'warning',
            title: '{{$message}}.'
        })
        @endif
    });
</script>
@yield('additionalScripts')
</body>
</html>
