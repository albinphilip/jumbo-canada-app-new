@extends('admin.layouts.form')
@section('title','Add/Edit Free shipping amount')
@section('actionUrl')
    {{route('free_shipping.store')}}
@endsection
@section('actionName','Add')
@section('indexRoute')
    {{route('free_shipping.index')}}
@endsection
@section('formBody')

    <div class="col-6">
        <div class="form-group">
            <label for="rate" class="control-label">Free Shipping Amount</label>
            <input type="text" id="rate" name="rate"  class="form-control {{$errors->has('rate') ? 'is-invalid' : ''}}"
                   @if($rate!=null) value="{{$rate->rate}}" @else value = "{{old('rate')}}" @endif placeholder="Enter the  amount to avail free shipping">
            @if($errors->has('rate'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('rate')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection

