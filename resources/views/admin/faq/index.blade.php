@extends('admin.layouts.datatable')
@section('title','FAQ')
@section('tableTitle','FAQ')
@section('createRoute')
    {{route('faq.create')}}
@endsection
@section('button')
    <a class="btn btn-primary btn-sm mr-2" href="@yield('createRoute')">Add new</a>
@endsection
@section('tableHead')
    <th>#</th>
    <th>Question</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($faqs->sortByDesc('id') as $faq)
        <tr>
            <td></td>
            <td>{{$faq->question}}</td>
            <td>
                <form action="{{route('faq.destroy',$faq->id)}}" method="POST" id="delete-form-{{$faq->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <a href="#" onclick="return confirmation({{$faq->id}});"><i class="fa fa-trash"></i> </a>
                </form>
                <a href="{{route('faq.edit',$faq->id)}}"><i class="fa fa-edit"></i> </a>
            </td>
        </tr>
    @endforeach
@endsection
