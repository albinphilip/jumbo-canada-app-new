@extends('admin.layouts.datatable')
@section('title','View Products')
@section('tableTitle','View Products')
@section('createRoute')
    {{route('product.create')}}
@endsection
@section('button')
    <a class="btn btn-primary btn-sm mr-2" href="@yield('createRoute')">Add new</a>
@endsection
@section('tableHead')
    <th>#</th>
    <th>Name</th>
    <th>Category</th>
    <th>Image</th>
    <th>Status</th>
    <th>Bestseller</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($products->sortByDesc('id') as $product)
        <tr>
            <td></td>
            <td>{{$product->name}}</td>
            <td>{{$product->category->name}}</td>
            <td>@if($product->getImage()->count() > 0)<img src="{{asset($product->getImage->image_path)}}"
                                                           width="100"> @endif</td>
            <td>{{$product->status}}</td>
            <td>
                @if($product->bestseller == 'yes')
                    <p class="badge badge-success">Bestseller</p>
                    <a href="{{route('product.remove-bestseller',$product->id)}}" onclick=" confirm('Are You Sure?');" ><i class="fas fa-window-close" style="color: red;"></i></a>

                @else
                    <a href="{{route('product.bestseller',$product->id)}}" class="btn btn-primary btn-sm">Add</a>
                @endif
            </td>
            <td>
                <form action="{{route('product.destroy',$product->id)}}" method="POST"
                      id="delete-form-{{$product->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <a href="#" onclick="return confirmation({{$product->id}});"><i class="fa fa-trash"></i> </a>
                </form>
                <a href="{{route('product.edit',$product->id)}}"><i class="fa fa-edit"></i> </a><br/>
            </td>
        </tr>
    @endforeach
@endsection
