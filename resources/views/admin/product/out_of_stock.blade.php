@extends('admin.layouts.datatable')
@section('title','Products Going To Out of Stock')
@section('tableTitle','Products Going To Out of Stock')

@section('tableHead')
    <th>#</th>
    <th>Name</th>
    <th>Variant</th>
    <th>Quantity</th>
@endsection
@section('tableBody')
    @foreach($outs->sortByDesc('id') as $out)
        <tr>
            <td></td>
            <td>{{$out->product->name}}</td>
            <td>{{$out->value}}</td>
            <td>{{$out->quantity}}</td>
        </tr>
    @endforeach
@endsection
