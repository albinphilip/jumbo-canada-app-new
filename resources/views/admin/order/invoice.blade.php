@extends('admin.layouts.app')
@section('additionalStyles')
    <style>
        #tracking-modal .modal-dialog {
            max-width: 700px;
            border-radius: 0;
        }
        #tracking-modal .modal-dialog .modal-content {
            border-radius: 0;
        }
        #tracking-modal .modal-dialog .modal-content .modal-header {
            padding: 0;
            border: 0;
        }
        #tracking-modal .modal-dialog .modal-content .modal-header button {
            padding: 5px;
            margin: 0 0 0 auto;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body h4 {
            text-transform: capitalize;
            font-weight: 600;
            font-size: 23px;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body h5 {
            font-weight: 600;
            font-size: 18px;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day {
            padding-top: 25px;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day h6 {
            font-weight: 600;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location {
            list-style: none;
            padding: 0;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location li {
            margin-bottom: 15px;
            display: flex;
            width: 100%;
            flex-wrap: wrap;
            align-items: center;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location li .time {
            display: inline-block;
            width: 125px;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location li .location {
            width: calc(100% - 125px);
            display: inline-block;
            position: relative;
            padding-left: 10px;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location li .location:before {
            position: absolute;
            left: 0;
            top: 50%;
            height: 100%;
            background-color: #869791;
            width: 1px;
            content: "";
            transform: translateY(-50%);
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location li .location h6 {
            margin-bottom: 0;
            font-size: 15px;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location li .location span {
            margin-bottom: 0;
            font-style: italic;
            font-size: 14px;
        }
    </style>
@endsection
@section('title','Order Details')
@section('content')
    <div class="row">
        <!-- Main content -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <!-- title row -->
                <div class="row">
                    <div class="col-12">
                        <h4>
                            <!-- <i class="fas fa-globe"></i> AdminLTE, Inc.-->
                            <small class="float-right">Order Date: {{date_format($orders[0]->created_at,'d-M-Y')}}</small>
                        </h4>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        <i class="fa fa-map-marker-alt"></i> Shipping Address
                        <address >
                            <strong>{{$orders[0]->deliveryAddress->firstname.' '.$orders[0]->deliveryAddress->lastname}}</strong><br>
                            {{$orders[0]->deliveryAddress->address_1}}<br/>
                            @if($orders[0]->deliveryAddress->address_2 != '')
                                {{$orders[0]->deliveryAddress->address_2}}<br/>
                            @endif
                            {{$orders[0]->deliveryAddress->city}}<br/>
                            {{$orders[0]->deliveryAddress->postcode}}<br/>
                            {{$orders[0]->deliveryAddress->province}}
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <i class="fa fa-map-marker-alt"></i> Billing Address
                        <address>
                            <strong>{{$orders[0]->billingAddress->firstname.' '.$orders[0]->billingAddress->lastname}}</strong><br>
                            {{$orders[0]->billingAddress->address_1}}<br/>
                            @if($orders[0]->billingAddress->address_2 != '')
                                {{$orders[0]->billingAddress->address_2}}<br/>
                            @endif
                            {{$orders[0]->billingAddress->city}}<br/>
                            {{$orders[0]->billingAddress->postcode}}<br/>
                            {{$orders[0]->billingAddress->province}}
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <a href="{{route("download-pdf",$orders[0]->order_id)}}"><i class="fa fa-hashtag"></i>
                            <b>Order ID:</b> {{$orders[0]->order_id}}<br></a>
                        <br>
                        <i class="fa fa-user pr-1" ></i><b>Customer:</b> {{$orders[0]->customer->firstname.' '.$orders[0]->customer->lastname}}<br>
                        <i class="fa fa-envelope pr-1"></i><b>Email Id:</b>{{$orders[0]->customer->email}}<br>
                        <i class="fa fa-phone-alt pr-1"></i><b>Phone:</b> {{$orders[0]->customer->telephone}}<br/>
                        <br>
                        @if($orders[0]->couponCode != null)
                            <b>Coupon Code:</b>{{$orders[0]->couponCode->code}}<br/>
                        @endif
                        @if($orders[0]->instruction != null)
                            <b>Delivery Instructions:</b> {{$orders[0]->instruction}}<br>
                        @endif

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product</th>
                                <th>Qty</th>
                                <th>Product Price</th>
                                <th>Status</th>
                                <th>Change Status</th>
                                <th>Delivery Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1)
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$order->product->name}}</td>
                                    <td>{{$order->quantity}}</td>
                                    <td>C${{$order->product_price}}</td>
                                    <td>{{$order->status}}</td>
                                    <td>
                                        <form action="{{route('order.update',$order->id)}}" method="POST" id="form{{$order->id}}">
                                            @method('PATCH')
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <select id="status{{$order->id}}" name="status" required class="form-control {{$errors->has('status') ? 'is-invalid' : ''}}" onchange="changestatus({{$order->id}})">

                                                    <option value="Order placed" @if($order->status == 'Order placed' &&  $order->status != 'Cancel requested') selected @else hidden @endif>
                                                        Order placed</option>

                                                    <option value="Processing"
                                                        {{$order->status == 'Processing' ? 'selected' : ($order->status == 'Order placed' ? '' : 'hidden')}}>
                                                        Processing</option>
                                                    <option value="Shipped"
                                                            {{$order->status == 'Shipped' ? 'selected' : ''}}
                                                            @if($order->status != 'Order placed' && $order->status != 'Processing' && $order->status != 'Shipped') hidden @endif>
                                                        Shipped</option>

                                                    <option value="Delivered"
                                                            {{$order->status == 'Delivered' ? 'selected' : ''}}
                                                            @if($order->status != 'Order placed' && $order->status != 'Processing' && $order->status != 'Shipped' && $order->status != 'Delivered')
                                                            hidden @endif>
                                                        Delivered</option>
                                                    <option value="Cancel requested" {{$order->status == 'Cancel requested' ? 'selected' : ''}}
                                                    @if( $order->status != 'Cancel requested' && $order->status != 'Order placed' && $order->status !='Processing' ) hidden
                                                        @endif>Cancel requested</option>

                                                    <option value="Cancel & refunded"
                                                            {{$order->status == 'Cancel & refunded' ? 'selected' :''}}
                                                            @if( $order->status != 'Cancel & refunded' && $order->status != 'Cancel requested') hidden @endif >
                                                        Cancel & refunded</option>

                                                    <option value="Return Requested" {{$order->status == 'Return Requested' ? 'selected' : ''}}
                                                    @if( $order->status != 'Return Requested') hidden
                                                        @endif>Return Requested</option>

                                                    <option value="Return Confirmed"
                                                            {{$order->status == 'Return Confirmed' ? 'selected' :''}}
                                                            @if( $order->status != 'Return Confirmed' && $order->status != 'Return Requested') hidden @endif >
                                                        Return Confirmed</option>
                                                </select>
                                                @if($errors->has('status'))
                                                    <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('status')}}</strong>
                                                     </span>
                                                @endif
                                                @if($order->status == 'Shipped' && $order->shipping_partner == '')
                                                    <a href="" class="add-new" data-toggle="modal" data-order="{{$order->id}}" data-target="#shipping-details" id="model">Add Details</a>
                                                @endif
                                            </div>
                                        </form><br/>
                                        <div class="wait" ><span data-id="{{$order->id}}" ></span></div>
                                    </td>
                                    <td>{{$order->delivery_date ? date('M-d-Y',strtotime($order->delivery_date)) : ''}} <br/>
                                        <a href="" class="add-new" data-toggle="modal" data-id="{{$order->id}}" data-date="{{$order->delivery_date}}" data-target="#delivery" id="model"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                                @php($i++)
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">

                    <div class="col-12">
                        <p class="lead"><strong>Order Fee Split up</strong></p>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Subtotal</th>
                                    <th>Tax</th>
                                    <th>Shipping Rate</th>
                                    <th>Reduction</th>
                                    <th>Order Total</th>
                                </tr>
                                </thead>
                                <tr>
                                    <td>C${{$order->orderFeesplitup->sub_total}}</td>
                                    <td>{{$order->orderFeesplitup->tax.' '}}%</td>
                                    <td>C${{$order->orderFeesplitup->shipping_charge}}</td>
                                    <td>C${{$order->orderFeesplitup->reduction}}</td>
                                    <td>C${{$order->order_total}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-12 pt-3">
                        <p class="lead"><strong>Shipping Details</strong></p>
                        <table class="table table-bordered table-striped ">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Shipping Partner</th>
                                <th>Shipment Id</th>
                                <th>Shipping Date</th>
                                <th>Shipping Charge</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($flag = 0)
                            @foreach($orders as $order)
                                @for($i = 0; $i <$order->quantity; $i++)
                                    @if($order->shipping_partner != null )
                                        @php($flag = 1)
                                        <tr>
                                            <td>{{$order->product->name}}({{$i+1}})</td>
                                            <td>{{$order->shipping_partner}}</td>
                                            <td>
                                                @if($order->shipping_partner != 'Canada Post')
                                                    {{$order->shippment_number}}
                                                @else
                                                    @php($ship_array = explode(',',$order->shipment_id))
                                                    {{$ship_array[$i]}}
                                                @endif
                                            </td>
                                            <td>{{date('M-d-Y',strtotime($order->shipping_date))}}
                                            </td>
                                            <td>
                                                @if($order->shipping_partner == 'Canada Post')
                                                    @php($price_array = explode(',',$order->shipment_price))
                                                    C${{$price_array[$i]}}
                                                @else
                                                    {{'NA'}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($order->shipping_partner == 'Canada Post')
                                                    @php($track_array = explode(',',$order->tracking_pin))
                                                    @php($file_array = explode(',',$order->shipping_label))
                                                    @php($manifest_array = explode(',',$order->manifest_file))

                                                    <p><a href="{{asset(basename($file_array[$i]))}}" target="_blank" class="btn btn-primary btn-sm" style="white-space: nowrap">
                                                            <i class="fa fa-download"> </i> Shipping Label</a></p>
                                                    @if($manifest_array[$i] != null)
                                                    <p><a href="{{asset(basename($manifest_array[$i]))}}" target="_blank" class="btn btn-primary btn-sm" style="white-space: nowrap">
                                                            <i class="fa fa-download"> </i> Manifest File</a></p>
                                                    @endif
                                                    <a href="" id="track{{$order->id}}" class="btn btn-primary btn-sm"
                                                       onclick="return track({{$track_array[$i]}});" type="button">Track </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                    @endfor
                            @endforeach
                            @if($flag != 1)
                                <tr>
                                    <td colspan="6" style="text-align: center;">Not Added</td>
                                </tr>
                            @endif
                            </tbody>

                        </table>


                    </div>
                    <!-- /.col -->

                </div>
                <!-- /.row -->
                </div>
            </div>
        </div>
    </div>
    <!-- model -->

    <div class="modal fade" id="shipping-details" tabindex="-1" role="dialog"
         aria-labelledby="consultation-modal"  data-backdrop="false"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Shipment Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <form action="{{route('order.shippingDetails')}}" method="post" name="shipping">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6" hidden>
                                    <input type="text" name="order_id" id="order_id" value="" class="form-control">
                                    <span class="text-danger">
                                        <small><strong id="order__id"></strong></small>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select name="shipping_partner" class="form-control" id="partner" required>
                                            <option value="">Select Partner</option>
                                            <option value="Fedex" >Fedex</option>
                                            <option value="Canada Post">Canada Post (Automatic)</option>
                                            <option value="Canada Post (Manual)">Canada Post (Manual)</option>                                        </select>
                                        <span class="text-danger">
                                            <small><strong id="partner-error"></strong></small>
                                        </span>
                                    </div>
                                </div>
                                 <div class="col-sm-6 details" hidden>
                                     <input type="text" placeholder="Shipping Number" name="shipping_number" id="number"  required class="form-control"
                                            value="">
                                     <span class="text-danger">
                                        <small><strong id="number-error"></strong></small>
                                    </span>
                                 </div>
                                <div class="col-sm-6">
                                    <input type="date" placeholder="shipping date" name="shipping_date" id="date"  required class="form-control"
                                           value="{{date('Y-m-d')}}">
                                    <span class="text-danger">
                                        <small><strong id="date-error"></strong></small>
                                    </span>
                                </div>

                                </div>
                            <div class="actions pt-3">
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <button type="button" data-dismiss="modal"
                                                aria-label="Close" class="form-control">cancel
                                        </button>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="btn btn-primary" onclick="formSubmit();">Add</a>
                                    </li>
                                    <li style="display:none;" id="response"><i class="fa fa-cog fa-spin"></i>Please Wait</li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- model end-->
    <!--delivery date-->
    <div class="modal fade" id="delivery" tabindex="-1" role="dialog"
         aria-labelledby="consultation-modal"  data-backdrop="false"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Estimated Delivery Date</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <form action="{{route('order.deliveryDate')}}" method="post" name="delivery">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6" hidden>
                                    <input type="text" name="delivery_order_id" id="delivery_order_id" value="" class="form-control">
                                    <span class="text-danger">
                                        <small><strong id="delivery_order_id"></strong></small>
                                    </span>
                                </div>

                                <div class="col-sm-6">
                                    <input type="date" placeholder="Delivery date" name="delivery_date" id="delivery_date"  required class="form-control"
                                           value="">
                                    <span class="text-danger">
                                        <small><strong id="delivery_date-error"></strong></small>
                                    </span>
                                </div>
                            </div>
                            <div class="actions">
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <button type="button" data-dismiss="modal"
                                                aria-label="Close" class="form-control">cancel
                                        </button>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="btn btn-primary" onclick="deliverySubmit();">Update</a>
                                    </li>
                                    <li style="display:none;" id="delivery_response"><i class="fa fa-cog fa-spin"></i>Please Wait</li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- delivery date end -->
    <!-- tracking modal -->
    <div class="modal" id="tracking-modal" tabindex="-1" role="dialog"
         aria-labelledby="tracking-modal" data-backdrop="false"
         style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!--                <h5 class="modal-title">Add Addresses</h5>-->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <h4>shipped with Canada Post</h4>
                        <h5>Tracking ID: <span id="track_id"></span></h5>
                        <div class="day">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script type="text/javascript">
        function  changestatus(id) {
            var status = $('#status'+id).val();
            var url=$('#form'+id).attr('action');
            $(".wait span[data-id="+id+"]").html('<i class="fa fa-cog fa-spin"></i>');
            //alert(url);
            console.log(status);
            $.ajax({
                type: "PATCH",
                url: url,
                cache: false,
                data: {_token: '{{csrf_token()}}',_method:"PATCH",status: status},
                success: function (result) {
                    if(result == 'success')
                    {
                        fireTost('Status updated');
                        setInterval('location.reload()', 2500);
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Failed to update status");
                }
            });
        }
        function fireTost(message) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }
        function formSubmit()
        {
            partner = $('#partner').val();
            date = $('#date').val();
            if(partner != 'Canada Post'){
                number = $('#number').val();
            }
            else{
                number = '';
            }
            id = $('#order_id').val();
            ii = $('#shipping-details').attr('data-id-order_id');
            console.log(ii);
            if(!partner) $('#partner-error').html('Select Shipping Partner');
            if(partner != 'Canada Post' && !number) $('#number-error').html('This Field is required');
            if( !date) $('#date-error').html('Select Shipping Date');
            else {
                $('#response').css('display','block');
                url = '/admin/order/shipping-details';
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    data: {_token: '{{csrf_token()}}',_method:"post",partner: partner,number:number,id:id,date:date},
                    success: function (result) {
                        if(result == 'success')
                        {
                            console.log(partner);
                            $('#shipping-details').modal('hide');
                            fireTost('Details Added');
                            setInterval('location.reload()', 2000);
                        }
                        else if(result == 'error')
                        {
                            $('#shipping-details').modal('hide');
                            alert('Failed to generate shipping label!');
                            setInterval('location.reload()', 2000);
                        }
                        else if(result == 'dimension')
                        {
                            $('#shipping-details').modal('hide');
                            alert('Please Add product dimensions!');
                            setInterval('location.reload()', 2000);
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Failed to update status");
                    }
                });
            }

        }
        $(document).ready(function(){
            $('#shipping-details').on("show.bs.modal", function (e) {

                $("#order_id").val($(e.relatedTarget).data('order'));
            });
            $('#delivery').on("show.bs.modal", function (e) {

                $("#delivery_order_id").val($(e.relatedTarget).data('id'));
                $("#delivery_date").val($(e.relatedTarget).data('date'));
            });
        });

        function deliverySubmit()
        {
            date = $('#delivery_date').val();
            id = $('#delivery_order_id').val();

            if(!date) $('#delivery_date-error').html('Select Estimated Delivery Date');
            else {
                $('#delivery_response').css('display','block');
                if(!date) $('#delivery_date-error').html('');
                url = '/admin/order/delivery-date';
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    data: {_token: '{{csrf_token()}}',_method:"post",id:id,date:date},
                    success: function (result) {
                        console.log(result);
                        if(result == 'success')
                        {
                            console.log('success');
                            $('#delivery').modal('hide');
                            fireTost('Delivery Date Updated');
                            setInterval('location.reload()', 2000);
                        }

                        else if(result == 'date-error')
                        {
                            $('#delivery_response').css('display','none');
                            $('#delivery_date-error').html('Select a date in future');
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Failed to update date");
                    }
                });
            }

        }
        // shipping details
        $('#partner').on('change',function (){
           partner = $(this).val();
           if(partner != 'Canada Post'){
               $('.details').removeAttr('hidden');
           }
           else{
               $('.details').attr('hidden',true);
           }
        });
    </script>
    <script>
            function track(pin) {
                //id = $(e.relatedTarget).data('pin');
                $("#track_id").text(pin);
                url = '/admin/order/tracking/'+pin;
                //alert('hh');
                $.ajax({
                    type: "GET",
                    url: url,
                    cache: false,
                    data: {_token: '{{csrf_token()}}',_method:"get",id:pin},
                    success: function (result) {
                        if(result.msg == 'success')
                        {
                            //console.log(result.datas[0]);
                            if(!result.datas[0])
                                $('.day').html('<p> Tracking History Not Available</p>');
                            else {
                                $('.day').html('');
                                for (i = 0; i < result.datas.length; i++) {
                                    c_date =  result.datas[i]["date"][0];
                                    d_date = c_date.split('-');
                                    new_date = new Date(d_date[0],d_date[1]-1,d_date[2]);
                                    $('.day').append('<h6>' +new_date.toDateString() + '</h6>' +
                                        '<ul class="time-location">' +
                                        '<li>' +
                                        '<div class="time">' + result.datas[i]["time"][0] + '</div>' +
                                        '<div class="location">' +
                                        '<h6>' + result.datas[i]["description"][0] + '</h6>' +
                                        '<span>' + result.datas[i]["site"][0] + '</span>' +
                                        '</div>' +
                                        '</li>' +
                                        '</ul>');
                                }
                            }
                            $('#tracking-modal').modal('show');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Failed to get tracking details");
                    }
                });
                return false;
            }
    </script>
@endsection
