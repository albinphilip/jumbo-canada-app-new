@extends('admin.layouts.form')
@section('title','Edit Additional Fees')
@section('actionUrl')
    {{route('fees.update',$fee)}}
@endsection
@section('actionName','Update')
@section('indexRoute')
    {{route('partner.index')}}
@endsection
@section('formBody')
    <!-- this is needed in edit form -->
    <input type="hidden" name="_method" value="PUT">
    <!-- ---------------------------- -->
    <div class="col-6">
        <div class="form-group">
            <label for="province" class="control-label">Province</label>
            <input type="text" id="province" name="province" required class="form-control {{$errors->has('province') ? 'is-invalid' : ''}}" value="{{$fee->province}}" readonly>
            @if($errors->has('province'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('province')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="shipping_charge" class="control-label">Shipping Charge</label>
            <input type="text" id="shipping_charge" name="shipping_charge" required class="form-control {{$errors->has('shipping_charge') ? 'is-invalid' : ''}}" value="{{$fee->shipping_charge}}">
            @if($errors->has('shipping_charge'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('shipping_charge')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="tax" class="control-label">Tax</label>
            <input type="text" id="tax" name="tax" required class="form-control {{$errors->has('tax') ? 'is-invalid' : ''}}" value="{{$fee->tax}}">
            @if($errors->has('tax'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('tax')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection
