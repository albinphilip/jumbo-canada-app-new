@extends('admin.layouts.datatable')
@section('title','Serial Number')
@section('tableTitle','Serial Number')
@section('createRoute')
    {{route('serial.create')}}
@endsection
@section('button')
    <a class="btn btn-primary btn-sm mr-2" href="@yield('createRoute')">Add new</a>
@endsection
@section('tableHead')
@section('tableHead')
    <th>#</th>
    <th>Product Name</th>
    <th>Serial Number</th>
    <th>Safety Code</th>
    <th>Product Code</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($products->sortBy('product_id') as $product)
        <tr>
            <td></td>
            <td>{{$product->product->name}}</td>
            <td>{{$product->serial_number}}</td>
            <td>{{$product->safety_code}}</td>
            <td>{{$product->product_code}}</td>
            <td>
                <form action="{{route('serial.destroy',$product->id)}}" method="POST" id="delete-form-{{$product->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <a href="#" onclick="return confirmation({{$product->id}});"><i class="fa fa-trash"></i> </a>
                </form>
                <a href="{{route('serial.edit',$product->id)}}"><i class="fa fa-edit"></i> </a>
            </td>
        </tr>
    @endforeach
@endsection

