@extends('admin.layouts.form')
@section('title','Import Serial Number')
@section('actionUrl')
    {{route('serial.store')}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Import')
@section('indexRoute')
    {{route('serial.index')}}
@endsection

@section('formBody')
    <div class="col-6">
        <div class="form-group">
            <label for="product" class="control-label">Product<small style="color:red;">*</small></label>
            <select name="product_id" class="form-control" required>
                <option value="">Select</option>
                @foreach($products->sortBy('name') as $product)
                    <option value="{{$product->id}}">{{$product->name}}</option>
                @endforeach
            </select>
            @if($errors->has('name'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('name')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="import_file" class="control-label">File ( CSV )
                <a href="{{asset('files/serial_file_sample.csv')}}">File Format</a></label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input {{$errors->has('import_file') ? 'is-invalid' : ''}}"
                           id="import_file" name="import_file" value="{{old('import_file')}}" required>
                    <label class="custom-file-label" for="import_file">Choose file</label>
                </div>
            </div>
            @if($errors->has('import_file'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('import_file')}}</strong>
                </span>
            @endif
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    {{ $error }}
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif
@endsection
