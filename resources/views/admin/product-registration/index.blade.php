@extends('admin.layouts.datatable')
@section('title','Product-registrations')
@section('tableTitle','Product-registrations')
{{--@section('createRoute')--}}
{{--    {{route('product.create')}}--}}
{{--@endsection--}}
@section('tableHead')
    <th>#</th>
    <th>Registration Number</th>
    <th>Status</th>
    <th>Serial Number</th>
    <th>Safety Code</th>
    <th>Product Code</th>
    <th>Date of Purchase</th>
    <th>Place of Purchase</th>
    <th>First name</th>
    <th>Last name</th>
    <th>Phone</th>
    <th>Email</th>
    <th>Province</th>
    <th>City</th>
    <th>Address</th>
    <th>Zip code</th>
    <th>Attachment</th>
    <th>Status</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($products->sortByDesc('id') as $product)
        <tr>
            <td></td>
            <td>{{$product->reg_no}}</td>
            <td>{{$product->status}}</td>
            <td>{{$product->serial_number}}</td>
            <td>{{$product->safety_code}}<br/>
                @if(preg_match('#^(PT20ULT90[1-9]{1,1}[0-9]{1,1}|PT20ULT9[1-9]{1,1}[0-9]{1,1}[0-9]{1,1}|PT20ULT10[0-4]{1,1}[0-9]{1,1}[0-9]{1,1}|PT20ULT105[00]{2}|PT21ULT[0-9]|PT21ULT[0-9]{2,}|PTMG21[0-9]{2,})$#i',
                        $product->safety_code)===0)
                    <small class="badge badge-warning">Potentially wrong PT Number</small>
                @endif
                @foreach($duplicates->sortByDesc('id') as $duplicate)
                    @if($product->safety_code == $duplicate->safety_code)
                        <small class="badge badge-danger">Duplicate PT Code</small>
                    @endif
                    @endforeach
            </td>
            <td>{{$product->product_code}}</td>
            <td>{{$product->purchase_date}}</td>
            <td>{{$product->store->city}}<br/>{{$product->store->address}}</td>
            <td>{{$product->first_name}}</td>
            <td>{{$product->last_name}}</td>
            <td>{{$product->phone}}</td>
            <td>{{$product->email}}</td>
            <td>{{$product->provenance}}</td>
            <td>{{$product->city}}</td>
            <td>{{$product->address}}</td>
            <td>{{$product->zipcode}}</td>
            <td>@if($product->attached_file!='')<a target="_blank" href="{{asset($product->attached_file)}}"><i class="fa fa-paperclip"></i> </a> @else No attachment @endif</td>
            <td>{{$product->status}}</td>
            <td>
                <a href="{{route('product-registration.edit',$product->id)}}"><i class="fa fa-edit"></i> </a>
            </td>
        </tr>
    @endforeach
            <a type="button" class="btn btn-success float-right" href="{{route('warranty.export')}}" target="_blank"><i class="fa fa-download"></i></a>
@endsection
