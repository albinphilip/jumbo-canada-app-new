@extends('admin.layouts.datatable')
@section('title','Performance')
@section('tableTitle','Performance')
@section('tableHead')
    <th>#</th>
    <th>Product List</th>
    <th>Total Sale</th>
    <th>Total Sales Value</th>
    <th hidden>Date</th>
    <th hidden>Quantity</th>
    <th hidden>Price</th>
    <th hidden>Status</th>
    <th hidden>Id</th>
@endsection
@section('tableBody')
     @php  $total_cost=0;  @endphp
     @foreach($products->sortByDesc('id') as $product)
         @php $count = 0 ; $total = 0 ; $date = '' ; $quantity = '' ; $price = ''; $status = ''; @endphp
         @foreach($product->order as $order)
             @if($order->status != 'Cancel & refunded' && $order->status != 'Return Confirmed' && $order->order_id != null)
                 @php $count = $count+$order->quantity;
                     $total = $total + ($order->product_price*$order->quantity);
                     $date = $date != '' ? $date.','.date_format($order->created_at,'Y-m-d') : date_format($order->created_at,'Y-m-d');
                     $quantity = $quantity != '' ? $quantity.','.$order->quantity : $order->quantity;
                     $price = $price != '' ? $price.','.$order->product_price : $order->product_price;
                     $status = $status != '' ? $status.','.$order->status : $order->status;
                 @endphp
             @endif
         @endforeach

             <tr>
                 <td></td>
                 <td>{{$product->name}}</td>
                 <td id="count{{$product->id}}" data-task="{{$product->id}}">{{$count}}</td>
                 <td id="total{{$product->id}}" data-task="{{$product->id}}">{{'C$'.$total}}</td>
                 <td hidden>{{$date}}</td>
                 <td hidden>{{$quantity}}</td>
                 <td hidden>{{$price}}</td>
                 <td hidden>{{$status}}</td>
                 <td hidden>{{$product->id}}</td>
             </tr>
     @endforeach

    <!-- date filter -->
    <div class="input-daterange">
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label for="from" class="control-label">From</label>
                    <input type="text" id="from_date" name="from_date" class="form-control dateFilter" value="{{old('from_date')}}" >
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="to" class="control-label">To</label>
                    <input type="text" id="to_date" name="to_date" required class="form-control dateFilter" value="{{old('to_date')}}" >
                </div>
            </div>
            <div class="col-1 pt-4">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
            </div>
            <div class="col-2 pt-4" id="download_div">
                <div class="form-group">
                    <button type="button" class="btn btn-success" onclick="exportForm()" target="_blank"><i class="fa fa-download"></i></button>
                </div>
                <div style="display:none;" id="response"><i class="fa fa-cog fa-spin"></i>Please Wait</div>
            </div>
        </div>
    </div>
@endsection

@section('evenMoreScripts')
    <!-- date filter -->
    <script>
        $(document).ready(function(){
            $('.dateFilter').datepicker({
                dateFormat: "yy-m-dd"
            });

            $.fn.dataTableExt.afnFiltering.push(
                function (settings, data, dataIndex) {
                    var min = $('#from_date').datepicker("getDate");
                    var max = $('#to_date').datepicker("getDate");
                    var date = data[4]; //date of order
                    var quantity = data[5];
                    var price = data[6];
                    var status = data[7];
                    var id  = data[8];
                    var myDataTable = $('#tb1').DataTable();

                    //console.log(data[2]);
                    if(min != null)
                    {
                        min=min.setHours(0,0,0,0);
                    }
                    if(max != null)
                    {
                        max=max.setHours(0,0,0,0);
                    }
                    count = 0;
                    total = 0;

                    if(date != '')
                    {
                        hireDate = date.split(',');
                        quantity = quantity.split(',');
                        price = price.split(',');
                        status = status.split(',');
                        for(i = 0; i< hireDate.length ; i++)
                        {
                            dorder = new Date(hireDate[i]);

                            dorder = dorder.setHours(0,0,0,0);

                            if (min == null && max == null && status[i] != 'Cancel & refunded' && status[i] != 'Return Confirmed' && status[i] != null) {
                                count = parseInt(count) + parseInt(quantity[i]);
                                total = parseFloat(total) + (parseFloat(price[i]) * quantity[i]);


                            }
                            else if (min == null && dorder <= max && status[i] != 'Cancel & refunded' && status[i] != 'Return Confirmed' && status[i] != null) {
                                count = parseInt(count) + parseInt(quantity[i]);
                                total = parseFloat(total) + (parseFloat(price[i]) * quantity[i]);

                            }
                            else if(max == null && dorder >= min && status[i] != 'Cancel & refunded' && status[i] != 'Return Confirmed' && status[i] != null) {
                                count = parseInt(count) + parseInt(quantity[i]);
                                total = parseFloat(total) + (parseFloat(price[i]) * quantity[i]);

                            }
                            else if (min <= dorder && dorder <= max && status[i] != 'Cancel & refunded' && status[i] != 'Return Confirmed' && status[i] != null) {
                                count = parseInt(count) + parseInt(quantity[i]);
                                total = parseFloat(total) + (parseFloat(price[i]) * quantity[i]);

                            }

                        }
                        //console.log(count);
                        if(count > 0)
                        {
                            data[2] = parseInt(count);
                            data[3] = parseFloat(total);
                            myDataTable.cell(dataIndex,2).data(parseInt(count));
                            myDataTable.cell(dataIndex,3).data('C$'+parseFloat(total));
                            console.log(data[3]);
                            return true;
                        }
                        else
                            return false;
                        count = 0;
                        total = 0;
                        //return false
                    }
                    //return false;
                }
            );
            $("#filter").click(function () {

                var myDataTable = $('#tb1').DataTable();
                myDataTable.paging = false;
                myDataTable.draw();
                myDataTable.paging = true;
            });
        });
        function exportForm()
        {
            var from = $('#from_date').datepicker("getDate");
            var to = $('#to_date').datepicker("getDate");
            $('#response').css('display','block');
            if(from != null )
                from = from.getFullYear() + "/" +
                    (from.getMonth() + 1) + "/" + from.getDate();
            if(to != null)
                //to = to.toLocaleDateString();
                to = to.getFullYear() + "/" +
                    (to.getMonth() + 1) + "/" + to.getDate();
            console.log(from);

            url = '/admin/report/performance-report';
            $.ajax({
                type: "post",
                url: url,
                cache: false,
                data: {_token: '{{csrf_token()}}',_method:"post",from: from,to:to},
                success: function (result) {
                    console.log(result);

                    if(result.success = 'success')

                    {
                        $('#response').css('display','none');
                        window.open(result.path,'_blank');
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error');
                }
            });
        }
    </script>
@endsection


