@extends('admin.layouts.datatable')
@section('title','Product Return')
@section('tableTitle','Product Return')
@section('tableHead')
    <th>#</th>
    <th>RMA</th>
    <th>Product Detail</th>
    <th>Store Detail</th>
    <th>Fault</th>
    <th>Customer Detail</th>
    <th hidden>Date</th>
    <th hidden>Store</th>
    <th hidden>Status</th>
@endsection
@section('tableBody')
    @foreach($returns->sortByDesc('order_id') as $return)
        <tr>
            <td></td>
            <td>{{$return->return_id}}</td>
            <td>
                Product: {{$return->product_registration->product->name}}<br/>
                Reg Id: {{$return->reg_num}}<br/>
                Reg Date: {{date('M d, Y', strtotime($return->product_registration->created_at))}}<br/>
                Purchase Date: {{date('M d, Y', strtotime($return->product_registration->purchase_date))}}<br/>
                Serial No: {{$return->product_registration->serial_number}}<br/>
                Product Code: {{$return->product_registration->product_code}}<br/>
                Safety Code: {{$return->product_registration->safety_code}}<br/>
            </td>
            <td>{{$return->product_registration->store->address}}<br/>
                {{$return->product_registration->store->city}}<br/>
                {{$return->product_registration->store->phone}}
            </td>
            <td>
                {{$return->dead}} <br/>{{$return->faulty}}<br/> Opened ? {{$return->is_open}} <br/>
                <a href="{{asset($return->image1)}}" target="_blank"><img class="mt-2" src="{{asset($return->image1)}}"
                                                                          width="100"></a>
            </td>
            <td>
                Name: {{$return->product_registration->first_name}} {{$return->product_registration->last_name}} <br/>
                Phone: {{$return->product_registration->phone}}<br/>
                Email: <a target="_blank"
                          href="mailto:{{$return->product_registration->email}}"> {{$return->product_registration->email}}</a><br/>
                Address: {{$return->product_registration->address}}, {{$return->product_registration->city}}
                , {{$return->product_registration->province}}, {{$return->product_registration->zipcode}}
            </td>
            <td hidden>{{date_format($return->created_at,'Y-m-d')}}</td>
            <td hidden>{{$return->product_registration->store->id}}</td>
            <td hidden>{{$return->status}}</td>
        </tr>
    @endforeach

    <!-- date filter -->
    <div class="input-daterange">
        <div class="row">
            <div class="col-2">
                <div class="form-group">
                    <label for="status" class="control-label">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="All">All</option>
                        <option value="New">New</option>
                        <option value="Replaced">Replaced</option>
                        <option value="Rejected">Rejected</option>
                       </select>
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="status" class="control-label">Store</label>
                    <select name="store" id="store" class="form-control">
                        <option value="All">All</option>
                       @foreach($stores as $store)
                           <option value="{{$store->id}}">{{$store->address}},{{ $store->city}}</option>
                       @endforeach
                    </select>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="from" class="control-label">From</label>
                    <input type="text" id="from_date" name="from_date" class="form-control dateFilter" value="{{old('from_date')}}" >
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="to" class="control-label">To</label>
                    <input type="text" id="to_date" name="to_date" required class="form-control dateFilter" value="{{old('to_date')}}" >
                </div>
            </div>
            <div class="col-1 pt-4">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
            </div>
            <div class="col-1 pt-4" id="download_div">
                <div class="form-group">
                    <button type="button" class="btn btn-success" onclick="exportForm()" target="_blank"><i class="fa fa-download"></i></button>
                </div>
                <div style="display:none;" id="response"><i class="fa fa-cog fa-spin"></i>Please Wait</div>
            </div>
        </div>
    </div>
@endsection

@section('evenMoreScripts')
    <!-- date filter -->
    <script>
        $(document).ready(function(){
            $('.dateFilter').datepicker({
                dateFormat: "yy-m-dd"
            });
            $.fn.dataTableExt.afnFiltering.push(
                function (settings, data, dataIndex) {
                    var min = $('#from_date').datepicker("getDate");
                    var max = $('#to_date').datepicker("getDate");
                    var status = $('#status').val();// selected status
                    var store = $('#store').val();// selected store

                    var hireDate = new Date(data[5]); //date of return
                    var store_cur = data[7];
                    var status_cur = data[8];
                    if(min != null)
                    {
                        min=min.setHours(0,0,0,0);
                    }
                    if(max != null)
                    {
                        max=max.setHours(0,0,0,0);
                    }
                    hireDate=hireDate.setHours(0,0,0,0);
                    if (min == null && max == null && (status==status_cur || status == 'All') && (store==store_cur || store == 'All')) {
                        return true; }
                    else if (min == null && hireDate <= max && (status==status_cur || status == 'All') && (store==store_cur || store == 'All')) {
                        return true;}
                    else if(max == null && hireDate >= min && (status==status_cur || status == 'All') && (store==store_cur || store == 'All')) {
                        return true;}
                    else if (min <= hireDate && hireDate <= max && (status==status_cur || status == 'All') && (store==store_cur || store == 'All')) {
                        return true; }
                    return false;
                }
            );
            $("#filter").click(function () {

                var myDataTable = $('#tb1').DataTable();
                myDataTable.draw();
                //console.log(total_amount);
            });
        });
        function exportForm()
        {
            var from = $('#from_date').datepicker("getDate");
            var to = $('#to_date').datepicker("getDate");
            var status = $('#status').val();// selected province
            var store = $('#store').val();// selected status
            $('#response').css('display','block');
            if(from != null )
                from = from.getFullYear() + "/" +
                    (from.getMonth() + 1) + "/" + from.getDate();
            if(to != null)
                //to = to.toLocaleDateString();
                to = to.getFullYear() + "/" +
                    (to.getMonth() + 1) + "/" + to.getDate();
            console.log(from);

            url = '/admin/report/return-report';
            $.ajax({
                type: "post",
                url: url,
                cache: false,
                data: {_token: '{{csrf_token()}}',_method:"post",from: from,to:to,store:store,status:status},
                success: function (result) {
                    console.log(result);

                    if(result.success = 'success')

                    {
                        $('#response').css('display','none');
                        window.open(result.path,'_blank');
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error');
                }
            });
        }
    </script>
@endsection


