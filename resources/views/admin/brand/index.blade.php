@extends('admin.layouts.datatable')
@section('title','Product Brands')
@section('tableTitle','Product Brands')
@section('createRoute')
    {{route('brand.create')}}
@endsection
@section('button')
    <a class="btn btn-primary btn-sm mr-2" href="@yield('createRoute')">Add new</a>
@endsection
@section('tableHead')
    <th>#</th>
    <th>Name</th>
    <th>Banner </th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($brands->sortByDesc('id') as $brand)
        <tr>
            <td></td>
            <td>{{$brand->name}}</td>
            <td>@if($brand->banner_image != NULL)
                <img src="{{asset($brand->banner_image)}}" width="200px"></td>
                @endif
            <td>
                <form action="{{route('brand.destroy',$brand->id)}}" method="POST" id="delete-form-{{$brand->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <a href="#" onclick="return confirmation({{$brand->id}});"><i class="fa fa-trash"></i> </a>
                </form>
                <a href="{{route('brand.edit',$brand->id)}}"><i class="fa fa-edit"></i> </a>
            </td>
        </tr>
    @endforeach
@endsection
