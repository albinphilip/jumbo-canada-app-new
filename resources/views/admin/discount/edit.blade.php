@extends('admin.layouts.form')
@section('title','Add Proof')
@section('actionUrl')
    {{route('discount.update',$discount)}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Add Proof')
@section('indexRoute')
    {{route('discount.index')}}
@endsection
@section('formBody')
    <!-- this is needed in edit form -->
    <input type="hidden" name="_method" value="PUT">
    <!-- ---------------------------- -->
    <div class="col-6">
        <div class="form-group">
            <label for="name" class="control-label">Name</label>
            <input type="text" id="name" name="name" required class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" value="{{$discount->name}}" readonly>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="email" class="control-label">Email</label>
            <input type="text" id="email" name="email" required class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" value="{{$discount->email}}" readonly>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="phone" class="control-label">Phone</label>
            <input type="text" id="phone" name="phone" data-inputmask='"mask": "(999) 999-9999"' data-mask
                   required class="form-control {{$errors->has('phone') ? 'is-invalid' : ''}}" value="{{$discount->phone}}" readonly>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="proof">Proof</label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input {{$errors->has('proof') ? 'is-invalid' : ''}}" id="proof" required name="proof" value="{{old('proof')}}" accept="application/pdf , image/png, image/jpeg">
                    <label class="custom-file-label" for="proof">Choose file</label>
                </div>
            </div>
            @if($errors->has('proof'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('proof')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script src="{{asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
    <script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
    <script>
        $(function () {
            // Summernote
            $('.textarea').summernote()
        })
    </script>
@endsection
