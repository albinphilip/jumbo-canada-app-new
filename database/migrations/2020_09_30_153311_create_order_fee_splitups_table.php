<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderFeeSplitupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_fee_splitups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_table_id');
            $table->foreign('order_table_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
            $table->string('order_id');
            $table->string('sub_total');
            $table->string('shipping_charge');
            $table->string('tax');
            $table->string('reduction')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_fee_splitups');
    }
}
